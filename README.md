# Carbonex
This is the start of the implementation of a clients project who is happy for me to use this as a case study. Details of the company and it's objective can be found here: https://carbonex.co

## Introduction
### What does it do
This platform allows clients to request new Carbon Credits to be minted. This creates a contract that represents the Carbon Credits and holds their category data, like how they were produced.

These carbon credits can then be sold to other users, facilitating the distribution of carbon credits and hopefully assisting climate change mitigation.

### How to run it
The infrastructure is based on a typical truffle box, so it should work very similarly. Please follow the following instructions to get started:
1. Download the repository.
2. Have Node, NPM, Ganche and Truffle installed.
3. Run `npm install`.
4. Start your instance of ganache `ganache-cli` and take a note of the mnemonic to enter in Metamask.
5. Run `truffle migrate`. This will migrate the contracts to ganache and seed some data for the development environment to make it easier to preview the functionality.
6. Run `npm run serve:dev`. This will serve the fronted at localhost:3000 and also start an oracle in node listening to requests while running.
7. Ensure you are logged in to metamask, entering the mnemonic into the seed phrase created earlier by ganache.
8. You should start on a for admin users (as the first account was treated as an admin). To see a typical user, please "Create Account" in metamask (3 already created). There have been three initial users been created to give a preview of what each user should be able to see.

If you have any issues there are appropriate error messages so please follow the frontend instructions. Otherwise, also try "Reset Account" in metamask if transactions remaing stuck on Submitted.

### Online preview
The website has also available online on IPFS at: [https://ipfs.io/ipfs/QmeRsZFouWtg1SDHDjvfJWjiwUajTrXByqkZtLdz7yjPip/](https://ipfs.io/ipfs/QmeRsZFouWtg1SDHDjvfJWjiwUajTrXByqkZtLdz7yjPip/).\
The contracts have also been deployed to Rinkeby if you would like to interact with them there. 


### Demo data
The final migration is a seed data migration. This only runs on the development network. It uses the accounts created by ganache and creates one admin (the first account) and three users. It randomly assigns them 10 carbon credits, of which 7 are minted and 3 have been put up for sale.

To interact as different users, simply create new users in metamask, with the first user being an admin user.

**Admin:** You can see the admin has a limited view. They can only see the "List" and "Admin" screens.\
The "List" shows all minted coins and is a record of all the coins that have been verified by Carbonex.\
The "Admin" screen shows all the requests for new carbon credits to be minted. Once the admin receives all the required information and certificate they are then able to approve a carbon credit to bring it onto the Carbonex exchange.

**User:** Users have a few different screens. 
The "List" screen is the same as for the admin user. However, it also shows how much ownership of a carbon credit they have.\
The "Buy" screen shows carbon credits put on the market to buy. They have a preset price. They can only see the carbon credits on the market that they did't put on the market.\
The "Mint" screen provides a form that allows a user to request that their carbon credits get minted.
The "Account" screen shows a status of all their personal carbon credits (minted or just requested). There is also an option to sell carbon credits. This will put them on the market (in the "Buy screen"), and they will also be able to see what they have on sale below in the Account screen.

### Typical user journey
1. A user requests their carbon credits to be minted in the "Mint" screen.
2. The user can then see them in their account screen with status requested and none of the credits assigned to them.
3. An admin user will then go to the "Admin" screen to mint a coin if they have verified it's genuine. This will mark the coin status as MINTED and assign the credits to the requester.
4. The user can then view the credits on the Account page with the updated data and the ability to sell the credits.
5. The user (seller) can then sell the carbon credits by setting the amount they want to sell and the price they want to sell out.
6. The seller will then see the carbon credits on the Account page in the Sales table.
7. Other users (buyer) will then be able to purchase the carbon credits. Buying these carbon credits will reduce the number of carbon credits for sale, and the buyer will have those carbon credits assigned to them.
8. The seller will now have fewer credits for sale but a larger balance. On the Account page they are able to withdraw any balance they have.

## Tests
A test suite covering the major security concerns can be seen by running `npm run test:bc` with ganche-cli running.

### Architecture
The contract architecture has been outlined here:
[Architecture](./documentation/architecture.md)

### Features
A list of features implemented to satisfy the rubric have been described here to make it easy to identify:
[Features](./documentation/features.md)

### Design Patterns
The design patterns used are described here: [Design Patterns](./documentation/design_pattern_decisions.md)

### Security Considerations
The measures I have taken to avoid common attacks are described here: [Security Considerations](./documentation/avoiding_common_attacks.md)

