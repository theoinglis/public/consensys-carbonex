pragma solidity ^0.4.24;

import "./CbnCoin.sol";

library CxMintingLibrary {

  struct Minted {
    address[] coins;
    mapping(address => bool) isMinted;
    mapping(address => address[]) userCoins;
  }

  event MintingRequest(address indexed senderAddress, address indexed coinAddress);
  event MintedCoin(address indexed senderAddress, address indexed coinAddress);
  event Expired(address indexed senderAddress, address indexed coinAddress, uint256 amount);
  event OwnsCoins(address indexed accountAddress, address indexed coinAddress);

  modifier onlyKnownCoin(Minted storage self, address coinAddress) {
    require(isMintedCoin(self, coinAddress));
    _;
  }

  function isMintedCoin(Minted storage self, address coinAddress) internal view returns(bool) {
    return self.isMinted[coinAddress];
  }

  function requestCarbonCreditMinting(
    Minted storage self,
    address registryAddress,
    uint256 volume,
    string variant,
    string region,
    string method,
    string certificatePath
  ) internal 
    view
  {
    CbnCoin coin = new CbnCoin(registryAddress, msg.sender, volume, variant, region, method, certificatePath);
    address coinAddress = address(coin);
    self.coins.push(coinAddress);
    self.userCoins[msg.sender].push(coinAddress);
    self.isMinted[coinAddress] = true;
    emit MintingRequest(msg.sender, coinAddress);
  }

  function mintCarbonCredit(
    Minted storage self,
    address coinAddress
  ) internal 
    view
    onlyKnownCoin(self, coinAddress)
  {
    CbnCoin coin = CbnCoin(coinAddress);
    coin.mint();
    emit MintedCoin(msg.sender, coinAddress);
  }

  function addCoinToAccount(
    Minted storage self,
    address accountAddress,
    address coinAddress
  ) internal
    view
    onlyKnownCoin(self, coinAddress)
  {
    self.userCoins[accountAddress].push(coinAddress);
    emit OwnsCoins(address(this), accountAddress);
  }
}
