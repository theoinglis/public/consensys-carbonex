pragma solidity ^0.4.24;


/** @title Coin Interface. */
interface CoinInterface {

  /** @dev Event emitted when a carbon credit is minted.
    * @param owner The owner of the minted coin.
    * @param amount The amount that was minted.
    */
  event Minted(address indexed owner, uint256 amount);

  /** @dev Event emitted when a carbon credit is expired.
    * @param coin The address of the expired coin.
    * @param owner The owner of the expired coin.
    * @param amount The amount that was expired.
    */
  event Expired(address indexed coin, address indexed owner, uint256 amount);

  /** @dev Event emitted when all of the carbon credits have expired.
    * @param coin The address of the expired coin.
    */
  event ExpirationComplete(address indexed coin);

  /** @dev Event emitted when the ipfs data is retrieved.
    * @param requester The requester of the ipfs data.
    * @param content The content that was retrieved.
    */
  event RetrievedIpfsData(address indexed requester, string content);

  /** @dev Get's all relevant data that's available
    * @return hasOwnership Whether the caller has ownership of the coin.
    * @return volume The number of coins created.
    * @return variant The variant of the coin.
    * @return region Where the coin was produced.
    * @return method The method used to produce the coin.
    * @return ownershipAmount The amount of coins owned by the caller.
    * @return currentState The current state of the coin.
    */
  function getData() 
    external 
    view 
    returns(
    bool hasOwnership, 
    uint256 volume, 
    string variant, 
    string region, 
    string method,
    uint256 ownershipAmount,
    uint8 currentState
  );

  /** @dev Get's the IPFS data. This is separate from get data as it is external information 
    * and therefore is run through an oracle. It will be returned to the client by listening
    * to RetrievedIpfsData event.
    */
  function getIpfsData() external;
  
  /** @dev Updates the status of a coin to be minted.
    */
  function mint() external payable;

  /** @dev Expires all coins for a given owner
    * @param owner The owner of the coins.
    */
  function expireAll(address owner) public payable;

  /** @dev Expires a given amount of coins for a given owner
    * @param owner The owner of the coins.
    * @param amountToExpire The amount of coins to expire.
    */
  function expire(address owner, uint256 amountToExpire) public payable;
}
