pragma solidity ^0.4.24;

import "zeppelin/ownership/Ownable.sol";
import "../support/registry/RegistryInterface.sol";
import "../support/registry/HasRegistry.sol";
import "../support/registry/RegistryAdminRole.sol";
import "../sale/CbnSale.sol";

import "./CxMinterInterface.sol";
import "./CxMintingLibrary.sol";
import "./CbnCoin.sol";

contract CxMinter is CxMinterInterface, Ownable, 
  HasRegistry, RegistryAdminRole {
  using CxMintingLibrary for CxMintingLibrary.Minted;

  CxMintingLibrary.Minted private minted;

  event MintingRequest(address indexed senderAddress, address indexed coinAddress);
  event MintedCoin(address indexed senderAddress, address indexed coinAddress);
  event Expired(address indexed senderAddress, address indexed coinAddress, uint256 amount);
  event OwnsCoins(address indexed sellerAddress, address indexed coinAddress);

  modifier onlyMintedCoin(address coinAddress) {
    require(minted.isMintedCoin(coinAddress));
    _;
  }
  modifier onlyFromSale() {
    require(true);
    _;
  }

  constructor(address _registryAddress)
  HasRegistry(_registryAddress) 
  public {}

  function getCoins() public view returns(address[]) {
    return minted.coins;
  }
  function getMyCoins() public view returns(address[]) {
    return minted.userCoins[msg.sender];
  }

  function requestCarbonCreditMinting(
    uint256 volume,
    string variant,
    string region,
    string method,
    string certificatePath
  ) public 
    payable
  {
    minted.requestCarbonCreditMinting(registryAddress, volume, variant, region, method, certificatePath);
  }

  function mintCarbonCredit(
    address coinAddress
  ) public 
    payable 
    onlyAdmin
  {
    return minted.mintCarbonCredit(coinAddress);
  }

  function addCoinToAccount(
    address accountAddress,
    address coinAddress
  ) external
    payable
    onlyMintedCoin(coinAddress)
    onlyFromSale
  {
    minted.addCoinToAccount(accountAddress, coinAddress);
  }

  function () public {
    revert();
  }
}
