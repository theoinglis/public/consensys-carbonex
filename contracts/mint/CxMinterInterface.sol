pragma solidity ^0.4.24;

interface CxMinterInterface {

  /** @dev Event emitted when a user requests carbon credits to be minted.
    * @param senderAddress The address of the person requesting the carbon credits to be minted.
    * @param coinAddress The address of the coin trying to be minted.
    */
  event MintingRequest(address indexed senderAddress, address indexed coinAddress);

  /** @dev Event emitted when the coin is minted.
    * @param senderAddress The address of the person who requested the carbon credits to be minted.
    * @param coinAddress The address of the coin that was minted.
    */
  event MintedCoin(address indexed senderAddress, address indexed coinAddress);
  
  /** @dev Event emitted when the coin is expired.
    * @param senderAddress The address of the person who requested the carbon credits to be expired.
    * @param coinAddress The address of the coin that was expired.
    * @param amount The number of coins being expired.
    */
  event Expired(address indexed senderAddress, address indexed coinAddress, uint256 amount);

  /** @dev Event emitted when a new user owns the coins.
    * @param accountAddress The address of the person who now has ownership of coins.
    * @param coinAddress The address of the coin that the user now has ownership of.
    */
  event OwnsCoins(address indexed accountAddress, address indexed coinAddress);

  /** @dev Adds coins to a user.
    * @param accountAddress The address of the person who now has coins added to their account.
    * @param coinAddress The address of the coin that the user now has ownership of.
    */
  function addCoinToAccount(
    address accountAddress,
    address coinAddress
  ) external payable;
}