pragma solidity ^0.4.24;

import "tokens/eip20/EIP20.sol";

import "../support/categories/CategoriesInterface.sol";
import "../support/registry/HasRegistry.sol";
import "../support/registry/HasRegistryStorage.sol";
import "../support/registry/HasRegistryCategories.sol";
import "../support/registry/RegistryAdminRole.sol";
import "../support/registry/RegistryInterface.sol";
import "../storage/CxStorage.sol";
import "../oracle/OracleRequester.sol";

import "./CoinInterface.sol";


/** @title Carbon Credit Coin */
contract CbnCoin is CoinInterface, EIP20, 
  HasRegistry, HasRegistryStorage, HasRegistryCategories, RegistryAdminRole,
  OracleRequester {

  enum State { Requested, Minted, Expired }
  struct Attributes {
    string variant;
    string region;
    string method;
  }

  State public state = State.Requested;
  address private registry;
  address private creator;
  uint256 public remainingSupply;
  string public certificatePath;
  Attributes public attributes;
  mapping (address => uint256) public expired;

  event Minted(address indexed owner, uint256 amount);
  event Expired(address indexed coin, address indexed owner, uint256 amount);
  event ExpirationComplete(address indexed coin);
  event RetrievedIpfsData(address indexed requester, string content);

  modifier onlyState(State requiredState) {
    require(state == requiredState);
    _;
  }

  constructor(
    address _registry,
    address _creator, 
    uint256 _volume,
    string _variant,
    string _region,
    string _method,
    string _certificatePath
  ) EIP20(
    0,
    "Carbonex Carbon Credit",
    0,
    "CBN"
  ) HasRegistry(
    _registry
  ) public {
    creator = _creator;
    totalSupply = _volume;
    remainingSupply = _volume;
    certificatePath = _certificatePath;
    
    CategoriesInterface categories = getCategories();
    require(categories.isCategoryOption("variant", _variant), "Variant is not a suitable option");
    require(categories.isCategoryOption("region", _region), "Region is not a suitable option");
    require(categories.isCategoryOption("method", _method), "Method is not a suitable option");
    attributes = Attributes({
      variant: _variant,
      region: _region,
      method: _method
    });
  }

  function getData() 
    external 
    view 
    returns(
    bool hasOwnership, 
    uint256 volume, 
    string variant, 
    string region, 
    string method,
    uint256 ownershipAmount,
    uint8 currentState
  ) {
    uint256 myBalance = balanceOf(msg.sender);
    return (
      myBalance != 0,
      totalSupply,
      attributes.variant,
      attributes.region,
      attributes.method,
      myBalance,
      uint8(state)
    );
  }

  function getCertificatePath() external view returns(string) { return certificatePath; }

  function handleOracleRequest(address from, string result) internal view {
    emit RetrievedIpfsData(from, result);
  }
  function getIpfsData() external {
    sendOracleRequest("any request, probably an api request");
  }
  
  function mint() 
    external 
    payable 
    onlyAdmin 
    onlyState(State.Requested) 
  {
    state = State.Minted;

    balances[creator] = totalSupply; 
    emit Minted(creator, totalSupply);
  }

  function expireAll(address _owner) 
    public
    payable
    onlyAdmin 
  {
    expire(_owner, balances[_owner]);
  }

  function expire(address _owner, uint256 _amountToExpire) 
    public 
    payable
    onlyAdmin 
  {
    require(balances[_owner] >= _amountToExpire, "You don't have enough coins to expire that amount");
    balances[_owner] -= _amountToExpire;
    expired[_owner] += _amountToExpire;
    remainingSupply -= _amountToExpire;
    emit Expired(address(this), _owner, _amountToExpire);

    if (remainingSupply == 0) markExpired();
  }

  function markExpired() 
    internal 
    onlyState(State.Minted) {
    state = State.Expired;
    emit ExpirationComplete(address(this));
  }

  function () public {
    revert();
  }
}
