pragma solidity ^0.4.24;

import "./StringConversion.sol";

library String32ListLibrary {
  using StringConversion for *;

  modifier notItem(List storage self, string item) {
    require(!self.items[item], "This is already an item in the list");
    _;
  }
  struct List {
    mapping (string => bool) items;
    bytes32[] itemsAsBytes;
  }

  function addItem(List storage self, string item) 
    internal
    notItem(self, item) 
  {
    self.items[item] = true;
    self.itemsAsBytes.push(item.stringToBytes32());
  }
  function addItems(List storage self, bytes32[] items) internal {
    for (uint i = 0; i < items.length; i++) {
      string memory itemName = items[i].bytes32ToString();
      addItem(self, itemName);
    }
  }

  function isItem(List storage self, string item) internal view returns(bool) {
    return self.items[item];
  }

  function getItems(List storage self) internal view returns(bytes32[]) {
    return self.itemsAsBytes;
  }
}
