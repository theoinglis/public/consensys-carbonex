pragma solidity ^0.4.24;

import "zeppelin/ownership/Ownable.sol";

contract AdminRole is Ownable {

  mapping (address => bool) public admins; 

  modifier onlyAdmin() {
    require(admins[msg.sender], "Only an admin user can perform this function");
    _; 
  }

  constructor() 
    public 
  {
    admins[msg.sender] = true;
  }

  function amAdmin() public view returns(bool) {
    return admins[msg.sender];
  }
  function setAdmin(address admin, bool isAdmin) public payable onlyOwner {
    admins[admin] = isAdmin;
  }
}