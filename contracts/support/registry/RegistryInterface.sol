pragma solidity ^0.4.24;

contract RegistryInterface {

  function addr(string name) public view returns (address);
}
