pragma solidity ^0.4.24;

import "zeppelin/ownership/Ownable.sol";

import "./EnsResolverInterface.sol";

contract EnsResolver is EnsResolverInterface, Ownable {
    event AddrChanged(bytes32 indexed node, address a);

    mapping(bytes32=>address) addresses;

    function Resolver() {
        owner = msg.sender;
    }

    function addr(bytes32 node) constant returns(address) {
        return addresses[node];    
    }

    function setAddr(bytes32 node, address addr) onlyOwner {
        addresses[node] = addr;
        AddrChanged(node, addr);
    }

    function supportsInterface(bytes4 interfaceID) constant returns (bool) {
        return interfaceID == 0x3b3b57de || interfaceID == 0x01ffc9a7;
    }

    function() {
        revert();
    }
}