pragma solidity ^0.4.24;

import "./HasRegistry.sol";
import "../categories/CategoriesInterface.sol";

contract HasRegistryCategories is HasRegistry {
  function getCategories() public view returns(CategoriesInterface) {
    return CategoriesInterface(registry.addr("categories.carbonex.eth"));
  }
}
