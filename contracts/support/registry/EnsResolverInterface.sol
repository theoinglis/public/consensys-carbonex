pragma solidity ^0.4.24;

interface EnsResolverInterface {
    event AddrChanged(bytes32 indexed node, address a);

    function addr(bytes32 node) public view returns(address);
    function setAddr(bytes32 node, address resolveToAddress) public;
    function supportsInterface(bytes4 interfaceID) public view returns (bool);
}