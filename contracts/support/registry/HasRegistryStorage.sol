pragma solidity ^0.4.24;

import "./HasRegistry.sol";
import "../../storage/CxStorage.sol";

contract HasRegistryStorage is HasRegistry {
  function getStorage() public view returns(CxStorage) {
    return CxStorage(registry.addr("storage.carbonex.eth"));
  }
}
