pragma solidity ^0.4.24;

import "./EnsResolverInterface.sol";

contract HasRegistry {

  address public registryAddress;
  EnsResolverInterface public registry;

  constructor(address _registryAddress) 
  public {
    registryAddress = _registryAddress;
    registry = EnsResolverInterface(registryAddress);
  }
}
