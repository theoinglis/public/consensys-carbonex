pragma solidity ^0.4.24;

import "./HasRegistry.sol";
import "./HasRegistryStorage.sol";

contract RegistryAdminRole is HasRegistry, HasRegistryStorage {
  modifier onlyAdmin() {
    require(amAdmin(), "You need to be an admin to perform this function");
    _;
  }

  function amAdmin() public view returns(bool) {
    return getStorage().admins(msg.sender);
  }
}
