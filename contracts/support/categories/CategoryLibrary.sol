pragma solidity ^0.4.24;

import "../utils/String32ListLibrary.sol";

library CategoryLibrary {

  using String32ListLibrary for String32ListLibrary.List;

  struct Category {
    string name;
    String32ListLibrary.List list;
  }

  modifier onlyInitialised(Category storage self, bool wantInitialised) {
    bool _isInitialised = isInitialised(self);
    if (wantInitialised) require(_isInitialised, "The category needs to be initialised");
    else require(!_isInitialised, "The category has already been initialised");
    _;
  }

  function isInitialised(Category storage self) public view returns(bool) {
    bytes memory currentName = bytes(self.name);
    return currentName.length != 0;
  }
  function createCategory(Category storage self, string name) public onlyInitialised(self, false) {
    self.name = name;
  }
  function createCategory(Category storage self, string name, bytes32[] options) public onlyInitialised(self, false) {
    createCategory(self, name);
    self.list.addItems(options);
  }

  function addOption(Category storage self, string option) public onlyInitialised(self, true)  {
    self.list.addItem(option);
  }

  function isOption(Category storage self, string option) public view onlyInitialised(self, true) returns(bool) {
    return self.list.isItem(option);
  }
  function getOptions(Category storage self) public view onlyInitialised(self, true) returns(bytes32[]) {
    return self.list.getItems();
  }
}
