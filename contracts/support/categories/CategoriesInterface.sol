pragma solidity ^0.4.24;

interface CategoriesInterface {

  /** @dev Gets the options for a given category
    * @param categoryName The name of the category to retreive the options from.
    * @return options A list of options for the category.
    */
  function getCategoryOptions(string categoryName) public view returns(bytes32[]);

  /** @dev Identifies whether the option is in the category
    * @param categoryName The name of the category to check from.
    * @param option The name of the option to check.
    * @return isOption Whether the option is in the category.
    */
  function isCategoryOption(string categoryName, string option) public view returns(bool);
}
