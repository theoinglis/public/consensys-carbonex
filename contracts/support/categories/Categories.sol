pragma solidity ^0.4.24;

import "zeppelin/ownership/Ownable.sol";

import "./CategoriesInterface.sol";
import "./CategoryLibrary.sol";

contract Categories is CategoriesInterface, Ownable {
  using CategoryLibrary for CategoryLibrary.Category;

  address private manager;
  mapping (string => CategoryLibrary.Category) private categories;

  modifier notExistingCategory(string name) {
    require(categories[name].isInitialised() == false, "Category already exists");
    _;
  }
  modifier existingCategory(string name) {
    require(categories[name].isInitialised(), "Category doesn't exists");
    _;
  }

  constructor() 
  public {}

  function addCategory(string categoryName, bytes32[] options) 
    public 
    onlyOwner
    notExistingCategory(categoryName) 
    payable {
    categories[categoryName].createCategory(categoryName, options);
  }
  function getCategoryOptions(string categoryName) 
    public 
    view 
    existingCategory(categoryName)
    returns(bytes32[]) {
    return categories[categoryName].getOptions();
  }
  function isCategoryOption(string categoryName, string option) 
    public 
    view 
    existingCategory(categoryName) 
    returns(bool) {
    return categories[categoryName].isOption(option);
  }
}
