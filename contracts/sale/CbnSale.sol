pragma solidity ^0.4.24;

import "zeppelin/ownership/Ownable.sol";
import "zeppelin/payment/PullPayment.sol";
import "zeppelin/math/SafeMath.sol";
import "zeppelin/lifecycle/Pausable.sol";

import "../support/registry/HasRegistry.sol";
import "../mint/CxMinterInterface.sol";
import "../mint/CbnCoin.sol";

import "./SaleInterface.sol";

contract CbnSale is SaleInterface, Ownable, Pausable, PullPayment, HasRegistry {
  using SafeMath for uint256;

  enum State { Created, Available, Complete, Cancelled }

  State public state = State.Created;
  address public sellerAddress;
  address public coinAddress;
  CbnCoin private coin;
  uint256 public totalAmount;
  uint256 public pricePerCoin;

  event SaleRequested(address indexed sellerAddress, address indexed coinAddress, uint256 totalAmount, uint256 pricePerCoin);
  event SaleVerified(address indexed sellerAddress, address indexed saleAddress);
  event SalePurchased(address indexed sellerAddress, address indexed saleAddress, address indexed buyerAddress, uint256 purchasedAmount);
  event SaleComplete(address indexed sellerAddress, address indexed saleAddress);
  event SaleCancelled(address indexed sellerAddress, address indexed saleAddress);

  modifier onlySeller() {
    require(amSeller());
    _;
  }

  modifier onlyState(State newState) {
    require(state == newState);
    _;
  }
  modifier verifyBuyerHasSufficientDeposit(uint256 amount) {
    require(msg.value >= amount.mul(pricePerCoin));
    _;
  }
  modifier verifySellerHasSufficientCoinsRemaining(uint256 amount) {
    require(getCoinsAvailable() >= amount);
    _;
  }

  constructor(
    address _registry,
    address _sellerAddress,
    address _coinAddress,
    uint256 _totalAmount,
    uint256 _pricePerCoin
  ) HasRegistry(
    _registry
  ) public {
    coin = CbnCoin(_coinAddress);
    require(coin.allowance(_sellerAddress, owner) >= _totalAmount);

    sellerAddress = _sellerAddress;
    coinAddress = _coinAddress;
    pricePerCoin = _pricePerCoin;
    totalAmount = _totalAmount;

    emit SaleRequested(sellerAddress, coinAddress, totalAmount, pricePerCoin);
  }

  function amSeller() public view returns(bool) {
    return sellerAddress == msg.sender;
  }
  function getMinter() public view returns(CxMinterInterface) {
    return CxMinterInterface(registry.addr("minter.carbonex.eth"));
  }
  function getMyBalance() public view returns(uint256) {
    return payments[msg.sender];
  }
  function getCoinsAvailable() public view returns(uint256) {
    return coin.balanceOf(address(this));
  }
  function getData() public view returns(
    bool _isSeller, 
    address _coinAddress, 
    address _sellerAddress, 
    uint256 _pricePerCoinInWei, 
    uint256 _remainingAmount, 
    uint256 _totalAmount, 
    uint256 _balance,
    uint8 _state) {
    return (
      amSeller(),
      coinAddress,
      sellerAddress,
      pricePerCoin,
      getCoinsAvailable(),
      totalAmount,
      getMyBalance(),
      uint8(state)
    );
  }

  function verifyCarbonCreditsTransferred() 
    public 
    payable 
    onlyOwner
    onlyState(State.Created) 
  {
    // Verify the coins have been transferred
    require(getCoinsAvailable() == totalAmount);
    state = State.Available;

    emit SaleVerified(sellerAddress, address(this));
  }

  function buyRemaining() 
    public 
    payable 
  {
    buy(getCoinsAvailable());
  }
  
  function buy(uint256 amount) 
    public
    payable 
    whenNotPaused
    onlyState(State.Available)
    verifyBuyerHasSufficientDeposit(amount)
    verifySellerHasSufficientCoinsRemaining(amount) 
  {
    asyncSend(sellerAddress, amount.mul(pricePerCoin));
    coin.transfer(msg.sender, amount);

    // Update the ownership of coins with the minter
    getMinter().addCoinToAccount(msg.sender, coinAddress);
    emit SalePurchased(sellerAddress, address(this), msg.sender, amount);

    if (getCoinsAvailable() == 0) completeSale();
  }

  function cancelRemainingSale() 
    public 
    payable
    onlyOwner 
    onlyState(State.Available)  
  {
    state = State.Cancelled;
    emit SaleCancelled(sellerAddress, address(this));
  }

  function completeSale() internal onlyState(State.Available) {
    state = State.Complete;
    emit SaleComplete(sellerAddress, address(this));
  }

  function () public {
    revert();
  }
}
