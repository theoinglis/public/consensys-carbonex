pragma solidity ^0.4.24;

interface CxBrokerInterface {
  
  /** @dev The event emitted when a sale is created
    * @param contractAddress The address of the new sale address.
    * @param senderAddress The user requesting the sale.
    * @param coinAddress The address of the coin being sold.
    * @param amount The number of coins being sold.
    */
  event OnSaleCreated(address indexed contractAddress, address indexed senderAddress, address indexed coinAddress, uint256 amount);

  /** @dev Retrieves all the sales created.
    */
  function getSales() public view returns(address[]);

  /** @dev Retrieves all the sales created by the requesting user.
    */
  function getMySales() public view returns(address[]);

  /** @dev Puts carbon credits up for sale.
    * @param coinAddress The address of the coin being put up for sale.
    * @param totalAmount The number of coins being put up for sale.
    * @param pricePerCoin The price per coin in Wei.
    */
  function putCarbonCreditsUpForSale(address coinAddress, uint256 totalAmount, uint256 pricePerCoin) public payable;
}