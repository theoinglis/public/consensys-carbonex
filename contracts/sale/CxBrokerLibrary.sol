pragma solidity ^0.4.24;

import "../support/registry/RegistryInterface.sol";

import "../mint/CbnCoin.sol";
import "./CbnSale.sol";

library CxBrokerLibrary {
  struct Broker {
    address[] sales;
    mapping(address => bool) isBrokerSale;
    mapping(address => address[]) userSales;
  }

  event OnSaleCreated(address indexed contractAddress, address indexed senderAddress, address indexed coinAddress, uint256 amount);

  function getSales(Broker storage self) internal view returns(address[]) {
    return self.sales;
  }
  function getMySales(Broker storage self) internal view returns(address[]) {
    return self.userSales[msg.sender];
  }

  function putCarbonCreditsUpForSale(
    Broker storage self,
    address registry,
    address coinAddress,
    uint256 totalAmount,
    uint256 pricePerCoin
  ) internal
    view
  {
    CbnCoin coin = CbnCoin(coinAddress);
    require(coin.amAdmin());
    CbnSale sale = new CbnSale(registry, msg.sender, coinAddress, totalAmount, pricePerCoin);
    address saleAddress = address(sale);
    coin.transferFrom(msg.sender, saleAddress, totalAmount);
    sale.verifyCarbonCreditsTransferred();
    self.sales.push(saleAddress);
    self.isBrokerSale[saleAddress] = true;
    self.userSales[msg.sender].push(saleAddress);
    
    emit OnSaleCreated(address(this), msg.sender, coinAddress, totalAmount);
  }
}
