pragma solidity ^0.4.24;

import "zeppelin/ownership/Ownable.sol";
import "zeppelin/lifecycle/Pausable.sol";

import "../support/registry/HasRegistry.sol";
import "./CxBrokerLibrary.sol";
import "./CxBrokerInterface.sol";


contract CxBroker is CxBrokerInterface, Ownable, Pausable, HasRegistry {
  using CxBrokerLibrary for CxBrokerLibrary.Broker;

  address public registryAddress;
  CxBrokerLibrary.Broker private broker;
  
  event OnSale(address indexed contractAddress, address indexed senderAddress, address indexed coinAddress, uint256 amount);
  
  constructor(address _registry)
  HasRegistry(_registry)
  public {}

  modifier onlyBrokerSale(address saleAddress) {
    require(broker.isBrokerSale[saleAddress]);
    _;
  }
  function getSales() public view returns(address[]) {
    return broker.getSales();
  }
  function getMySales() public view returns(address[]) {
    return broker.getMySales();
  }

  function putCarbonCreditsUpForSale(
    address coinAddress,
    uint256 totalAmount,
    uint256 pricePerCoin
  ) public 
    payable
    whenNotPaused 
  {
    broker.putCarbonCreditsUpForSale(registry, coinAddress, totalAmount, pricePerCoin);
  }

  function () public {
    revert();
  }
}
