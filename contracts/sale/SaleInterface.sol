pragma solidity ^0.4.24;

interface SaleInterface {

  /** @dev The event emitted when a sale is requested
    * @param sellerAddress The address of the person requesting the sale.
    * @param coinAddress The address of the coin being put up for sale.
    * @param totalAmount The number of coins being put up for sale.
    * @param pricePerCoin The price per coin in Wei.
    */
  event SaleRequested(address indexed sellerAddress, address indexed coinAddress, uint256 totalAmount, uint256 pricePerCoin);

  /** @dev The event emitted when the coins have been transferred into the contract
    * @param sellerAddress The address of the person who requested the sale.
    * @param saleAddress The address of the sale being verified.
    */
  event SaleVerified(address indexed sellerAddress, address indexed saleAddress);

  /** @dev The event emitted when a user purchases coins
    * @param sellerAddress The address of the person who put the coins up for sale.
    * @param saleAddress The address of this contract.
    * @param buyerAddress The address of the individual buying the coins.
    * @param purchasedAmount The amount of coins that were purchased.
    */
  event SalePurchased(address indexed sellerAddress, address indexed saleAddress, address indexed buyerAddress, uint256 purchasedAmount);

  /** @dev The event emitted when all the coins are bought
    * @param sellerAddress The address of the person who requested the sale.
    * @param saleAddress The address of the sale being completed.
    */
  event SaleComplete(address indexed sellerAddress, address indexed saleAddress);

  /** @dev The event emitted when all the coins were cancelled
    * @param sellerAddress The address of the person who requested the sale.
    * @param saleAddress The address of the sale being cancelled.
    */
  event SaleCancelled(address indexed sellerAddress, address indexed saleAddress);

  /** @dev Whether the seller is the seller
    * @return isSeller Is the user the seller.
    */
  function amSeller() public view returns(bool);

  /** @dev The balance of the requester
    * @return balance The balance of the seller.
    */
  function getMyBalance() public view returns(uint256);

  /** @dev The number of coins available to buy.
    * @return numberOfCoins The number of coins available.
    */
  function getCoinsAvailable() public view returns(uint256);

  /** @dev Get's all relevant data that's available
    * @return isSeller Whether the caller is the owner of the coins.
    * @return coinAddress The address of the coin being sold.
    * @return pricePerCoinInWei The price of each coin in Wei.
    * @return remainingAmount The remaining amount of coins to be sold.
    * @return totalAmount The total amount of coins that were available to be sold.
    * @return balance The balance of the requester.
    * @return state The current state of the sale.
    */
  function getData() public view returns(
    bool isSeller, 
    address coinAddress, 
    address sellerAddress, 
    uint256 pricePerCoinInWei, 
    uint256 remainingAmount, 
    uint256 totalAmount, 
    uint256 balance,
    uint8 state
  );

  /** @dev Verifies that the carbon credits have been trasnferred to the contract
    */
  function verifyCarbonCreditsTransferred() public payable;

  /** @dev Buys all the remaining coins
    */
  function buyRemaining() public payable;

  /** @dev Buys some coins
    * @param amount The number of coins to be bought.
    */
  function buy(uint256 amount) public payable;

  /** @dev Cancels the sale.
    */
  function cancelRemainingSale() public payable;
}
