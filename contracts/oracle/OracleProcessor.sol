pragma solidity ^0.4.24;

import "../support/registry/HasRegistry.sol";
import "../support/registry/RegistryAdminRole.sol";
import "./OracleProcessorInterface.sol";
import "./OracleRequesterInterface.sol";

contract OracleProcessor is OracleProcessorInterface, HasRegistry, RegistryAdminRole {
  event GetData(address sender, address originator, string data);
  event GotData(string data);

  constructor(address _registry)
  HasRegistry(_registry)
  public {}

  function requestData(address originator, string data) external view {
    emit GetData(msg.sender, originator, data);
  }

  function sendData(address sender, address originator, string data) external onlyAdmin {
    OracleRequesterInterface(sender).handleOracleRequestFromOracle(originator, data);
    emit GotData(data);
  }

  function () public {
    revert();
  }
}