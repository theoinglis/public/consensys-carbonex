pragma solidity ^0.4.24;


interface OracleProcessorInterface {

  /** @dev Event emitted when a request for data is made.
    * @param sender The address of the contract requesting the data.
    * @param originator The address of the user who initially requested the data.
    * @param data The data request to be made.
    */
  event GetData(address sender, address originator, string data);

  /** @dev Event emitted when the data has been retrieved.
    * @param data The data retrieved.
    */
  event GotData(string data);

  /** @dev Requests data to an external api.
    * @param originator The address of the user who initially requested the data.
    * @param data The data to be retrieved.
    */
  function requestData(address originator, string data) external view;

  /** @dev Sends the requested data back to the sender.
    * @param sender The address of the contract to send the data back to.
    * @param originator The address of the user who initially requested the data.
    * @param data The data that was retrieved.
    */
  function sendData(address sender, address originator, string data) external;
}