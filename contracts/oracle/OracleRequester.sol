pragma solidity ^0.4.24;

import "../support/registry/HasRegistry.sol";
import "./OracleProcessorInterface.sol";
import "./OracleRequesterInterface.sol";

contract OracleRequester is OracleRequesterInterface, HasRegistry {

  modifier onlyFromOracle() {
    require(msg.sender == registry.addr("oracle.carbonex.eth"));
    _;
  }
  function getOracle() internal view returns(OracleProcessorInterface) {
    return OracleProcessorInterface(registry.addr("oracle.carbonex.eth"));
  }
  function sendOracleRequest(string data) internal view {
    getOracle().requestData(msg.sender, data);
  }
  function handleOracleRequest(address orignator, string data) internal view;
  function handleOracleRequestFromOracle(address orignator, string data) external view onlyFromOracle {
    handleOracleRequest(orignator, data);
  }
}
