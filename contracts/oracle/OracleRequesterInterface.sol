pragma solidity ^0.4.24;

import "./OracleProcessorInterface.sol";

interface OracleRequesterInterface {
  
  /** @dev The function that receieves the data from the oracle.
    * @param originator The address of the user who initially requested the data.
    * @param data The data that was retrieved.
    */
  function handleOracleRequestFromOracle(address originator, string data) external view;
}
