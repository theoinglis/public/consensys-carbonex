pragma solidity ^0.4.24;

import "zeppelin/ownership/Ownable.sol";

import "../support/AdminRole.sol";

import "./CxMinterStorage.sol";
import "./CxBrokerStorage.sol";

contract CxStorage is Ownable, AdminRole, CxMinterStorage, CxBrokerStorage {
 
  function () public {
    revert();
  }
}
