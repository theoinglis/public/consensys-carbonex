pragma solidity ^0.4.24;

import "zeppelin/ownership/Ownable.sol";
import "../support/AdminRole.sol";
import "../sale/CxBrokerLibrary.sol";

contract CxBrokerStorage is Ownable, AdminRole {
  using CxBrokerLibrary for CxBrokerLibrary.Broker;

  CxBrokerLibrary.Broker private broker;

  function addSaleToAccount(address saleAddress) external view onlyAdmin {
    broker.sales.push(saleAddress);
    broker.userSales[msg.sender].push(saleAddress);
  }

  function () public {
    revert();
  }
}
