pragma solidity ^0.4.24;

import "zeppelin/ownership/Ownable.sol";
import "../support/AdminRole.sol";
import "../mint/CxMintingLibrary.sol";

contract CxMinterStorage is Ownable, AdminRole {
  using CxMintingLibrary for CxMintingLibrary.Minted;

  CxMintingLibrary.Minted private minted;

  function getMinter() internal view returns(CxMintingLibrary.Minted) {
    return minted;
  }

  function addCoin(address userAddress, address coinAddress) public view onlyAdmin {
    minted.coins.push(coinAddress);
    addCoinToAccount(userAddress, coinAddress);
  }
  function addCoinToAccount(address userAddress, address coinAddress) public view onlyAdmin {
    minted.userCoins[userAddress].push(coinAddress);
  }

  function () public {
    revert();
  }
}
