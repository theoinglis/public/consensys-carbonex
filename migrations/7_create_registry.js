const Categories = artifacts.require(`./support/Categories.sol`);
const EnsResolver = artifacts.require(`./support/registry/EnsResolver.sol`);
const CxMinter = artifacts.require(`./mint/CxMinter.sol`);
const CxBroker = artifacts.require(`./sale/CxBroker.sol`);
const CxStorage = artifacts.require(`./storage/CxStorage.sol`);
const OracleProcessor = artifacts.require(`./oracle/OracleProcessor.sol`);
const createEnsInstance = require(`./helpers/ens/createEns`);
const getResolver = require(`./helpers/ens/getResolverForName`);

module.exports = async (deployer, network, accounts) => {
  let resolverPromise;
  if (network === `live`) {
    // Use ENS resolver on mainnet
    const resolverAddress = getResolver(web3, `carbonex.eth`);
    resolverPromise = deployer.deploy()
      .then(() => {
        return EnsResolver(resolverAddress);
      });
  } else {
    // Deploy own ENS instance
    resolverPromise = deployer.deploy(EnsResolver)
      .then(async (resolver) => {
        await createEnsInstance(web3, resolver, `carbonex`, accounts[0]);
        return resolver;
      });
  }

  return resolverPromise
    .then(async (resolver) => {
      await deployer.deploy(CxMinter, resolver.address);
      await deployer.deploy(CxBroker, resolver.address);
      await deployer.deploy(OracleProcessor, resolver.address);
      resolver.setAddr(`categories.carbonex.eth`, Categories.address);
      resolver.setAddr(`storage.carbonex.eth`, CxStorage.address);
      resolver.setAddr(`minter.carbonex.eth`, CxMinter.address);
      resolver.setAddr(`broker.carbonex.eth`, CxBroker.address);
      resolver.setAddr(`oracle.carbonex.eth`, OracleProcessor.address);
    });
};
