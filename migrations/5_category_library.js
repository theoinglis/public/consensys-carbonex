const CategoryLibrary = artifacts.require(`./support/categories/CategoryLibrary.sol`);
const Categories = artifacts.require(`./support/categories/Categories.sol`);
const String32ListLibrary = artifacts.require(`./support/utils/String32ListLibrary.sol`);

module.exports = async (deployer) => {
  deployer.link(String32ListLibrary, CategoryLibrary);
  return deployer.deploy(CategoryLibrary)
    .then(() => {
      deployer.link(CategoryLibrary, Categories);
      return deployer.deploy(Categories)
        .then((categories) => {
          categories.addCategory(
            `variant`,
            [
              `VER`,
              `CER`,
            ],
          );
          categories.addCategory(
            `region`,
            [
              `UK`,
              `USA`,
              `AUS`,
            ],
          );
          categories.addCategory(
            `method`,
            [
              `Solar`,
              `Wind`,
            ],
          );
        });
    });
};
