const Chance = require(`chance`);

const chance = new Chance();
const WEI = 1000000000000000000;


module.exports = (minter, broker, CbnCoin, [owner, user1, user2, user3]) => {
  const random = {
    user: () => {
      return chance.pickone([user1, user2, user3]);
    },
    intAmount: ({ min = 0, max = 999, ...rest }) => {
      return chance.integer({ min, max, ...rest });
    },
    floatAmount: (c) => {
      return chance.floating(c);
    },
    string: (c) => {
      return chance.string(c);
    },
    variant: () => {
      return chance.pickone([`VER`, `CER`]);
    },
    region: () => {
      return chance.pickone([`UK`, `USA`, `AUS`]);
    },
    method: () => {
      return chance.pickone([`Solar`, `Wind`]);
    },
  };
  return {
    random,
    requestCarbonCreditMinting: async (userAddress = random.user()) => {
      const data = {
        amount: random.intAmount({ min: 200, max: 999 }),
        variant: random.variant(),
        region: random.region(),
        method: random.method(),
        ipfs: random.string({ length: 10 }),
        userAddress,
      };
      const {
        logs,
      } = await minter.requestCarbonCreditMinting(
        data.amount,
        data.variant,
        data.region,
        data.method,
        data.ipfs,
        { from: data.userAddress },
      );
      const coinAddress = logs[0].args.coinAddress;
      return {
        data,
        coinAddress,
      };
    },
    putCarbonCreditsUpForSale: async (coinData) => {
      const {
        userAddress,
      } = coinData.data;
      const {
        coinAddress,
      } = coinData;
      const coin = await CbnCoin.at(coinAddress);
      const price = random.floatAmount({ min: 0, max: 1, fixed: 5 }) * WEI;
      const data = {
        amount: random.intAmount({ min: 1, max: coinData.data.amount }),
        pricePerCoin: price,
      };

      await coin.approve(broker.address, data.amount, { from: userAddress });
      await broker.putCarbonCreditsUpForSale(coinAddress, data.amount, data.pricePerCoin, { from: userAddress });
      return {
        data,
        coinData,
        coinAddress,
      };
    },
  };
};
