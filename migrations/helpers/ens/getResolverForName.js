const Ens = require(`ethereum-ens`);

module.exports = async (web3, name) => {
  const ens = new Ens(web3.currentProvider);
  return ens.resolver(name);
};
