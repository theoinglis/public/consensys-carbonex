const namehash = require(`eth-ens-namehash`);

const ensAbi = require(`./ensAbi.json`);
const reigstrarAbi = require(`./registrarAbi.json`);

const createEnsContract = (web3, account) => {
  const ensContract = web3.eth.contract(ensAbi);
  return new Promise((resolve, reject) => {
    ensContract.new({
      from: account,
      data: `0x33600060000155610220806100146000396000f3630178b8bf60e060020a600035041415610023576020600435015460405260206040f35b6302571be360e060020a600035041415610047576000600435015460405260206040f35b6316a25cbd60e060020a60003504141561006b576040600435015460405260206040f35b635b0fc9c360e060020a6000350414156100b8576000600435015433141515610092576002565b6024356000600435015560243560405260043560198061020760003960002060206040a2005b6306ab592360e060020a6000350414156101165760006004350154331415156100df576002565b6044356000600435600052602435602052604060002001556044356040526024356004356021806101e660003960002060206040a3005b631896f70a60e060020a60003504141561016357600060043501543314151561013d576002565b60243560206004350155602435604052600435601c806101ca60003960002060206040a2005b6314ab903860e060020a6000350414156101b057600060043501543314151561018a576002565b602435604060043501556024356040526004356016806101b460003960002060206040a2005b6002564e657754544c28627974657333322c75696e743634294e65775265736f6c76657228627974657333322c61646472657373294e65774f776e657228627974657333322c627974657333322c61646472657373295472616e7366657228627974657333322c6164647265737329`,
      gas: 4700000,
    }, (e, contract) => {
      if (e) return reject(e);
      else if (typeof contract.address !== `undefined`) {
        console.log(`Contract mined! address: ${contract.address} transactionHash: ${contract.transactionHash}`);
        resolve(contract);
      } else {
        console.log(`ENS Contract not mined yet`);
      }
    });
  });
};

const createRegistrarContract = (web3, account, ens) => {
  const registrarContract = web3.eth.contract(reigstrarAbi);
  return new Promise((resolve, reject) => {
    registrarContract.new(
      ens.address,
      0,
      0,
      {
        from: account,
        data: `0x60606040818152806101c4833960a0905251608051600080546c0100000000000000000000000080850204600160a060020a0319909116179055600181905550506101768061004e6000396000f3606060405260e060020a6000350463d22057a9811461001e575b610002565b34610002576100f4600435602435600154604080519182526020808301859052815192839003820183206000805494830181905283517f02571be3000000000000000000000000000000000000000000000000000000008152600481018390529351879592949193600160a060020a03909316926302571be3926024808201939182900301818787803b156100025760325a03f11561000257505060405151915050600160a060020a038116158015906100ea575033600160a060020a031681600160a060020a031614155b156100f657610002565b005b60008054600154604080517f06ab5923000000000000000000000000000000000000000000000000000000008152600481019290925260248201899052600160a060020a03888116604484015290519216926306ab59239260648084019382900301818387803b156100025760325a03f11561000257505050505050505056`,
        gas: 4700000,
      }, (e, contract) => {
        if (e) return reject(e);
        else if (typeof contract.address !== `undefined`) {
          console.log(`Contract mined! address: ${contract.address} transactionHash: ${contract.transactionHash}`);
          resolve(contract);
        } else {
          console.log(`Registrar Contract not mined yet`);
        }
      },
    );
  });
};

module.exports = async (web3, resolver, name, account) => {
  const ens = await createEnsContract(web3, account);
  const registrar = await createRegistrarContract(web3, account, ens);
  // Set the registrar as the owner of ens
  console.log('setting the owner');
  await new Promise((resolve, reject) => {
    ens.setOwner(0, registrar.address, { from: account }, (e) => {
      if (e) return reject(e);
      else return resolve();
    });
  });
  // Set the owner of the name
  console.log('registring the registrar');
  await new Promise((resolve, reject) => {
    registrar.register(web3.sha3(name), account, { from: account }, (e) => {
      if (e) return reject(e);
      else return resolve();
    });
  });
  // Set the resolver
  console.log('setting the resolver');
  await new Promise((resolve, reject) => {
    ens.setResolver(namehash.hash(name), resolver.address, { from: account }, (e) => {
      if (e) return reject(e);
      else return resolve();
    });
  });
};
