const CxStorage = artifacts.require(`./storage/CxStorage.sol`);
const CxMinter = artifacts.require(`./mint/CxMinter.sol`);
const CxBroker = artifacts.require(`./sale/CxBroker.sol`);

module.exports = (deployer) => {
  return deployer.then(() => {
    return CxStorage.deployed();
  }).then((storage) => {
    storage.setAdmin(CxMinter.address, true);
    storage.setAdmin(CxBroker.address, true);
  });
};
