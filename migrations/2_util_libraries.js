const StringConversion = artifacts.require(`./support/utils/StringConversion.sol`);
const StringManipulation = artifacts.require(`./support/utils/StringManipulation.sol`);
const String32ListLibrary = artifacts.require(`./support/utils/String32ListLibrary.sol`);

module.exports = (deployer) => {
  return deployer.deploy(StringConversion)
    .then(() => {
      return deployer.deploy(StringManipulation);
    })
    .then(() => {
      deployer.link(StringConversion, String32ListLibrary);
      return deployer.deploy(String32ListLibrary);
    });
};
