const CxMinter = artifacts.require(`./mint/CxMinter.sol`);
const CbnCoin = artifacts.require(`./mint/CbnCoin.sol`);
const CxBroker = artifacts.require(`./sale/CxBroker.sol`);
const dataCreator = require(`./helpers/data_creator`);


module.exports = (deployer, network, accounts) => {
  if (network === `development`) {
    // Create seed data for demo purposes
    return deployer
      .then(() => {
        return CxMinter.deployed();
      }).then((minter) => {
        return CxBroker.deployed()
          .then(async (broker) => {
            const requests = 10;
            const minted = 7;
            const forSale = 3;
            const admin = accounts[0];
            const data = dataCreator(minter, broker, CbnCoin, accounts);
            const coins = [];
            console.log(`Creating ${requests} requests ...`);
            for (var reqNo = 0; reqNo < requests; reqNo++) {
              coins.push(await data.requestCarbonCreditMinting());
            }
            console.log(`Minting ${minted} coins ...`);
            for (var mintNo = 0; mintNo < minted; mintNo++) {
              const coin = coins[mintNo];
              await minter.mintCarbonCredit(coin.coinAddress, { from: admin });
            }
            console.log(`Selling ${forSale} ...`);
            for (var saleNo = 0; saleNo < forSale; saleNo++) {
              const coin = coins[saleNo];
              await data.putCarbonCreditsUpForSale(coin);
            }
            console.log(`Data initialised`)
          });
      });
  }
};
