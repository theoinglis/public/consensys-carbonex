const CbnCoin = artifacts.require(`./CbnCoin.sol`);
const CxMinter = artifacts.require(`./CxMinter.sol`);

contract(`CxMinter`, (accounts) => {
  let minter;
  const volume = 100;
  const ipfsHash = `fdhjjfdkdfjs`;
  const variant = `VER`;
  const method = `Solar`;
  const region = `UK`;
  const [
    admin,
    owner,
  ] = accounts;

  beforeEach(`setup contract`, async () => {
    minter = await CxMinter.deployed();
  });

  describe(`request mint`, () => {
    it(`can work`, async () => {
      await minter.requestCarbonCreditMinting(volume, variant, region, method, ipfsHash, { from: owner });

      assert.isOk(true);
    });

    it(`there are more coins after`, async () => {
      const coinsBefore = await minter.getCoins();
      await minter.requestCarbonCreditMinting(volume, variant, region, method, ipfsHash, { from: owner });
      const coinsAfter = await minter.getCoins();

      assert.equal(coinsBefore.length + 1, coinsAfter.length);
    });
    it(`the account has more coins`, async () => {
      const coinsBefore = await minter.getMyCoins({ from: owner });
      await minter.requestCarbonCreditMinting(volume, variant, region, method, ipfsHash, { from: owner });
      const coinsAfter = await minter.getMyCoins({ from: owner });

      assert.equal(coinsBefore.length + 1, coinsAfter.length);
    });
    it(`the account has more coins`, async () => {
      const coinsBefore = await minter.getMyCoins({ from: owner });
      await minter.requestCarbonCreditMinting(volume, variant, region, method, ipfsHash, { from: owner });
      const coinsAfter = await minter.getMyCoins({ from: owner });

      assert.equal(coinsBefore.length + 1, coinsAfter.length);
    });
  });
  describe(`do mint`, () => {
    let coinAddress;
    beforeEach(`request carbon credit`, async () => {
      const {
        logs,
      } = await minter.requestCarbonCreditMinting(volume, variant, region, method, ipfsHash, { from: owner });
      coinAddress = logs[0].args.coinAddress;
    });
    it(`can work`, async () => {
      await minter.mintCarbonCredit(coinAddress, { from: admin });

      assert.isOk(true);
    });
    it(`can't mint carbon credit if not admin`, async () => {
      try {
        await minter.mintCarbonCredit(coinAddress, { from: owner });
      } catch (e) {
        return assert.isOk(true);
      }
      return assert.isOk(false);
    });
  });
});
