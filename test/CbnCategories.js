const Categories = artifacts.require(`./support/categories/Categories.sol`);

contract(`Categories`, () => {
  let categories;

  beforeEach(`setup contract`, async () => {
    categories = await Categories.deployed();
  });

  describe(`on created`, () => {
    it(`has category`, async () => {
      const regionList = await categories.getCategoryOptions(`region`);
      const regionstringlist = regionList.map((l) => web3.toUtf8(l));

      assert.equal(regionstringlist.length, 3);
    });
    it(`is category`, async () => {
      const isOption = await categories.isCategoryOption(`region`, `UK`);
      const isNotOption = await categories.isCategoryOption(`region`, `FDJKL`);

      assert.isOk(isOption);
      assert.isNotOk(isNotOption);
    });
  });
});
