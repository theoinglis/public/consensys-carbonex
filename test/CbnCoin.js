
const EnsResolver = artifacts.require(`./support/registry/EnsResolver.sol`);
const CbnCoin = artifacts.require(`./CbnCoin.sol`);
const OracleProcessor = artifacts.require(`./oracle/OracleProcessor.sol`);

contract(`CbnCoin`, (accounts) => {
  let coin;
  let registry;
  const volume = 999;
  const ipfsHash = `fdhjjfdkdfjs`;
  const variant = `VER`;
  const region = `UK`;
  const method = `Solar`;
  const [
    minter,
    owner,
    other,
  ] = accounts;

  beforeEach(`setup contract`, async () => {
    registry = await EnsResolver.deployed();
    coin = await CbnCoin.new(registry.address, owner, volume, variant, region, method, ipfsHash, { from: minter });
  });

  const listen = async () => {
    const oracle = await OracleProcessor.deployed();
    oracle.GetData()
      .watch(async (err, info) => {
        const {
          sender,
          originator,
          data
        } = info.args;
        await oracle.sendData(sender, originator, `it worked!`);
      });
  }

  describe(`on creating`, () => {
    it(`totalSupply should be the same as the volume`, async () => {
      assert.equal(await coin.totalSupply(), volume);
    });
    it(`the owner is not assigned any coins`, async () => {
      assert.equal(await coin.balanceOf(owner), 0);
    });
    it(`throws error if not a valid variant`, async () => {
      try {
        await CbnCoin.new(registry.address, owner, volume, `XXX`, region, method, ipfsHash, { from: minter });
      } catch (e) {
        return assert.isOk(true);
      }
      assert.isOk(false);
    });
    it(`throws error if not a valid region`, async () => {
      try {
        await CbnCoin.new(registry.address, owner, volume, variant, `XXX`, method, ipfsHash, { from: minter });
      } catch (e) {
        return assert.isOk(true);
      }
      assert.isOk(false);
    });
    it(`throws error if not a valid method`, async () => {
      try {
        await CbnCoin.new(registry.address, owner, volume, variant, region, `XXX`, ipfsHash, { from: minter });
      } catch (e) {
        return assert.isOk(true);
      }
      assert.isOk(false);
    });
  });

  describe(`on minting`, () => {
    it(`only admin users can mint coins`, async () => {
      try {
        await coin.mint({ from: other });
      } catch (e) {
        return assert.isOk(true);
      }
      assert.isOk(false);
    });
    it(`the owner is assigned all initial credits`, async () => {
      await coin.mint({ from: minter });

      assert.equal(await coin.balanceOf(owner), volume);
    });
    it(`can't mint a second time`, async () => {
      await coin.mint({ from: minter });
      try {
        await coin.mint({ from: minter });
      } catch (e) {
        return assert.isOk(true);
      }
      assert.isOk(false);
    });
    it(`the Minted event is emitted`);
  });

  describe(`on expire`, () => {
    const expire = 200;
    beforeEach(`setup contract`, async () => {
      await coin.mint({ from: minter });
    });

    it(`the owner has their balance reduced`, async () => {
      await coin.expire(owner, expire, { from: minter });

      assert.equal(await coin.balanceOf(owner), volume - expire);
    });
    it(`the remainingSupply is reduced`, async () => {
      await coin.expire(owner, expire, { from: minter });

      assert.equal(await coin.remainingSupply(), volume - expire);
    });
    it(`the expired amount is added`, async () => {
      await coin.expire(owner, expire, { from: minter });

      assert.equal(await coin.expired(owner), expire);
    });
    it(`the Expired event is emitted`);
    it(`only the minter can expire the coins`, async () => {
      try {
        await coin.expire(owner, expire, { from: owner });
      } catch (e) {
        return assert.isOk(true);
      }
      assert.isOk(false);
    });
    it(`can't expire greater than their balance`, async () => {
      try {
        const currentBalance = await coin.balanceOf(owner);
        await coin.expire(owner, currentBalance * 2, { from: owner });
      } catch (e) {
        return assert.isOk(true);
      }
      assert.isOk(false);
    });

    it(`coin gets ipfs`, async () => {
      await listen();
      const retrievedData = new Promise((resolve, reject) => {
        coin.RetrievedIpfsData()
          .watch((err, data) => {
            if (err) reject(err);
            resolve(data);
          })
      });
      await coin.getIpfsData({ from: minter });
      await retrievedData;
      assert.isOk(true);
    });
  });
});
