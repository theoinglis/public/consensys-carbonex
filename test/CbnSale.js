const EnsResolver = artifacts.require(`./support/registry/EnsResolver.sol`);
const CxMinter = artifacts.require(`./CxMinter.sol`);
const CbnSale = artifacts.require(`./CbnSale.sol`);
const CbnCoin = artifacts.require(`./CbnCoin.sol`);

contract(`CbnSale`, (accounts) => {
  let coin;
  let registry;
  let sale;
  let cxMinter;
  const volume = 999;
  const ipfsHash = `fdhjjfdkdfjs`;
  const variant = `VER`;
  const region = `UK`;
  const method = `Solar`;

  const sellAmount = 400;
  const pricePerCoinInEth = 0.000000001;
  const pricePerCoinInWei = web3.toWei(pricePerCoinInEth, `ether`);
  const [
    minter,
    seller,
    buyer,
  ] = accounts;

  beforeEach(`setup contract`, async () => {
    registry = await EnsResolver.deployed();
    cxMinter = await CxMinter.deployed();
    const {
      logs,
    } = await cxMinter.requestCarbonCreditMinting(volume, variant, region, method, ipfsHash, { from: seller });
    const coinAddress = logs[0].args.coinAddress;
    await cxMinter.mintCarbonCredit(coinAddress, { from: minter });
    coin = await CbnCoin.at(coinAddress);
    await coin.approve(minter, sellAmount, { from: seller });
    sale = await CbnSale.new(registry.address, seller, coin.address, sellAmount, pricePerCoinInWei, { from: minter });
  });

  describe(`on creating`, () => {
    it(`the properties are set correctly`, async () => {
      assert.equal(await sale.sellerAddress(), seller, `Coin seller not propertly set`);
      assert.equal(await sale.coinAddress(), coin.address, `Coin address not propertly set`);
      assert.equal(await sale.totalAmount(), sellAmount, `The number of coins to sell isn't correct`);
      assert.equal(await sale.pricePerCoin(), pricePerCoinInWei, `The price per coin wasn't set correctly`);
    });
    it(`the remaining amount starts as 0`, async () => {
      assert.equal(await sale.getCoinsAvailable(), 0, `The number of coins available should start with 0`);
    });

    it(`can't create the contract hasn't approved the minter to transfer the coins`, async () => {
      try {
        await CbnSale.new(registry.address, seller, coin.address, volume + 1, pricePerCoinInWei, { from: minter });
      } catch (e) {
        return assert.isOk(true);
      }
      assert.isOk(false);
    });
  });

  describe(`on verifyCarbonCreditsTransferred`, () => {
    beforeEach(`approve coin sale`, async () => {
      await coin.transferFrom(seller, sale.address, sellAmount, { from: minter });
    });

    it(`the remaining amount is filled`, async () => {
      await sale.verifyCarbonCreditsTransferred({ from: minter });

      assert.equal(await sale.getCoinsAvailable(), sellAmount, `The number of coins available is not full`);
    });

    it(`the remaining amount is filled`, async () => {
      await sale.verifyCarbonCreditsTransferred({ from: minter });

      const sellerHas = await coin.balanceOf(seller);
      const saleContractHas = await coin.balanceOf(sale.address);

      assert.equal(sellerHas, volume - sellAmount, `The seller has not had the required tokens transferred`);
      assert.equal(saleContractHas, sellAmount, `The sale contract hasn't been transferred the required tokens`);
    });

    it(`can't buy coins if carbon credits haven't been transferred`, async () => {
      const buyAmount = 10;
      try {
        await sale.buy(buyAmount, { from: buyer, value: buyAmount * pricePerCoinInWei });
      } catch (e) {
        return assert.isOk(true);
      }
      assert.isOk(false);
    });
    it(`can't buyRemaining coins if carbon credits haven't been transferred`, async () => {
      try {
        await sale.buyRemaining({ from: buyer, value: sellAmount * pricePerCoinInWei });
      } catch (e) {
        return assert.isOk(true);
      }
      assert.isOk(false);
    });
  });

  describe(`on buy`, () => {
    const buyAmount = 10;
    beforeEach(`approve coin sale`, async () => {
      await coin.transferFrom(seller, sale.address, sellAmount, { from: minter });
      await sale.verifyCarbonCreditsTransferred({ from: minter });
    });

    it(`can buy coins`, async () => {
      await sale.buy(buyAmount, { from: buyer, value: buyAmount * pricePerCoinInWei });

      assert.isOk(true, `Transaction was successful`);
    });
    it(`remainingAmount reduces by the amount bought`, async () => {
      await sale.buy(buyAmount, { from: buyer, value: buyAmount * pricePerCoinInWei });

      assert.equal(await sale.getCoinsAvailable(), sellAmount - buyAmount, `The number of coins available is less the bought amount`);
    });
    it(`remainingAmount reduces by the amount bought`, async () => {
      await sale.buy(buyAmount, { from: buyer, value: buyAmount * pricePerCoinInWei });

      const boughtCarbonCredits = await coin.balanceOf(buyer);

      assert.equal(boughtCarbonCredits, buyAmount, `The buyer has the right number of carbon credits`);
    });

    it(`seller has the correct balance`, async () => {
      await sale.buy(buyAmount, { from: buyer, value: buyAmount * pricePerCoinInWei });

      const balance = await sale.payments(seller);

      assert.equal(balance, buyAmount * pricePerCoinInWei, `The seller has the right balance`);
    });

    it(`can't buy more coins than being sold`, async () => {
      try {
        await sale.buy(sellAmount + 1, { from: buyer, value: (sellAmount + 1) * pricePerCoinInWei });
      } catch (e) {
        return assert.isOk(true);
      }
      assert.isOk(false);
    });
    it(`can't buy with insufficient balance`, async () => {
      try {
        await sale.buy(buyAmount, { from: buyer, value: (buyAmount * pricePerCoinInWei) - 1 });
      } catch (e) {
        return assert.isOk(true);
      }
      assert.isOk(false);
    });

    it(`coins added to users account`, async () => {
      const wasCoins = await cxMinter.getMyCoins({ from: buyer });
      await sale.buy(buyAmount, { from: buyer, value: buyAmount * pricePerCoinInWei });
      const isCoins = await cxMinter.getMyCoins({ from: buyer });

      assert.isTrue(wasCoins.indexOf(coin.address) < 0, `The buyer already had the coin in their account`);
      assert.isTrue(isCoins.indexOf(coin.address) >= 0, `The buyer didn't have the coin added to their account`);
    });
  });
  describe(`on buyRemaining`, () => {
    const buyValue = sellAmount * pricePerCoinInWei;

    beforeEach(`approve coin sale`, async () => {
      await coin.transferFrom(seller, sale.address, sellAmount, { from: minter });
      await sale.verifyCarbonCreditsTransferred({ from: minter });
    });

    it(`can buy remaining coins`, async () => {
      await sale.buyRemaining({ from: buyer, value: sellAmount * pricePerCoinInWei });

      assert.isOk(true, `Transaction was successful`);
    });

    it(`the sellers sale balance should have the buy amount added`, async () => {
      const initialBalance = await sale.getMyBalance({ from: seller });

      await sale.buyRemaining({ from: buyer, value: buyValue });

      const afterBalance = await sale.getMyBalance({ from: seller });
      assert.isTrue(initialBalance.add(buyValue).eq(afterBalance), `The sellers balance didn't have the buy amount added`);
    });
    it(`the buyer should have all the carbon credits for sale`, async () => {
      await sale.buyRemaining({ from: buyer, value: buyValue });

      assert.equal(await coin.balanceOf(buyer), sellAmount, `The buyer doesn't have the correct number of carbon credits`);
    });
    it(`the seller has the right number of carbon credits`, async () => {
      await sale.buyRemaining({ from: buyer, value: buyValue });

      assert.equal(await coin.balanceOf(seller), volume - sellAmount, `The sller doesn't have the correct number of carbon credits`);
    });
    it(`the contract doesn't have any more carbon credits`, async () => {
      await sale.buyRemaining({ from: buyer, value: buyValue });

      assert.equal(await coin.balanceOf(sale.address), 0, `The sller doesn't have the correct number of carbon credits`);
    });
  });
  describe(`on withdrawPayments`, () => {
    const buyAmount = 10;
    beforeEach(`approve coin sale and have buyer`, async () => {
      await coin.transferFrom(seller, sale.address, sellAmount, { from: minter });
      await sale.verifyCarbonCreditsTransferred({ from: minter });
      await sale.buy(buyAmount, { from: buyer, value: buyAmount * pricePerCoinInWei });
    });

    it(`can withdraw`, async () => {
      await sale.withdrawPayments({ from: seller });
      assert.isOk(true, `Transaction was successful`);
    });
  })
});
