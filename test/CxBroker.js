const CbnCoin = artifacts.require(`./CbnCoin.sol`);
const CxMinter = artifacts.require(`./CxMinter.sol`);
const CxBroker = artifacts.require(`./CxBroker.sol`);

contract(`CxBroker`, (accounts) => {
  let minter, broker;
  const volume = 100;
  const ipfsHash = `fdhjjfdkdfjs`;
  const variant = `VER`;
  const method = `Solar`;
  const region = `UK`;
  const [
    admin,
    owner,
  ] = accounts;

  beforeEach(`setup contract`, async () => {
    minter = await CxMinter.deployed();
    broker = await CxBroker.deployed();
  });

  describe(`putCarbonCreditsUpForSale`, () => {
    let coinAddress;
    const saleAmount = 20;
    const salePrice = 0.001;

    beforeEach(`approve transfer`, async () => {
      const {
        logs,
      } = await minter.requestCarbonCreditMinting(volume, variant, region, method, ipfsHash, { from: owner });
      coinAddress = logs[0].args.coinAddress;
      await minter.mintCarbonCredit(coinAddress);
      const coin = await CbnCoin.at(coinAddress);
      await coin.approve(broker.address, saleAmount, { from: owner });
    });
    it(`can work`, async () => {
      await broker.putCarbonCreditsUpForSale(coinAddress, saleAmount, salePrice, { from: owner });
      assert.isOk(true);
    });
    it(`is added to the sales`, async () => {
      const salesBefore = await broker.getSales();
      await broker.putCarbonCreditsUpForSale(coinAddress, saleAmount, salePrice, { from: owner });
      const salesAfter = await broker.getSales();
      assert.equal(salesBefore.length + 1, salesAfter.length);
    });
  });
});
