var HDWalletProvider = require("truffle-hdwallet-provider");

module.exports = {
  // See <http://truffleframework.com/docs/advanced/configuration>
  // to customize your Truffle configuration!
  networks: {
    development: {
      host: `localhost`,
      port: 8545,
      gasPrice: 3000000000,
      network_id: `*`, // Match any network id
    },
    test: {
      host: `localhost`,
      port: 8545,
      network_id: `*`, // Match any network id
    },
    staging: {
      network_id: 3,
      gas: 8000000,
      provider: function() {
        return new HDWalletProvider(process.env.WALLET_MNEMONIC_ROPSTEN, `https://ropsten.infura.io/v3/${process.env.INFURA_API_KEY}`)
      }
    },
    rinkeby: {
      network_id: 4,
      provider: function () {
        return new HDWalletProvider(process.env.WALLET_MNEMONIC_RINKEBY, `https://rinkeby.infura.io/v3/${process.env.INFURA_API_KEY}`)
      }
    }
  },
};
