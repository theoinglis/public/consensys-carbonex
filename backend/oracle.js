import Web3 from 'web3';
import contract from 'truffle-contract';
import OracleProcessor from '../build/contracts/OracleProcessor.json';

const defaultProvider = new Web3.providers.HttpProvider(`http://localhost:8545`);

const web3 = new Web3(defaultProvider);
const oracleContract = contract(OracleProcessor);
oracleContract.setProvider(defaultProvider);

oracleContract.deployed()
  .then((oracle) => {
    console.log(`Oracle waiting for data ...`);
    web3.eth.getAccounts((accountError, [admin]) => {
      oracle.GetData()
        .watch(async (err, info) => {
          const {
            sender,
            originator,
            data
          } = info.args;
          setTimeout(() => { // Simulate api call
            oracle.sendData(sender, originator, `some retrieved answer ...`, { from: admin });
          }, 2000);
        });
    });
  });
