import config from 'config';

const variables = config.env !== `test`
  ? {
    colorPrimary: `rgb(29,28,27)`,
    colorPrimaryComplementary: `#999999`,
    colorAccent: `rgb(50,232,183)`,
    colorAccentComplementary: `black`,
  
    colorBackgroundDark: `rgb(16, 16, 14)`,
    colorBackgroundLight: `rgb(29, 28, 27)`,
  
    heightHeader: `80px`,
    maxPageWidth: `860px`,
  
    paddingSection: `20px 12px 30px`,
  
    transitionDefault: `all 0.4s $ease-out-quart`,
    transitionDefaultDelayed: `all 0.4s $ease-out-quart 0.2s`,
  
    fontPrimary: `'Open Sans', sans-serif`,
    fontAccent: `'Open Sans', sans-serif`,
  
    easeInCubic: `cubic-bezier(0.55, 0.055, 0.675, 0.19)`,
    easeOutCubic: `cubic-bezier(0.215, 0.61, 0.355, 1)`,
    easeInOutCubic: `cubic-bezier(0.645, 0.045, 0.355, 1)`,
    easeInQuart: `cubic-bezier(0.895, 0.03, 0.685, 0.22)`,
    easeOutQuart: `cubic-bezier(0.165, 0.84, 0.44, 1)`,
    easeInOutQuart: `cubic-bezier(0.77, 0, 0.175, 1)`,
    easeOutQuint: `cubic-bezier(0.23, 1, 0.32, 1)`,
    easeInQuint: `cubic-bezier(0.755, 0.05, 0.855, 0.06)`,
    easeInOutQuint: `cubic-bezier(0.86, 0, 0.07, 1)`,
    easeOutExpo: `cubic-bezier(0.19, 1, 0.22, 1)`,
    easeInExpo: `cubic-bezier(0.95, 0.05, 0.795, 0.035)`,
    easeInOutExpo: `cubic-bezier(1, 0, 0, 1)`,
  
    sizeTiny: `320px`,
    sizeExtraSmall: `480px`,
    sizeSmall: `768px`,
    sizeMedium: `992px`,
    sizeLarge: `1200px`,
  
    positionBack: 9,
    positionMiddle: 99,
    positionFront: 999,
  } : {
    sizeTiny: 9999,
    sizeSmall: 9999,
    sizeMedium: 9999,
    sizeLarge: 9999,
  };

export default variables;
