import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import classnames from 'classnames';
import CloseIcon from '../CloseIcon';

import styles from './styles.css';

export default class Menu extends Component {
  static propTypes = {
    closeMenuFunc: PropTypes.func,
    isMenuOpen: PropTypes.bool,
    links: PropTypes.array,
  }

  constructor(props) {
    super(props);
    this.state = {
      prevOverflow: ``,
    };
  }

  componentWillReceiveProps = (nextProps) => {
    if (nextProps.isMenuOpen && (nextProps.isMenuOpen !== this.props.isMenuOpen)) {
      this.setState({ prevOverflow: document.documentElement.style.cssText });
      document.documentElement.style.cssText = `overflow-y: hidden`;
    }
  }

  closeMenuWrapper = (parentCloseFunc, link) => () => {
    const el = document.getElementById(`navMenu`);
    document.documentElement.style.cssText = this.state.prevOverflow;
    if (link !== `/`) {
      el.setAttribute(`style`, `transition: none;`);
      setTimeout(() => el.setAttribute(`style`, ``), 1000);
    }
    parentCloseFunc();
  }

  renderCloseButton = isActive => (
    <div
      className={classnames(styles.closeButton, isActive)}
      onClick={this.closeMenuWrapper(this.props.closeMenuFunc)}
    >
      <CloseIcon />
    </div>
  )

  renderLinks = ({ linkText, link }) => (
    <Link
      key={link}
      onClick={this.closeMenuWrapper(this.props.closeMenuFunc, link)}
      className={styles.menuItem}
      to={link}
    >
      {linkText}
    </Link>
  )

  render() {
    const activeclass = this.props.isMenuOpen ? styles.active : null;

    return (
      <div className={classnames(styles.menu, activeclass)}>
        {this.renderCloseButton(activeclass)}
        <div className={classnames(styles.menuInner, activeclass)}>
          {this.props.links.map(this.renderLinks)}
        </div>
      </div>
    );
  }
}
