import ReactGA from 'react-ga';
import { LOCATION_CHANGE } from 'react-router-redux';

export function initialise(id) {
  ReactGA.initialize(id);
}

export function reduxMiddleware() {
  const logPageView = (payload) => {
    ReactGA.set({ page: payload.pathname + payload.hash + payload.search });
    ReactGA.pageview(payload.pathname + payload.hash + payload.search);
  };

  return () => next => (action) => {
    if (action.type === LOCATION_CHANGE) {
      logPageView(action.payload);
    }
    return next(action);
  };
}

export default ReactGA;
