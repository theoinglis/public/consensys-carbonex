import LogRocket from 'logrocket';

export function initialise(id) {
  LogRocket.init(id);
}

export function reduxMiddleware() {
  return LogRocket.reduxMiddleware();
}

export default LogRocket;
