import React from 'react';
import PropTypes from 'prop-types';
import Spinner from 'react-spinkit';

const DefaultSpinnerComponent = () => (
  <Spinner fadeIn="half" name="double-bounce" className="spinner" />
);
export default class LoadingIndicator extends React.Component {
  static propTypes = {
    SpinnerComponent: PropTypes.func,
    className: PropTypes.string,
    children: PropTypes.node,
    isLoading: PropTypes.bool,
  };

  static defaultProps = {
    SpinnerComponent: DefaultSpinnerComponent,
    isLoading: true,
  };

  render = () => {
    const {
      SpinnerComponent,
      className,
      children,
    } = this.props;

    return this.props.isLoading
      ? (
        <div className={className}>
          <SpinnerComponent className="spinner" />
          {children && (
            <div className="content">
              {children}
            </div>
          )}
        </div>
      )
      : null;
  }
}
