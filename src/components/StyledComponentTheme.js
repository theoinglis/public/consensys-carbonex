import cssVariables from './StyleVariables';
import { css } from 'styled-components';

const sizes = {
  largeDesktop: cssVariables.sizeLarge || `1200px`,
  desktop: cssVariables.sizeMedium || `992px`,
  tablet: cssVariables.sizeSmall || `768px`,
  phablet: cssVariables.sizeExtraSmall || `480px`,
  phone: cssVariables.sizeTiny || `320px`,
};

const mediaTags = Object.keys(sizes).reduce((acc, label) => {
  acc[label] = (...args) => css`
    @media (min-width: ${sizes[label]}) {
      ${css(...args)}
    }
  `;
  return acc;
}, {});

cssVariables.media = {
  ...mediaTags,
  custom: size => (...args) => css`
      @media (min-width: ${size}) {
        ${css(...args)}
      }
    `
  ,
};

export default cssVariables;
