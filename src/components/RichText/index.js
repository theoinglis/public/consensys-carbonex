import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';

export default class RichText extends Component {
  static propTypes = {
    initialText: PropTypes.string,
    onChangeHandler: PropTypes.func,
  }

  static defaultProps = {
    initialText: ``,
  }

  constructor(props) {
    super(props);
    this.state = { text: props.initialText };
  }

  handleChange = (value) => {
    const { onChangeHandler } = this.props;
    this.setState({ text: value });
    if (onChangeHandler) onChangeHandler(value);
  }

  render() {
    return (
      <ReactQuill
        value={this.state.text}
        onChange={this.handleChange}
      />
    );
  }
}
