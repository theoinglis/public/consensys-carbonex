import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import styles from './styles.css';

export default function FullScreenSection({ className, children }) {
  return (
    <div className={classnames(styles.fullScreenContainer, className)}>
      { children }
    </div>
  );
}

FullScreenSection.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
};
