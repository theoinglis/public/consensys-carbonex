import React from 'react';
import {
  test,
  mount,
  MountableTestComponent,
} from '__tests__/helpers/test-setup';

import CloseIcon from './index';


test.beforeEach((t) => {
  const wrapper = mount(
    <MountableTestComponent routerInitialEntries={[`/`]} authorized>
      <CloseIcon />
    </MountableTestComponent>,
  );
  wrapper.render();
  // eslint-disable-next-line no-param-reassign
  t.context = wrapper;
});

test(`Close Icon exists`, (t) => {
  // eslint-disable-next-line no-param-reassign
  t.true(t.context.contains(<CloseIcon />));
});
