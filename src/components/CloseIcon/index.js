import React, { Component } from 'react';
import styles from './styles.css';

export default class CloseIcon extends Component {
  render() {
    return (
      <div>
        <svg className={styles.svg}>
          <line
            x1="1"
            y1="30"
            x2="30"
            y2="1"
            stroke="currentColor"
            strokeWidth="2"
          />
          <line
            x1="1"
            y1="1"
            x2="30"
            y2="30"
            stroke="currentColor"
            strokeWidth="2"
          />
        </svg>
      </div>
    );
  }
}
