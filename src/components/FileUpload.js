import React, { Component } from 'react';
import styled from 'styled-components';
import Dropzone from 'react-dropzone';

import PropTypes from 'prop-types';
import classnames from 'classnames';
import * as e from 'common/Elements';
import { setTimeout } from 'core-js/library/web/timers';

export class FileUpload extends Component {
  static propTypes = {
    className: PropTypes.string,
    dragOverClassName: PropTypes.string,
    hoverMessage: PropTypes.string,
    errorMessage: PropTypes.string,
    errorMessageDuration: PropTypes.number,
    onDropRejected: PropTypes.func,
  }
  static defaultProps = {
    dragOverClassName: `drag-over`,
    hoverMessage: `Click to browse or drop a file here to upload`,
    errorMessage: `Sorry but we don’t accept that file`,
    errorMessageDuration: 3000,
  }
  constructor() {
    super();
    this.state = {
      isDragOver: false,
    };
  }

  showErrorMessage = (...args) => {
    console.log(`rejected file`);
    if (this.props.onDropRejected) this.props.onDropRejected(...args);
    this.setState({
      showErrorMessage: true,
    });
    setTimeout(() => {
      this.setState({
        showErrorMessage: false,
      });
    }, this.props.errorMessageDuration);
  }

  render = () => {
    const {
      dragOverClassName,
      className,
      hoverMessage,
      errorMessage,
      onDropRejected,
      ...rest
    } = this.props;
    const {
      isDragOver,
      showErrorMessage,
    } = this.state;
    const classes = {
      [className]: true,
      [dragOverClassName]: isDragOver,
    };
    return (
      <e.Popover.Tooltip isDisabled={showErrorMessage} content={hoverMessage}>
        <e.Popover.Tooltip isDisabled={!showErrorMessage} isForcedOpen={showErrorMessage} content={errorMessage}>
          <Dropzone
            className={classnames(classes)}
            onDragEnter={() => (this.setState({ isDragOver: true }))}
            onDragLeave={() => (this.setState({ isDragOver: false }))}
            onDropRejected={this.showErrorMessage}
            {...rest}
          />
        </e.Popover.Tooltip>
      </e.Popover.Tooltip>
    );
  }
}

export default styled(FileUpload)`
  display: inline-block;
  vertical-align: top;
  cursor: pointer;
`;
