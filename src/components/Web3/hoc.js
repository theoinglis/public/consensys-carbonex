import React from 'react';
import Web3Provider from './component';

const injectWeb3 = (config = {}) => (WrappedComponent) => {
  const web3Wrapper = (props) => (
    <Web3Provider onInitialise={config.onInitialise ? config.onInitialise : () => true}>
      {(web3Info) => {
        return (
          <WrappedComponent web3={web3Info} {...props} />
        );
      }}
    </Web3Provider>
  );

  return web3Wrapper;
};

export default injectWeb3;
