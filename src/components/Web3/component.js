import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { initialiseWeb3 } from './get';

let web3Instance;

export class Web3Consumer extends Component {
  componentWillMount = () => {
    const {
      onInitialise,
    } = this.props;
    onInitialise(web3Instance);
  }

  render = () => {
    const {
      children,
    } = this.props;
    return children;
  }
}

export default class Web3Provider extends Component {
  static propTypes = {
    children: PropTypes.func.isRequired,
    onInitialise: PropTypes.func,
  }

  static defaultProps = {
    onInitialise: () => true,
  }

  constructor(props) {
    super(props);

    this.state = {
      isInitialised: false,
      isLoading: true,
      isError: false,
      error: null,
      data: null,
    };
  }

  componentWillMount = () => {
    const {
      onInitialise,
    } = this.props;
    this.setState({
      isLoading: true,
      isError: false,
      error: null,
    });
    initialiseWeb3
      .then((web3) => {
        web3Instance = web3;
        this.setState({
          isError: false,
          error: null,
        });
        return onInitialise(web3)
      })
      .then((data) => {
        this.setState({
          isInitialised: !!data,
          data,
          isLoading: false,
        })
      })
      .catch((err) => {
        this.setState({
          isLoading: false,
          isError: true,
          error: err,
        });
      });
  }

  render = () => {
    const {
      children,
    } = this.props;
    const {
      isInitialised,
      isLoading,
      isError,
      data,
      error,
    } = this.state;
    return children({
      initialised: isInitialised,
      loading: isLoading,
      error: isError,
      errorInfo: error,
      web3: web3Instance,
      data,
    });
  }
}
