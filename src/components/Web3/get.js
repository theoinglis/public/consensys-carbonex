import Web3 from 'web3';

let web3 = null;

export const initialiseWeb3 = new Promise(((resolve, reject) => {
  // Wait for loading completion to avoid race conditions with web3 injection timing.
  window.addEventListener(`load`, () => {
    // Checking if Web3 has been injected by the browser (Mist/MetaMask)
    if (typeof window.web3 !== `undefined`) {
      // Use Mist/MetaMask's provider.
      web3 = new Web3(window.web3.currentProvider);

      console.log(`Injected web3 detected.`);

      resolve(web3);
    } else {
      // Fallback to localhost if no web3 injection. We've configured this to
      // use the development console's port by default.
      const provider = new Web3.providers.HttpProvider(`http://127.0.0.1:8545`);

      web3 = new Web3(provider);

      console.log(`No web3 instance injected, using Local web3.`);

      resolve(web3);
    }
  });
}));

export const getWeb3 = () => {
  if (!web3) throw Error(`Web3 hasn't been initialised yet`);
  return web3;
};

export default getWeb3;
