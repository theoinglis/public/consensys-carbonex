import Responsive from 'react-responsive';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import v from 'components/StyleVariables';
/* eslint react/no-multi-comp: off */
const widths = {
  tiny: v.sizeTiny,
  extraSmall: v.sizeExtraSmall,
  small: v.sizeSmall,
  medium: v.sizeMedium,
  large: v.sizeLarge,
};

const createMinWidth = width => class extends Component {
  static propTypes = {
    children: PropTypes.oneOfType([PropTypes.func, PropTypes.node]),
  }

  render() {
    return (
      <Responsive query={`(min-width: ${width})`}>
        {this.props.children}
      </Responsive>
    );
  }
};

export const TinyDevices = createMinWidth(widths.tiny);
export const OverExtraSmallDevices = createMinWidth(widths.extraSmall);
export const OverSmallDevices = createMinWidth(widths.small);
export const OverMediumDevices = createMinWidth(widths.medium);
export const OverLargeDevices = createMinWidth(widths.large);
