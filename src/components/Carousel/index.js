import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';

const themes = {
  horizontal: css`
    transition: all 0s ${p => p.theme.easeOutQuart};
    opacity: 0;
    ${props => (
      props.isBehind
        ? css`left: -50px;`
        : css`left: 50px;`
    )}
    ${props => props.isShowing && css`
      transition: all 0.4s ${p => p.theme.easeOutQuart} 0s;
      opacity: 1;
      left: 0;
    `}
  `,
};

const CarouselContainer = styled.div`
  position: relative;
  height: 100%;
  width: 100%;
`;
const CarouselItemContainer = styled.div`
  position: relative;
  width: ${p => p.itemCount * 100}%;
  left: -${p => p.position * 100}%;
  display: flex;
  /* The following is a hack to get 
  it to top align on smaller devices */
  ${p => p.theme.media.tablet`
    align-items: center;
  `}
`;
const CarouselItem = styled.div`
  position: relative;
  display: inline-block;
  pointer-events: none;
  vertical-align: top;
  width: ${p => 100 / p.itemCount}%;
  ${p => p.isShowing && css`
    pointer-events: auto;
  `}
  ${p => themes[p.themeName]}
`;
const Controls = styled.div`
  position: absolute;
  top: 50%;
  width: 100%;
  transform: translateY(-50%);
  pointer-events: none;
`;
const Control = styled.button`
  border: none;
  position:absolute;
  pointer-events: auto;
  ${props => (props.isLeft
    ? css`
      left: 20px;
    ` : css`
      right: 20px;
    `)}
`;

export default class Carousel extends Component {
  static propTypes = {
    position: PropTypes.number.isRequired,
    onChange: PropTypes.func.isRequired,
    items: PropTypes.arrayOf(PropTypes.func.isRequired).isRequired,
    themeName: PropTypes.oneOf([`horizontal`]),
    ControlsComponent: PropTypes.func,
  }
  static defaultProps = {
    themeName: `horizontal`,
    ControlsComponent: Controls,
  }

  canChangeTo = (newIndex) => {
    return newIndex >= 0 && newIndex <= this.props.items.length - 1;
  }
  changeTo = (newIndex) => {
    if (this.canChangeTo(newIndex)) {
      this.props.onChange(newIndex);
      return true;
    } else return false;
  }

  render = () => {
    const {
      position,
      items,
      themeName,
      ControlsComponent,
    } = this.props;
    return (
      <CarouselContainer>
        <CarouselItemContainer position={position} itemCount={items.length}>
          {
            items.map((Item, index) => {
              return (
                <CarouselItem
                  itemCount={items.length}
                  isShowing={index === position}
                  isBehind={index <= position}
                  themeName={themeName}
                >
                  <Item />
                </CarouselItem>
              );
            })
          }
        </CarouselItemContainer>
        <ControlsComponent>
          <Control
            disabled={!this.canChangeTo(position - 1)}
            onClick={() => this.changeTo(position - 1)}
            isLeft
          >
            {`<`}
          </Control>
          <Control
            disabled={!this.canChangeTo(position + 1)}
            onClick={() => this.changeTo(position + 1)}
          >
            {`>`}
          </Control>
        </ControlsComponent>
      </CarouselContainer>
    );
  }
}
