import { Decorator as FormsyElement } from 'formsy-react';
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import FormLabel from './label';
import ToggleButtonComponent from 'common/Elements/toggleButton';
import styles from '../styles.css';
import classnames from 'classnames';

@FormsyElement()
export default class ToggleButton extends Component {
  static propTypes = {
    LabelComponent: PropTypes.func,
    ToggleComponent: PropTypes.func,
    name: PropTypes.string,
    title: PropTypes.string,

    getValue: PropTypes.func.isRequired,
    setValue: PropTypes.func.isRequired,
  }

  static defaultProps = {
    LabelComponent: FormLabel,
    ToggleComponent: ToggleButtonComponent,
  }

  constructor(props) {
    super(props);

    this.state = {
      isVirgin: true,
      isFocused: false,
    };
  }

  onChange = (isChecked) => {
    this.props.setValue(isChecked);
  }

  render = () => {
    const {
      LabelComponent,
      ToggleComponent,
      name,
      title,
      getValue,
    } = this.props;
    const {
      isVirgin,
      isFocused,
    } = this.state;

    return (
      <LabelComponent
        title={title}
        isFocused={isFocused}
        errorMessage={(!isVirgin && errorMessage) || ``}
      >
        <ToggleComponent
          name={name}
          isChecked={getValue()}
          onChange={this.onChange}
          onFocus={() => { this.setState({ isFocused: true }); }}
          onBlur={() => { this.setState({ isFocused: false, isVirgin: false }); }}
        />
      </LabelComponent>
    );
  }
}
