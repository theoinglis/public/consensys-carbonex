import { Decorator as FormsyElement } from 'formsy-react';
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import v from 'components/StyleVariables';
import styled, { css } from 'styled-components';

import FormLabel from './label';

const HiddenInput = styled.input`
  display: none;
`;
const RadioLabel = styled.label`
  display: inline-block;
  cursor: pointer; 
`;

@FormsyElement()
export default class RadioButton extends Component {
  static propTypes = {
    LabelComponent: PropTypes.func,
    RadioComponent: PropTypes.func,
    className: PropTypes.string,
    title: PropTypes.string,
    name: PropTypes.string,
    options: PropTypes.arrayOf(PropTypes.shape({
      name: PropTypes.string.isRequired,
      value: PropTypes.string.isRequired,
      displayText: PropTypes.string.isRequired,
    })),
    // Formsy Props
    getValue: PropTypes.func.isRequired,
    setValue: PropTypes.func.isRequired,
    getErrorMessage: PropTypes.func.isRequired,
  }

  static defaultProps = {
    LabelComponent: FormLabel,
    RadioComponent: RadioLabel,
  }

  constructor(props) {
    super(props);

    this.state = {
      isFocused: false,
      isVirgin: true,
    };
  }
  componentWillMount = () => {
    if (!this.props.getValue()) { this.selectOption(this.props.options[0]); }
  }

  selectOption = (option) => {
    this.props.setValue(option.value);
  }

  onChange = (option) => {
    this.selectOption(option);
  }

  render = () => {
    const {
      LabelComponent,
      RadioComponent,
      className,
      name,
      options,
      title,
      defaultValue,
      getValue,
      getErrorMessage,
    } = this.props;
    const {
      isFocused,
      isVirgin,
    } = this.state;
    const value = getValue();
    const errorMessage = getErrorMessage();

    return (
      <LabelComponent
        className={className}
        title={title}
        isFocused={isFocused}
        errorMessage={(!isVirgin && errorMessage) || ``}
      >
        {
          options.map((option) => {
            const isSelected = value === option.value;
            return (
              <RadioComponent
                key={option.name}
                htmlFor={option.name}
                isSelected={isSelected}
              >
                {option.displayText}
                <HiddenInput
                  type="radio"
                  id={option.name}
                  name={name}
                  value={option.value}
                  checked={isSelected}
                  onChange={() => this.onChange(option)}
                  onFocus={() => { this.setState({ isFocused: true }); }}
                  onBlur={() => { this.setState({ isFocused: false, isVirgin: false }); }}
                />
              </RadioComponent>
            );
          })
        }
      </LabelComponent>
    );
  }
}

export const CurrencyRadioButton = RadioLabel.extend`
  display: inline-block;
  width: 30px;
  height: 30px;
  font-size: 18px;
  line-height: 30px;
  text-align: center;
  border-radius: 15px;
  border: 1px solid currentColor;
  margin-right: 8px;
  color: ${v.colorInput};
  transition: all 0.6s ${v.easeOutQuart};
  transform: scale(1);
  &:hover, &:active {
    transform: scale(1.001);
    box-shadow: 0 2px 5px 0 rgba(120, 120, 120, .62);
  }
  ${props => props.isSelected && css`
    color: white;
    border-color: ${v.colorInput};
    background: ${v.colorInputActive};
  `}
`;
