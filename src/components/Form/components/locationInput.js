import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Decorator as FormsyElement } from 'formsy-react';
import Geosuggest from 'react-geosuggest';
import styled from 'styled-components';
import v from 'components/StyleVariables';

import FormLabel from './label';

const London = { isFixture: true, description: `London, United Kingdom`, label: `London, United Kingdom`, placeId: `ChIJdd4hrwug2EcRmSrV3Vo6llI`, matchedSubstrings: { length: 6, offset: 0 }, gmaps: { address_components: [{ long_name: `London`, short_name: `London`, types: [`locality`, `political`] }, { long_name: `London`, short_name: `London`, types: [`postal_town`] }, { long_name: `Greater London`, short_name: `Greater London`, types: [`administrative_area_level_2`, `political`] }, { long_name: `England`, short_name: `England`, types: [`administrative_area_level_1`, `political`] }, { long_name: `United Kingdom`, short_name: `GB`, types: [`country`, `political`] }], formatted_address: `London, UK`, geometry: { bounds: { south: 51.38494009999999, west: -0.351468299999965, north: 51.6723432, east: 0.14827100000002247 }, location: { lat: 51.5073509, lng: -0.12775829999998223 }, location_type: `APPROXIMATE`, viewport: { south: 51.38494009999999, west: -0.351468299999965, north: 51.6723432, east: 0.14827100000002247 } }, place_id: `ChIJdd4hrwug2EcRmSrV3Vo6llI`, types: [`locality`, `political`] }, location: { lat: 51.5073509, lng: -0.12775829999998223 } };
const NewYork = { isFixture: true, description: `New York, NY, United States`, label: `New York, NY, United States`, placeId: `ChIJOwg_06VPwokRYv534QaPC8g`, matchedSubstrings: { length: 8, offset: 0 }, gmaps: { address_components: [{ long_name: `New York`, short_name: `New York`, types: [`locality`, `political`] }, { long_name: `New York`, short_name: `NY`, types: [`administrative_area_level_1`, `political`] }, { long_name: `United States`, short_name: `US`, types: [`country`, `political`] }], formatted_address: `New York, NY, USA`, geometry: { bounds: { south: 40.4773991, west: -74.25908989999999, north: 40.9175771, east: -73.7002721 }, location: { lat: 40.7127753, lng: -74.0059728 }, location_type: `APPROXIMATE`, viewport: { south: 40.4773991, west: -74.25908989999999, north: 40.9175771, east: -73.7002721 } }, place_id: `ChIJOwg_06VPwokRYv534QaPC8g`, types: [`locality`, `political`] }, location: { lat: 40.7127753, lng: -74.0059728 } };
const Frankfurt = { isFixture: true, description: `Frankfurt, Germany`, label: `Frankfurt, Germany`, placeId: `ChIJxZZwR28JvUcRAMawKVBDIgQ`, matchedSubstrings: { length: 9, offset: 0 }, gmaps: { address_components: [{ long_name: `Frankfurt`, short_name: `Frankfurt`, types: [`locality`, `political`] }, { long_name: `Darmstadt`, short_name: `DA`, types: [`administrative_area_level_2`, `political`] }, { long_name: `Hesse`, short_name: `HE`, types: [`administrative_area_level_1`, `political`] }, { long_name: `Germany`, short_name: `DE`, types: [`country`, `political`] }], formatted_address: `Frankfurt, Germany`, geometry: { bounds: { south: 50.0152145, west: 8.47272989999999, north: 50.2272095, east: 8.800397699999962 }, location: { lat: 50.1109221, lng: 8.682126700000026 }, location_type: `APPROXIMATE`, viewport: { south: 50.0152145, west: 8.47272989999999, north: 50.2272095, east: 8.800397699999962 } }, place_id: `ChIJxZZwR28JvUcRAMawKVBDIgQ`, types: [`locality`, `political`] }, location: { lat: 50.1109221, lng: 8.682126700000026 } };
const DefaultFixtureList = [
  London,
  NewYork,
  Frankfurt,
];

@FormsyElement()
export default class LocationInput extends Component {
  static propTypes = {
    LabelComponent: PropTypes.func,
    className: PropTypes.string,
    title: PropTypes.string,
    fixtures: PropTypes.array,
    getLocationLabel: PropTypes.func,
    // Formsy Props
    name: PropTypes.string.isRequired,
    getValue: PropTypes.func.isRequired,
    setValue: PropTypes.func.isRequired,
    getErrorMessage: PropTypes.func.isRequired,
  }

  static defaultProps = {
    LabelComponent: FormLabel,
    fixtures: DefaultFixtureList,
    getLocationLabel: value => (value.label),
  }

  constructor(props) {
    super(props);

    this.state = {
      isFocused: false,
    };
  }

  onSelect = (location) => {
    this.props.setValue(location);
  }

  filterNonFixturesOnPlaceId = fixtureIds => suggest => (
    !suggest.isFixture
    && fixtureIds.indexOf(suggest.place_id || suggest.placeId) > -1
  )

  render = () => {
    const {
      LabelComponent,
      className,
      title,
      fixtures,
      getLocationLabel,
      getValue,
      getErrorMessage,
    } = this.props;
    const {
      isFocused,
    } = this.state;
    const value = getValue();
    const fixtureIds = fixtures.map(f => f.gmaps.place_id);
    const filterFixtures = this.filterNonFixturesOnPlaceId(fixtureIds);

    return (
      <LabelComponent
        className={className}
        title={title}
        onFocus={() => this.setState({ isFocused: true })}
        onBlur={() => this.setState({ isFocused: false })}
        errorMessage={getErrorMessage()}
      >
        <OutlineGeosuggest
          ref={(el) => { this._geoSuggest = el; return 1; }}
          initialValue={getLocationLabel(value)}
          onSuggestSelect={this.onSelect}
          fixtures={fixtures}
          skipSuggest={filterFixtures}
          types={[`(cities)`]}
          suggestsClassName="dropdown"
          suggestsHiddenClassName="hidden"
          suggestItemClassName="dropdown-item"
          suggestItemActiveClassName="is-active"
        />
      </LabelComponent>
    );
  }
}

const OutlineGeosuggest = styled(Geosuggest)`
  height: 32px;
  .geosuggest__input {
    border: 1px solid currentColor;
    border-radius: 4px;
    width: 100%;
    line-height: 14px;
    font-size: 14px;
    margin: 0 0 4px;
    padding: 7px 8px;
    height: 32px;
    box-sizing: border-box;
    color: inherit;
    transition: all 0.6s ${v.easeOutQuart};
    transform: scale(1);
    &:hover, &:focus {
      transform: scale(1.001);
      box-shadow: 0 2px 5px 0 rgba(120, 120, 120, .62);
    }
  }
  .dropdown {
    border: 1px solid currentColor;
    border-radius: 0 0 4px 4px;
    &.hidden {
      pointer-events: none;
      border-color: transparent;
    }
  }
  .dropdown-item {
    transition: all 0.6s ${v.easeOutQuart};
    margin: 4px 0;
    font-size: 14px;
    :hover, &.is-focused {
      transition: all 0.2s ${v.easeOutQuart};
      background: ${v.colorInputOptionHover};
    }
    &.is-active {
      background: ${v.colorInputActive};
      color: white;
    }
  }
`;
