import Formsy, { Decorator as FormsyElement } from 'formsy-react';
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Label from './label';
import Input from 'common/Elements/input';

@FormsyElement()
export default class FormsyInput extends Component {
  static propTypes = {
    LabelComponent: PropTypes.func,
    InputComponent: PropTypes.func,
    info: PropTypes.string,
    title: PropTypes.string.isRequired,
    type: PropTypes.string,
    name: PropTypes.string,
    placeholder: PropTypes.string,
    disabled: PropTypes.bool,
    onFocusChange: PropTypes.func,
    // Formsy Props
    getValue: PropTypes.func.isRequired,
    setValue: PropTypes.func.isRequired,
    getErrorMessage: PropTypes.func,
    isValid: PropTypes.func,
    isFormSubmitted: PropTypes.func,
  }

  static defaultProps = {
    LabelComponent: Label,
    InputComponent: Input,
    type: `text`,
    disabled: false,
    onFocusChange: () => null,
    autocomplete: `on`,
  }

  constructor(props) {
    super(props);

    this.state = {
      isFocused: false,
      isVirgin: !this.props.getValue(),
    };
  }

  changeValue = (e) => {
    this.props.setValue(e.target.value);
  }

  render = () => {
    const {
      LabelComponent,
      InputComponent,
      name,
      title,
      type,
      info,
      getValue,
      getErrorMessage,
      isValid,
      disabled,
      placeholder,
      onFocusChange,
      isFormSubmitted,
    } = this.props;
    const {
      isFocused,
      isVirgin,
    } = this.state;
    const state = this.state;
    const errorMessage = getErrorMessage();
    state.isValid = !isValid();
    const shouldHideError = isVirgin && !isFormSubmitted();

    return (
      <LabelComponent
        title={title}
        isFocused={isFocused}
        isDisabled={disabled}
        shouldShowError={!shouldHideError}
        errorMessage={errorMessage}
        infoMessage={info}
      >
        <InputComponent
          type={type}
          name={name}
          placeholder={placeholder}
          disabled={disabled}
          onChange={this.changeValue}
          value={getValue()}
          onFocus={() => { onFocusChange(true); this.setState({ isFocused: true }); }}
          onBlur={() => { onFocusChange(false); this.setState({ isFocused: false, isVirgin: false }); }}
        />
      </LabelComponent>
    );
  }
}
