import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form } from 'formsy-react';

export default class FormContainer extends Component {
  static propTypes = {
    formRef: PropTypes.func,
    status: PropTypes.object,
  }
  static defaultProps = {
    formRef: () => null,
  }

  set400ErrorMessages = (status) => {
    const errorMessages = {};
    Object.keys(status.error.body).forEach((key) => {
      errorMessages[key] = status.error.body[key].msg;
    });
    console.log(`setting errors`, errorMessages);
    this.form.updateInputsWithError(errorMessages);
  }
  is400Error = (status) => {
    return status && status.isError && status.error.response.status === 400;
  }

  render = () => {
    const {
      formRef,
      status,
      ...rest
    } = this.props;
    if (this.form && this.is400Error(status)) {
      this.set400ErrorMessages(status);
    }
    return (
      <Form
        ref={(c) => { this.form = c; formRef(c); }}
        {...rest}
      />
    );
  }
}
