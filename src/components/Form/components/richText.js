import { Decorator as FormsyElement } from 'formsy-react';
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import RichText from 'common/Elements/richText';
import FormLabel from './label';

const Delta = require(`quill-delta`);

@FormsyElement()
export default class FormsyRichText extends Component {
  static propTypes = {
    LabelComponent: PropTypes.func,
    RichTextComponent: PropTypes.func,
    className: PropTypes.string,
    title: PropTypes.string,
    name: PropTypes.string.isRequired,

    toolbar: PropTypes.array,
    formats: PropTypes.array,

    // Formsy Props
    getValue: PropTypes.func.isRequired,
    setValue: PropTypes.func.isRequired,
    getErrorMessage: PropTypes.func.isRequired,
  }

  static defaultProps = {
    LabelComponent: FormLabel,
    RichTextComponent: RichText,
    toolbar: [
      [`bold`, `italic`, `underline`],
      [{ header: 1 }, { header: 2 }],
      [{ list: `ordered` }, { list: `bullet` }],
      [`clean`],
    ],
    formats: [
      `bold`,
      `italic`,
      `underline`,
      `header`,
      `blockquote`,
      `list`,
    ],
  }

  constructor() {
    super();
    this.state = {
      // We have to use state rather than formsy 
      // I think because of a cycle delay caused 
      // in sending it to Formsy and waiting for
      // the value to get set
      content: new Delta(),
      isFocused: false,
      isVirgin: true,
    };
  }

  componentWillMount = () => {
    const value = this.props.getValue();
    if (value && value.content) {
      const content = value.content;
      const isString = typeof content === `string`;
      this.setState({
        content: isString ? content : new Delta(content),
      });
    } else {
      this.setState({
        content: new Delta(),
      });
    }
  }

  onChange = (htmlContent, delta, source, editor) => {
    const content = editor.getContents();
    this.setState({
      content,
    });
    this.props.setValue({
      content,
      length: editor.getLength(),
      text: editor.getText(),
    });
  }
  onClick = () => {
    console.log(this.editor);
    this.editor.focus();
  }

  render = () => {
    const {
      LabelComponent,
      RichTextComponent,
      className,
      title,
      toolbar,
      formats,
      getErrorMessage,
      getValue,
      isFormSubmitted,
    } = this.props;
    const {
      content,
      isFocused,
      isVirgin,
    } = this.state;
    const errorMessage = getErrorMessage();
    const shouldHideError = isVirgin && !isFormSubmitted();

    return (
      <LabelComponent
        onClick={() => this.editor.focus()}
        className={className}
        title={title}
        isFocused={isFocused}
        showError={!shouldHideError}
        errorMessage={errorMessage}
        infoMessage={`Highlight the text for more formatting options`}
      >
        <RichTextComponent
          innerRef={c => this.editor = c}
          value={content}
          onChange={this.onChange}
          theme="bubble"
          modules={{
            toolbar,
          }}
          formats={formats}
          isFocused={isFocused}
          onFocus={() => { this.setState({ isFocused: true }); }}
          onBlur={() => { this.setState({ isFocused: false, isVirgin: false }); }}
        />
      </LabelComponent>
    );
  }
}
