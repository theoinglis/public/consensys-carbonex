import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styled, { css } from 'styled-components';
import v from 'components/StyleVariables';

import { Dot } from 'common/Elements/interestPoint';
import { Tooltip } from 'common/Elements/popover';

const TitleContainer = styled.div`
  margin-bottom: 8px;
`;
const Title = styled.span`
  position: relative;
  bottom: 1px;
  font-size: 18px;
  font-weight: 300;
  letter-spacing: 0.03em;
  color: currentColor;
  transition: all 0.6s $${v.easeOutQuart};
`;
const TitleDot = styled(Dot) `
  position: relative;
  top: 0.5px;
  margin-left: 4px;
`;
const TitleTooltip = styled(Tooltip) `
  bottom: 2px;
  margin-left: 4px;
`;

class LabelComponent extends Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
    children: PropTypes.object.isRequired,
    shouldShowError: PropTypes.bool,
    infoMessage: PropTypes.string,
    errorMessage: PropTypes.string,
    className: PropTypes.string,
  };
  static defaultProps = {
    shouldShowError: true,
  }

  render = () => {
    const {
      title,
      children,
      shouldShowError,
      infoMessage,
      errorMessage,
      className,
      ...rest
    } = this.props;

    return (
      <label className={className} {...rest}>
        <TitleContainer>
          <Title>{title}</Title>
          {infoMessage && (
            <TitleTooltip
              content={infoMessage}
              isDisabled={!infoMessage}
            >
              <TitleDot
                isVisible={infoMessage}
              />
            </TitleTooltip>
          )}
          <TitleTooltip
            content={errorMessage}
            isDisabled={!shouldShowError || !errorMessage}
          >
            <TitleDot
              color={`red`}
              isVisible={shouldShowError && errorMessage}
              isActive
            />
          </TitleTooltip>
        </TitleContainer>
        {children}
      </label>
    );
  }
}
const Label = styled(LabelComponent) `
  display: inline-block;
  margin-bottom: 14px;
  width: 100%;
  transition: all 0.6s ${v.easeOutQuart};
  color: ${v.colorInput};
  &:hover {
    color: ${v.colorInputHover};
  }
  ${props => props.isFocused && css`
    color: ${v.colorInputFocused};
  `}
  ${props => props.isDisabled && css`
    color: ${v.colorDisabled};
    opacity: 0.8;
    pointer-events: none;
  `}
`;
export default Label;
