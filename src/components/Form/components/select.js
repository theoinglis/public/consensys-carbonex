import { Decorator as FormsyElement } from 'formsy-react';
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
  OutlineSelect,
  OutlineSelectValue,
  OutlineCreatable,
} from 'common/Elements/select';

import FormLabel from './label';


@FormsyElement()
export default class Select extends Component {
  static propTypes = {
    LabelComponent: PropTypes.func,
    SelectComponent: PropTypes.func,
    ValueComponent: PropTypes.func,
    className: PropTypes.string,
    name: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    options: PropTypes.array.isRequired,
    info: PropTypes.string,

    isClearable: PropTypes.bool,
    isSearchable: PropTypes.bool,
    isMulti: PropTypes.bool,
    labelKey: PropTypes.string,
    valueKey: PropTypes.string,
    menuContainerStyle: PropTypes.object,
    arrowRenderer: PropTypes.func,

    // Option Props
    getOptionValue: PropTypes.func,

    // Formsy Props
    getValue: PropTypes.func.isRequired,
    setValue: PropTypes.func.isRequired,
    getErrorMessage: PropTypes.func,
    // isValid: PropTypes.func,
  }

  static defaultProps = {
    LabelComponent: FormLabel,
    SelectComponent: OutlineSelect,
    ValueComponent: OutlineSelectValue,
    isClearable: false,
    isSearchable: false,
    isMulti: false,
    getOptionValue: option => option.value,
  }

  constructor() {
    super();
    this.state = {
      isVirgin: true,
      isFocused: false,
    };
  }

  onChange = (option) => {
    this.props.setValue(this.props.getOptionValue(option));
  }

  render = () => {
    const {
      LabelComponent,
      SelectComponent,
      ValueComponent,
      className,
      name,
      title,
      info,
      isClearable,
      isSearchable,
      isMulti,
      labelKey,
      valueKey,
      getValue,
      getErrorMessage,
      options,
      menuContainerStyle,
      arrowRenderer,
    } = this.props;
    const {
      isVirgin,
      isFocused,
    } = this.state;
    const errorMessage = getErrorMessage();

    return (
      <LabelComponent
        className={className}
        title={title}
        isFocused={isFocused}
        errorMessage={(!isVirgin && errorMessage) || ``}
        infoMessage={info}
      >
        <SelectComponent
          name={name}
          value={getValue()}
          options={options}
          onChange={this.onChange}
          clearable={isClearable}
          searchable={isSearchable}
          multi={isMulti}
          labelKey={labelKey}
          valueKey={valueKey}
          valueComponent={isMulti ? ValueComponent : undefined}
          onFocus={() => { this.setState({ isFocused: true }); }}
          onBlur={() => { this.setState({ isFocused: false, isVirgin: false }); }}
          menuContainerStyle={menuContainerStyle}
          arrowRenderer={arrowRenderer}
        />
      </LabelComponent>
    );
  }
}

export const LocalDataSingleSelect = ({ valueKey, labelKey, getOptionValue, ...rest }) => (
  <Select
    valueKey={valueKey || `value`}
    labelKey={labelKey || `label`}
    getOptionValue={getOptionValue || (option => option.value)}
    {...rest}
  />
);
export const SingleSelect = ({ valueKey, labelKey, getOptionValue, ...rest }) => (
  <Select
    valueKey={valueKey || `_id`}
    labelKey={labelKey || `name`}
    getOptionValue={getOptionValue || (option => option)}
    {...rest}
  />
);

export const MultiSelect = ({ isSearchable, valueKey, labelKey, ...rest }) => (
  <Select
    isMulti
    isSearchable={isSearchable || true}
    getOptionValue={option => option}
    valueKey={valueKey || `_id`}
    labelKey={labelKey || `name`}
    {...rest}
  />
);

export const MultiCreatable = ({ getOptionValue, ...rest }) => (
  <Select
    isMulti
    isSearchable
    SelectComponent={OutlineCreatable}
    getOptionValue={option => option}
    valueKey={`value`}
    labelKey={`label`}
    menuContainerStyle={{ display: `none` }}
    arrowRenderer={() => null}
    {...rest}
  />
);
