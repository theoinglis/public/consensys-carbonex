import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Dropzone from 'react-dropzone';

export default class PhotoInput extends Component {
  static propTypes = {
    className: PropTypes.string,
    accept: PropTypes.string,
    isDisabled: PropTypes.bool,
    children: PropTypes.func.isRequired,
  }
  static defaultProps = {
    accept: `image/jpeg, image/png`,
    isDisabled: false,
  }

  onDrop = () => {

  }

  render = () => {
    const {
      className,
      accept,
      isDisabled,
      children,
    } = this.props;
    return (
      <Dropzone
        className={className}
        onDrop={this.onDrop}
        accept={accept}
        isDisabled={isDisabled}
      >
        { children }
      </Dropzone>
    );
  }
}
