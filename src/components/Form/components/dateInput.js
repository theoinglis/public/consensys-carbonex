import { Decorator as FormsyElement } from 'formsy-react';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import Label from './label';
import Input from 'common/Elements/input';
import Calendar from 'common/Elements/calendar';

const ClickOnlyInput = Input.withComponent(`div`).extend`
  cursor: pointer;
`;

@FormsyElement()
export default class DateInput extends Component {
  static propTypes = {
    InputComponent: PropTypes.func,
    CalendarComponent: PropTypes.func,
    title: PropTypes.string.isRequired,
    type: PropTypes.string,
    name: PropTypes.string,
    placeholder: PropTypes.string,
    disabled: PropTypes.bool,
    displayFormat: PropTypes.string,
    // Formsy Props
    getValue: PropTypes.func.isRequired,
    setValue: PropTypes.func.isRequired,
    getErrorMessage: PropTypes.func,
    isValid: PropTypes.func,
  }

  static defaultProps = {
    displayFormat: `Do MMMM YYYY`,
    disabled: false,
    InputComponent: ClickOnlyInput,
    CalendarComponent: Calendar,
  }

  constructor(props) {
    super(props);

    this.state = {
      isFocused: false,
      isVirgin: !this.props.getValue(),
    };
  }

  selectValue = (jsDate) => {
    const date = moment(jsDate);
    this.props.setValue(date.toISOString());
    this.setState({
      isFocused: false,
      isVirgin: false,
    });
  }

  render = () => {
    const {
      name,
      title,
      displayFormat,
      getValue,
      getErrorMessage,
      isValid,
      disabled,
      placeholder,
      InputComponent,
      CalendarComponent,
      ...rest
    } = this.props;
    const {
      isFocused,
      isVirgin,
    } = this.state;
    const state = this.state;
    const errorMessage = getErrorMessage();
    state.isValid = !isValid();
    const dateValue = getValue();
    const date = dateValue ? moment(dateValue) : moment();

    return (
      <Label
        title={title}
        isFocused={isFocused}
        isDisabled={disabled}
        errorMessage={(!isVirgin && errorMessage) || ``}
        onMouseLeave={() => { this.setState({ isFocused: false }); }}
      >
        <InputComponent
          name={name}
          placeholder={placeholder}
          disabled={true && disabled}
          onClick={() => { this.setState({ isFocused: true }); }}
        >
          {date.format(displayFormat)}
        </InputComponent>
        <CalendarComponent
          height={300}
          width={300}
          shouldShow={isFocused}
          selected={date.toDate()}
          displayOptions={{
            showHeader: false,
          }}
          onSelect={this.selectValue}
          {...rest}
        />
      </Label>
    );
  }
}
