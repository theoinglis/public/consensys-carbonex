/* eslint react/no-string-refs: "off" */

import Formsy, { Form as FormsyForm } from 'formsy-react';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

export const Form = FormsyForm;

Formsy.addValidationRule(`isRequired`, (values, value) => {
  const isEmpty = value !== undefined && value !== null && value !== ``;
  return isEmpty;
});
Formsy.addValidationRule(`isMax`, (values, value, maxNo) => value <= maxNo);
Formsy.addValidationRule(`isMin`, (values, value, minNo) => value >= minNo);
Formsy.addValidationRule(`richTextMinLength`, (values, value, minLength) => value && (value.length) >= minLength);
Formsy.addValidationRule(`richTextMaxLength`, (values, value, maxLength) => {
  if (!value) return true;
  return (value.length) <= maxLength;
});
Formsy.addValidationRule(`minSelectedCount`, (values, value, minCount) => value && (value.length) >= minCount);
Formsy.addValidationRule(`maxSelectedCount`, (values, value, maxCount) => {
  if (!value) return true;
  return value.length <= maxCount;
});

export const Row = styled.div`
display: flex;
`;
export const Cell = styled.div`
margin-right: ${props => (props.isLastCell ? 0 : `8px`)};
`;
export const CellFixed = Cell.extend`
flex-shrink: 0;
flex-grow: 0;
`;
export const CellFill = Cell.extend`
flex: 1;
`;

export default class MyForm extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
    handleRequest: PropTypes.func,
    serverResponse: PropTypes.object,
    FormButton: PropTypes.func,
    resetOnChange: PropTypes.bool,
    onFormChange: PropTypes.func,
    resetOnSucessfulSubmit: PropTypes.bool,
    className: PropTypes.string,
    onValidChanged: PropTypes.func,
  }

  static defaultProps = {
    serverResponse: {},
    resetOnChange: false,
    onValidChanged: () => {},
  }

  static defaultProps = {
    serverResponse: {},
  }

  constructor(props) {
    super(props);

    const validationErrors = {};

    this.state = {
      canSubmit: false,
      preSubmit: true,
      errorMessages: [],
      validationErrors,
    };
  }

  componentDidUpdate = (prevProps) => {
    const response = this.props.serverResponse;
    const { isError, error, resetForm } = response;

    if (prevProps.serverResponse !== response) {
      if (!isError && resetForm) {
        this.resetForm();
      } else {
        this.setState({
          canSubmit: false,
          errorMessages: [`${error.error}`],
          validationErrors: { [error.field]: error.error },
        });
      }
    }
  }

  onValid = () => {
    this.setState({ canSubmit: true });
    this.props.onValidChanged(true);
  }

  onInvalid = () => {
    this.setState({ canSubmit: false });
    this.props.onValidChanged(false);
  }

  onFormChange = (data) => {
    this.setState({ validationErrors: {} });
    if (this.props.onFormChange) this.props.onFormChange(data);
    if (this.props.resetOnChange && !this.state.preSubmit) this.resetForm();
  }

  disableButton = () => {
    this.setState({
      canSubmit: false,
    });
  }

  enableButton = () => {
    this.setState({
      canSubmit: true,
    });
  }

  submit = (data) => {
    const { handleRequest } = this.props;
    let errorMessages = [];
    const inputs = this.refs.form.inputs;

    Object.keys(inputs).map((idx) => {
      const input = inputs[idx];
      errorMessages = errorMessages.concat(input.getErrorMessages());
      return 1;
    });

    const finalErrors = errorMessages.filter(x => x !== ``);

    if (finalErrors.length <= 0) {
      handleRequest(data);
      if (this.props.resetOnSucessfulSubmit) this.resetForm();
    } else {
      this.setState({ errorMessages: finalErrors });
    }
  }

  resetForm = () => {
    this.refs.form.reset();
    this.setState({ preSubmit: true, canSubmit: false });
  }

  render = () => {
    const { FormButton } = this.props;

    return (
      <div>
        <Formsy.Form
          ref="form"
          onSubmit={this.submit}
          onChange={this.onFormChange}
          onValid={this.onValid}
          onInvalid={this.onInvalid}
          validationErrors={this.state.validationErrors}
          className={this.props.className}
        >
          {this.props.children}
          {FormButton &&
            <FormButton type="submit" disabled={!this.state.canSubmit}>
              sign in
            </FormButton>}
        </Formsy.Form>
      </div>
    );
  }
}
