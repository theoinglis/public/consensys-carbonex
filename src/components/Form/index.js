import Formsy from 'formsy-react';
import styled, { css } from 'styled-components';
import moment from 'moment';

import FormContainer from './components/container';
import * as e from 'common/Elements';
import * as Button from './components/button';

import * as Date from './components/dateInput';
import * as File from './components/fileInput';
import * as Input from './components/input';
import * as Location from './components/locationInput';
import * as Radio from './components/radio';
import * as RichText from './components/richText';
import * as Select from './components/select';
import * as Toggle from './components/toggleButton';

export {
  Button,
  Date,
  File,
  Input,
  Location,
  Radio,
  RichText,
  Select,
  Toggle,
};


Formsy.addValidationRule(`isRequired`, (values, value) => {
  const isEmpty = value !== undefined && value !== null && value !== ``;
  return isEmpty;
});
Formsy.addValidationRule(`isMax`, (values, value, maxNo) => value <= maxNo);
Formsy.addValidationRule(`isMin`, (values, value, minNo) => value >= minNo);
Formsy.addValidationRule(`isGreaterThanField`, (values, value, fieldName) => {
  const otherValue = values[fieldName];
  if (!otherValue) return true;
  return value > otherValue;
});
Formsy.addValidationRule(`isLessThanField`, (values, value, fieldName) => {
  const otherValue = values[fieldName];
  if (!otherValue) return true;
  return value < otherValue;
});
Formsy.addValidationRule(`richTextMinLength`, (values, value, minLength) => value && (value.length) >= minLength);
Formsy.addValidationRule(`richTextMaxLength`, (values, value, maxLength) => {
  if (!value) return true;
  return (value.length) <= maxLength;
});
Formsy.addValidationRule(`minSelectedCount`, (values, value, minCount) => value && (value.length) >= minCount);
Formsy.addValidationRule(`maxSelectedCount`, (values, value, maxCount) => {
  if (!value) return true;
  return value.length <= maxCount;
});
Formsy.addValidationRule(`isDate`, (values, value) => {
  if (!value) return false;
  return moment(value).isValid();
});


export const Container = FormContainer;
export const Row = styled.div`
  display: flex;
`;
export const Cell = styled.div`
  margin-right: 8px;
  &:last-child {
    margin-right: 0;
  }
  ${p => p.noLabel && css`
    padding-top: 20px;
  `}
`;
export const CellFixed = Cell.extend`
  flex-shrink: 0;
  flex-grow: 0;
  width: ${p => p.width || `auto`};
`;
export const CellFill = Cell.extend`
  flex: 1;
`;

export const Section = styled.div`
  margin-bottom: 30px;
  color: #33557E;
`;
export const SectionAside = styled.div`
  padding: 0 20px;
  width: 100x;
  position: absolute;
`;
export const SectionContent = styled.div`
  margin: 14px 140px 14px;
`;
export const SectionDivider = styled.hr`
  border: none;
  border-top: 1px solid #33557E;
`;
export const SectionContentDivider = styled.hr`
  border: none;
  border-top: 1px solid #DCE8EE;
`;
export const Title = styled.h2`
  font-family: Raleway;
  font-size: 13px;
  line-height: 13px;
  font-weight: bold;
  width: 100%;
  color: #6685A9;
  padding: 0;
  margin: 0;
  max-width: 160px;
`;
export const ValuePair = styled.span`
  display: inline-block;
  margin-right: 30px;
  min-height: 40px;
`;
export const Label = styled.h4`
  font-family: Raleway;
  font-size: 11px;
  font-weight: bold;
  text-transform: uppercase;
  margin-bottom: 6px;
`;
export const Value = styled.span`
  font-family: Roboto;
  font-size: 11px;
  font-weight: bold;
`;
