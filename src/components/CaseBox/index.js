import React from 'react';
import { renderPrismicText } from 'helpers/prismic-reader';
import moment from 'moment';
import { Link } from 'react-router-dom';

import styles from './styles.css';

const truncate = (str, n) => str.substr(0, n - 1) + (str.length > n ? `...` : ``);

const renderOverview = content => (
  <div className={styles.overviewContainer}>
    <p className={styles.overviewTitle}>Overview</p>
    <p className={styles.overviewContent}>{truncate(content, 200)}</p>
  </div>
);

export const renderCaseBox = (individualCaseDocument, overview, caseBoxClass) => {
  const individualCase = individualCaseDocument.rawJSON;
  const summary = individualCase.summary;
  const endDate = individualCase.end_date;
  const projectOverview = renderPrismicText(individualCase, `project_overview`);
  let displayDate = ``;
  if (endDate) {
    const date = moment(endDate);
    displayDate = date.isValid ? date.format(`D MMMM YYYY`) : ``;
  }
  const caseId = individualCaseDocument.uid;
  return (
    <Link
      to={`/case-study/${individualCaseDocument.uid}`}
      className={`${styles.caseBoxContainer} ${styles.customCaseBox} ${caseBoxClass}`}
      key={caseId}
    >
      <div className={styles.individualCaseBox}>
        <p className={styles.summary}>
          {truncate(summary, 45)}
        </p>
        { overview
          ? renderOverview(projectOverview)
          : null
        }
        <p className={styles.date}>{displayDate}</p>
      </div>
      <div className={styles.viewButton}>view</div>
    </Link>
  );
};
