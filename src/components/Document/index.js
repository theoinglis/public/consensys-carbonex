import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Scrollspy from 'react-scrollspy';

import FullScreenSection from 'components/FullScreenSection';
import { OverSmallDevices } from 'components/Responsive';

import styles from './styles.css';

export default class Document extends Component {
  static propTypes = {
    data: PropTypes.array.isRequired,
    title: PropTypes.string.isRequired,
    description: PropTypes.string,
  }

  renderDocument() {
    const scrollspyItems = [];
    const renderedIndex = this.props.data.map((data, key) => (
      <li className={styles.indexText}>
        <a href={`#section-${key}`}> <span className={styles.indexNumber}> {`${key + 1}. `} </span> <span className={styles.indexText}>{data.title}</span> </a>
      </li>
    ));
    const renderedContent = this.props.data.map((data, key) => {
      const contentData = data.content.map(section => (
        <p className={styles.contentText}>{section}</p>
      ));
      scrollspyItems.push(`section-${key}`);
      return (
        <div className={styles.contentItem}>
          <p className={styles.contentTitle} id={`section-${key}`}> {`${key + 1}. ${data.title}`}</p>
          {contentData}
        </div>
      );
    });
    return (
      <div className={styles.documentContent}>
        <OverSmallDevices>
          <div className={styles.documentIndex}>
            <Scrollspy items={scrollspyItems} currentClassName="is-current" offset={-1000}>
              {renderedIndex}
            </Scrollspy>
          </div>
        </OverSmallDevices>
        <div className={styles.documentBody}>
          {renderedContent}
        </div>
      </div>
    );
  }

  render() {
    return (
      <div>
        <FullScreenSection className={styles.documentContainer}>
          <div className={styles.documentHeader}>
            <div className={styles.title}>{this.props.title}</div>
            <div className={styles.underline} />
            <div className={styles.description}><p>{this.props.description}</p></div>
          </div>
          {this.renderDocument()}
        </FullScreenSection>
      </div>
    );
  }
}
