import React from 'react';
import {
  test,
  spy,
  expect,
  mount,
} from '__tests__/helpers/test-setup';

import NavButton from './';

test(`onClick property is called`, () => {
  const onClick = spy();
  const wrapper = mount(
    <NavButton onClick={onClick} />,
  );
  wrapper.simulate(`click`);
  expect(onClick.called).toBe(true);
});

test(`default src uses placehold.it`, () => {
  const wrapper = mount(
    <NavButton />,
  );
  expect(wrapper.find(`img`).prop(`src`)).toBe(`https://placehold.it/80x80`);
});

test(`can pass in image source`, () => {
  const imageUrl = `www.image.com`;
  const wrapper = mount(
    <NavButton imageSrc={imageUrl} />,
  );
  expect(wrapper.find(`img`).prop(`src`)).toBe(imageUrl);
});

test(`creates a label if label property is applied`, () => {
  const label = `random button`;
  const wrapper = mount(
    <NavButton label={label} />,
  );
  expect(wrapper.contains(<div>{label}</div>)).toBe(true);
});
