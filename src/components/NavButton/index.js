import React from 'react';
import PropTypes from 'prop-types';

import styles from './styles.css';

const NavButton = ({ onClick, imageSrc, label }) => (
  <a role={`button`} className={styles.navButton} onClick={onClick} tabIndex={0}>
    <img src={imageSrc} alt="icon" />
    {
      label && <div>{label}</div>
    }
  </a>
);

NavButton.propTypes = {
  onClick: PropTypes.func.isRequired,
  imageSrc: PropTypes.string.isRequired,
  label: PropTypes.string,
};

NavButton.defaultProps = {
  imageSrc: `https://placehold.it/80x80`,
};

export default NavButton;
