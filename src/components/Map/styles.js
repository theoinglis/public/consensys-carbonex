

const FlatMapStyle = [
  {
    featureType: `administrative.locality`,
    elementType: `geometry.fill`,
    stylers: [
      {
        visibility: `on`,
      },
    ],
  },
  {
    featureType: `landscape.natural`,
    elementType: `geometry.fill`,
    stylers: [
      {
        visibility: `on`,
      },
      {
        color: `#e0efef`,
      },
    ],
  },
  {
    featureType: `poi`,
    elementType: `geometry.fill`,
    stylers: [
      {
        visibility: `on`,
      },
      {
        hue: `#1900ff`,
      },
      {
        color: `#c0e8e8`,
      },
    ],
  },
  {
    featureType: `poi.business`,
    elementType: `labels.text.stroke`,
    stylers: [
      {
        visibility: `on`,
      },
      {
        weight: `0.91`,
      },
      {
        color: `#06c1ab`,
      },
    ],
  },
  {
    featureType: `poi.business`,
    elementType: `labels.icon`,
    stylers: [
      {
        weight: `1.43`,
      },
      {
        hue: `#ff0000`,
      },
      {
        visibility: `off`,
      },
    ],
  },
  {
    featureType: `road`,
    elementType: `geometry`,
    stylers: [
      {
        lightness: 100,
      },
      {
        visibility: `simplified`,
      },
    ],
  },
  {
    featureType: `road`,
    elementType: `labels`,
    stylers: [
      {
        visibility: `off`,
      },
    ],
  },
  {
    featureType: `road.local`,
    elementType: `labels.icon`,
    stylers: [
      {
        hue: `#ff0000`,
      },
    ],
  },
  {
    featureType: `transit.line`,
    elementType: `geometry`,
    stylers: [
      {
        visibility: `on`,
      },
      {
        lightness: 700,
      },
    ],
  },
  {
    featureType: `water`,
    elementType: `all`,
    stylers: [
      {
        color: `#7dcdcd`,
      },
    ],
  },
];
export default FlatMapStyle;
