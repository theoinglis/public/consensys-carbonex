import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import FlatMapStyle from './styles';
import styles from './styles.css';

class MapComponent extends Component {
  static propTypes = {
    lat: PropTypes.number.isRequired,
    lng: PropTypes.number.isRequired,
    zoom: PropTypes.number,
    addressContent: PropTypes.object,
    options: PropTypes.object,
    className: PropTypes.string,
    Marker: PropTypes.func.isRequired,
  }
  static defaultProps = {
    zoom: 10,
    options: {
      mapTypeControl: false,
      zoomControl: false,
      scrollwheel: false,
      disableDefaultUI: true,
      styles: FlatMapStyle,
    }
  }

  render() {
    const {
      lat,
      lng,
      zoom,
      addressContent,
      options,
      className,
      Marker,
    } = this.props;

    return (
      <div className={className}>
        <GoogleMapReact
          center={{ lat, lng }}
          defaultZoom={zoom}
          options={options}
        >
          <Marker
            lat={lat}
            lng={lng}
          />
        </GoogleMapReact>
        {
          addressContent
        }
      </div>
    );
  }
}
const Map = styled(MapComponent)`
  height: 70vh;
  position: relative;
`;
export default Map;

export const HillgateMap = () => (<Map
  lat={51.520632}
  lng={-0.135778}
  zoom={17}
  addressContent={

    <div className={styles.addressBox}>
      <p className={styles.addressText}>43 Whitfield Street,</p>
      <p className={styles.addressText}>5th Floor,</p>
      <p style={{ marginBottom: `20px` }} className={styles.addressText}>London, W1T 4HD</p>
      <a
        href="tel:+44-20-3290-7299"
        className={styles.addressText}
      >
        UK: +44 (0) 20 3290 7299
      </a>
      <a
        style={{ marginBottom: `20px` }}
        href="tel:+1-415-830-6083"
        className={styles.addressText}
      >
        US: +1 415 830 6083
      </a>
      <a
        href="mailto:info@hillgateconnect.com"
        className={styles.addressText}
      >
        info@hillgateconnect.com
      </a>
    </div>
  }
/>
);
