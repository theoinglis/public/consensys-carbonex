import React from 'react';
import PropTypes from 'prop-types';

import styles from './styles.css';

export default function PageSection(props) {
  return (
    <div className={styles.pageSection}>
      {props.children}
    </div>
  );
}

PageSection.propTypes = {
  children: PropTypes.object,
};
