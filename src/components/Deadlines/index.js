import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import styles from './styles.css';

export default class Deadlines extends Component {
  static propTypes = {
    containerClass: PropTypes.string,
    startDate: PropTypes.string,
    applicationDeadline: PropTypes.string,
  }

  static defaultProps = {
    startDate: `TBC`,
    applicationDeadline: `TBC`,
  }

  render() {
    const { containerClass } = this.props;
    let { startDate, applicationDeadline } = this.props;
    startDate = moment(startDate);
    applicationDeadline = moment(applicationDeadline);

    return (
      <div className={`${styles.deadlineContainer} ${containerClass}`}>
        <p className={styles.startDate}>
          Start date: {startDate.isValid() ? startDate.format(`DD/MM/YYYY`) : `TBC`}
        </p>
        <p className={styles.applicationDeadline}>
          application deadline:
          {` `}
          {applicationDeadline.isValid() ? applicationDeadline.format(`DD/MM/YYYY`) : `TBC`}
        </p>
      </div>
    );
  }
}
