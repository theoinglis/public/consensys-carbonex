import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styled, { css } from 'styled-components';

const PopoverComponentContainer = styled.div`
  position: relative;
  display: inline-block;
`;
const bottom = css`
  top: 100%;
  left: 50%;
  transform: translateX(-50%);
  ${p => p.isVisible && css`
    top: 105%;
  `}
`;
const right = css`
  left: 100%;
  top: 50%;
  transform: translateY(-50%);
  margin-left: 0;
  ${p => p.isVisible && css`
    left: 105%;
    margin-left: 16px;
  `}
`;
const left = css`
  right: 100%;
  top: 50%;
  transform: translateY(-50%);
  margin-right: 0;
  ${p => p.isVisible && css`
    right: 105%;
    margin-right: 16px;
  `}
`;
const positionCss = {
  bottom,
  right,
  left,
};
const PopoverContainer = styled.div`
  transition: ${p => p.theme.transitionDefault};
  position: absolute;
  z-index: 999999;

  pointer-events: none;
  opacity: 0;
  ${p => p.isVisible && css`
    pointer-events: auto;
    opacity: 1;
  `}
  ${p => positionCss[p.position]}
`;
const DefaultPopoverComponent = styled.div`
`;

export default class Popover extends Component {
  static propTypes = {
    className: PropTypes.string,
    isDisabled: PropTypes.bool,
    isForcedOpen: PropTypes.bool,
    isOnHover: PropTypes.bool,
    children: PropTypes.func,
    PopoverComponent: PropTypes.func,
    content: PropTypes.string,
    position: PropTypes.oneOf([`right`, `left`, `bottom`, `top`]),
  }

  static defaultProps = {
    isForcedOpen: false,
    isDisabled: false,
    isOnHover: false,
    PopoverComponent: DefaultPopoverComponent,
    position: `bottom`,
  }

  constructor() {
    super();
    this.state = {
      isOpen: false,
      isHoverOver: false,
      isClicked: false,
    };
  }

  checkIsOpen() {
    // this.setState({
    //   is
    // })
  }

  render = () => {
    const {
      className,
      isForcedOpen,
      isDisabled,
      isOnHover,
      children,
      PopoverComponent,
      content,
      position,
    } = this.props;
    const {
      isClicked,
      isHoverOver,
    } = this.state;
    let isOpen = ((isOnHover && isHoverOver) || isClicked);
    if (isForcedOpen) isOpen = true;
    else if (isDisabled) isOpen = false;

    return (
      <PopoverComponentContainer
        className={className}
        onMouseEnter={() => this.setState({ isHoverOver: true })}
        onMouseLeave={() => this.setState({ isHoverOver: false })}
      >
        { children }
        <PopoverContainer
          isVisible={isOpen}
          position={position}
        >
          <PopoverComponent isVisible={isOpen} position={position}>
            {content}
          </PopoverComponent>
        </PopoverContainer>
      </PopoverComponentContainer>
    );
  }
}
