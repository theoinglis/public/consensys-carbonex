import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styles from './styles.css';

export default class Header extends Component {
  static propTypes = {
    menuItems: PropTypes.arrayOf(PropTypes.shape({ text: PropTypes.string })),
  };

  static defaultProps = {
    menuItems: [{ text: `menu` }],
  };

  render() {
    return (
      <div className={styles.container}>
        <div className={styles.logo}>
          <h1>hillgate</h1>
        </div>
        <div className={styles.linksContainer}>
          { this.props.menuItems.length > 0 &&
            this.props.menuItems.map(({ text }) => <div>{text}</div>) }
        </div>
        <div className={styles.buttonsContainer}>
          <button className={styles.button}>sign up</button>
          <button className={styles.button}>log in</button>
        </div>
      </div>
    );
  }
}
