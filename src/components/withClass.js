import React from 'react';
import PropTypes from 'prop-types';
import shortid from 'shortid';

export default (StyledComponent) => {
  const uniqueIdentifier = shortid.generate();
  const renderComponent = ({ className, ...rest }) => {
    return (<StyledComponent className={`${uniqueIdentifier} ${className}`} {...rest} />);
  };
  renderComponent.propTypes = {
    className: PropTypes.string,
  };
  renderComponent.className = uniqueIdentifier;
  return renderComponent;
};
