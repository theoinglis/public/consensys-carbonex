import React, { Component } from 'react';
import PropTypes from 'prop-types';
import CloseIcon from 'components/CloseIcon';
import styled, { css, ThemeProvider } from 'styled-components';
import { Portal } from 'react-portal';

export const CardTheme = (props) => ({
  colorModalBackground: `rgba(13,103,211,0.81)`,
  colorModalContentBackground: `white`,
  colorModalContentText: props.colorText,
  boxShadowModal: props.boxShadowDefault,
  ...props,
});
export const SimpleTextTheme = (props) => ({
  colorModalBackground: props.colorAccentComplementary,
  colorModalContentBackground: `transparent`,
  colorModalContentText: props.colorAccent,
  boxShadowModal: `none`,
  ...props,
});

export const DefaultContainer = styled.div`
  position: fixed;
  z-index: 999999999999999;
  top: 0; 
  left: 0;
  right: 0;
  bottom: 0;
  transition: all 0.6s ${(p) => p.theme.easeOutQuart} 0.2s;
  pointer-events: none;
  opacity: 0;
  background: ${(p) => p.theme.colorModalBackground};
  overflow: scroll;
  padding: 120px 20px;

  ${(props) => props.isOpen && css`
    opacity: 1;
    transition: all 0.6s ${(p) => p.theme.easeOutQuart};
    pointer-events: auto;
  `}
`;
export const DefaultModalContentContainer = styled.div`
  position: relative;
  margin: 0 auto;
  padding: 16px 30px;
  border-radius: 4px;
  background: ${(p) => p.theme.colorModalContentBackground};
  color: ${(p) => p.theme.colorModalContentText};
  box-shadow: ${(p) => p.theme.boxShadowModal};
  box-sizing: border-box;
  max-width: 520px;
  transition: all 0.6s ${(p) => p.theme.easeOutQuart};
  transform: translateY(20px);
  opacity: 0;
  min-height: 30vh;
  ${(p) => p.isOpen && css`
    transform: translateY(0);
    opacity: 1;
  `}
`;
export const DefaultCloseButton = styled.button`
  position: fixed;
  right: 49px;
  width: 40px;
  height: 40px;
  top: 31px;
  cursor: pointer;
  opacity: 0;
  transition: all 0.6s ${(p) => p.theme.easeOutQuart};
  background: transparent;
  outline: none;
  border: none;
  color: white;
  z-index: 999;
  ${(p) => p.isOpen && css`
    opacity: 1;
    top: 50px;
  `}
`;

export default class Modal extends Component {
  static propTypes = {
    canClose: PropTypes.bool,
    onClose: PropTypes.func.isRequired,
    isOpen: PropTypes.bool.isRequired,
    closeOnClickOff: PropTypes.bool,
    children: PropTypes.node.isRequired,
    Container: PropTypes.func,
    ContentContainer: PropTypes.func,
    CloseButton: PropTypes.func,
    theme: PropTypes.func,
  }

  static defaultProps = {
    canClose: true,
    Container: DefaultContainer,
    ContentContainer: DefaultModalContentContainer,
    CloseButton: DefaultCloseButton,
    closeOnClickOff: true,
    theme: SimpleTextTheme,
  };

  componentWillReceiveProps = (nextProps) => {
    const isTogglingOpen = nextProps.isOpen !== this.props.isOpen;
    if (isTogglingOpen) {
      if (nextProps.isOpen) {
        this.setState({ prevOverflow: document.documentElement.style.cssText });
        document.documentElement.style.cssText = `overflow-y: hidden`;
      } else {
        document.documentElement.style.cssText = `overflow-y: auto`;
      }
    }
  }

  render() {
    const {
      children,
      isOpen,
      Container,
      ContentContainer,
      CloseButton,
      onClose,
      canClose,
      closeOnClickOff,
      theme,
    } = this.props;

    return (
      <Portal>
        <ThemeProvider theme={theme}>
          <Container isOpen={isOpen} onClick={canClose && closeOnClickOff && onClose}>
            {canClose ? (<CloseButton isOpen={isOpen} onClick={onClose}><CloseIcon /></CloseButton>) : null}
            <ContentContainer isOpen={isOpen} onClick={(e) => e.stopPropagation()}>
              {children}
            </ContentContainer>
          </Container>
        </ThemeProvider>
      </Portal>
    );
  }
}
