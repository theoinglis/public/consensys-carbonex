import React from 'react';
import PropTypes from 'prop-types';

import anime from 'animejs';

import styles from './styles.css';

export default class MenuIcon extends React.Component {
  static propTypes = {
    strokeWidth: PropTypes.number,
    width: PropTypes.number,
    height: PropTypes.number,
  };

  static defaultProps = {
    strokeWidth: 1,
    width: 10,
    height: 15,
  };

  componentDidMount() {
    anime({
      targets: `.${styles.menuIcon} path`,
      strokeDashoffset: [anime.setDashoffset, 0],
      opacity: 1,
      easing: `easeInOutSine`,
      duration: 1000,
      delay: 500,
      loop: false,
      elasticity: (el, i) => (200 + (i * 200)),
    });
  }

  handleMouseEnter = () => {
    anime({
      targets: `.${styles.menuIcon} path`,
      strokeDashoffset: [50, 0],
      easing: `easeInOutSine`,
      duration: 500,
      loop: false,
      elasticity: (el, i) => (200 + (i * 200)),
    });
  }

  render() {
    const { strokeWidth, width, height } = this.props;
    const outerOffset = strokeWidth / 2;
    let upperTransform;
    let lowerTransform;
    let middleTransform;

    return (
      <div className={styles.menuIcon} onMouseEnter={this.handleMouseEnter}>
        <svg viewBox={`0 0 ${width}px ${height}px`}>
          <g fill="none" stroke="currentColor" strokeWidth={strokeWidth}>
            <path transform={middleTransform} d={`M-20,${height / 2} H${width + 20},${height / 2}`} />
            <path transform={lowerTransform} d={`M-20,${height - outerOffset} H${width + 20},${height - outerOffset}`} />
            <path transform={upperTransform} d={`M-20,${0 + outerOffset} H${width + 20},0`} />
          </g>
        </svg>
      </div>
    );
  }
}
