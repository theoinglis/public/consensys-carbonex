import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import classnames from 'classnames';

import styles from './styles.css';

export default class DaysLeft extends Component {
  static propTypes = {
    deadline: PropTypes.string,
    customStyle: PropTypes.string,
    noDaysCustomStyle: PropTypes.string,
  }

  daysLeftCalc = (deadline) => {
    const daysDiff = moment(deadline).diff(moment.now(), `days`);
    return daysDiff >= 0 ? daysDiff : -1;
  }

  render() {
    let daysLeftElement = null;
    const { deadline, customStyle, noDaysCustomStyle } = this.props;
    const daysLeft = deadline ? this.daysLeftCalc(deadline) : false;

    if (daysLeft === false) {
      daysLeftElement = <div />;
    } else if (daysLeft > 0) {
      daysLeftElement = (
        <p className={classnames(styles.daysLeft, customStyle)}>
          <b className={styles.daysLeftNumber}>
            {daysLeft}
          </b>
          days left to apply
        </p>
      );
    } else if (daysLeft === 0) {
      daysLeftElement = (
        <p className={classnames(styles.daysLeft, customStyle)}>
          Less than 24 hours left to apply!
        </p>
      );
    } else {
      daysLeftElement = (
        <p className={classnames(styles.noDaysLeft, customStyle, noDaysCustomStyle)}>
          The application deadline has passed.
        </p>
      );
    }

    return daysLeftElement;
  }
}
