import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';

const themes = {
  up: css`
    transition: ${p => p.theme.transitionDefault};
    transition-delay: ${p => p.scrollDelay}s;
    opacity: 0;
    top: ${p => p.movementSize}px;
    ${props => props.isAbove && css`
      opacity: 1;
      top: 0;
    `}
  `,
};
const ScrollItemContainer = styled.div`
  position: relative;
  display: ${p => (p.isInline ? `inline-block` : `block`)};
  opacity: 0;
  ${p => themes[p.componentTheme]}
  ${p => p.isAbove && css`
    opacity: 1;
  `}
`;

export default class ScrollItem extends Component {
  static propTypes = {
    children: PropTypes.number.isRequired,
    className: PropTypes.string,
    scrollContainer: PropTypes.object,
    scrollVisibilityAdjustment: PropTypes.number,
    componentTheme: PropTypes.string,
    scrollDelay: PropTypes.number,
    movementSize: PropTypes.number,
    onVisible: PropTypes.func,
    isInline: PropTypes.bool,
  }
  static defaultProps = {
    scrollContainer: window,
    scrollVisibilityAdjustment: 120,
    componentTheme: `up`,
    scrollDelay: 0,
    movementSize: 8,
    onVisible: () => null,
    isInline: false,
  }

  constructor(props) {
    super(props);
    this.state = {
      isAbove: false,
    };
  }

  componentDidMount = () => {
    this.props.scrollContainer.addEventListener(`scroll`, this.handleScroll);
  }
  componentWillUnmount = () => {
    this.props.scrollContainer.removeEventListener(`scroll`, this.handleScroll);
  }

  handleScroll = (e) => {
    this.checkVisibility();
  }
  checkVisibility = () => {
    const {
      scrollContainer,
      scrollVisibilityAdjustment,
    } = this.props;
    const scrollWindowHeight = scrollContainer.innerHeight;
    const elementRect = this.container.getBoundingClientRect();
    const elementDistanceFromTop = elementRect ? elementRect.top : 0;


    if (!this.state.isAbove && (scrollWindowHeight - scrollVisibilityAdjustment) > elementDistanceFromTop) {
      this.setState({
        isAbove: true,
      });
      this.props.onVisible();
    }
  }

  render() {
    const {
      children,
      componentTheme,
      scrollDelay,
      movementSize,
      className,
      isInline,
    } = this.props;
    const {
      isAbove,
    } = this.state;
    return (
      <ScrollItemContainer
        className={className}
        componentTheme={componentTheme}
        scrollDelay={scrollDelay}
        movementSize={movementSize}
        isAbove={isAbove}
        isInline={isInline}
        innerRef={(c) => {
          if (c) {
            this.container = c;
            this.checkVisibility();
          }
        }}
      >
        { children }
      </ScrollItemContainer>
    );
  }
}
