import styled, { css } from 'styled-components';

const themes = {
  noMovement: css`
  `,
  left: css`
    left: -20px;
    ${props => props.isVisible && css`
      left: 0;
    `}
  `,
  right: css`
    right: -20px;
    ${props => props.isVisible && css`
      right: 0;
    `}
  `,
  up: css`
    top: -20px;
    ${props => props.isVisible && css`
      top: 0;
    `}
  `,
};
export default styled.div.attrs({
  enterDelay: p => p.enterDelay || 0.3,
  delay: p => p.delay || 0,
  componentTheme: p => p.componentTheme || `noMovement`,
  isInline: p => p.isInline || false,
})`
  display: ${p => (p.isInline ? `inline-block` : `block`)};
  position: relative;
  transition: all 0.6s ${p => p.theme.easeOutQuartic};
  opacity: 0;
  pointer-events: none;
  transition-delay: ${p => p.delay}s;

  ${props => props.isVisible && css`
    pointer-events: auto;
    opacity: 1;
    transition-delay: ${p => p.enterDelay + p.delay}s;
  `}
  ${p => themes[p.componentTheme]}
`;
