import { createSelector } from 'reselect';

const getFormModel = form => (form && form.getModel());
export const getCoordinatesFromForm = createSelector(
  getFormModel,
  model => (model && model.location && model.location.location) || {},
);
