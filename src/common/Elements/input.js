import styled from 'styled-components';

export const OutlineInput = styled.input`
  outline: none;
  border: 1px solid currentColor;
  width: 100%;
  line-height: 14px;
  font-size: 14px;
  margin: 0 0 4px;
  padding: 12px 12px;
  height: 42px;
  box-sizing: border-box;
  color: black;
  transition: all 0.6s ${p => p.theme.easeOutQuart};
`;

export default OutlineInput;
