import styled from 'styled-components';
import Map from 'components/Map';
import v from 'components/StyleVariables';

export const HeaderMap = styled(Map)`
  display: block;
  width: 100%;
  height: 200px;
  max-height: 500px;
`;

export const FullMap = styled(Map)`
  transition: all 0.4s ${v.easeOutQuart};
  display: block;
  width: 100%;
  height: ${props => props.height || `100%`};
`;
