import styled, { withTheme } from 'styled-components';
import React from 'react';
import PropTypes from 'prop-types';
import { DotLoader } from 'react-spinners';

const DefaultSpinnerComponentWithTheme = (props) => (
  <DotLoader size={40} color={props.theme.colorAccent} className={props.className} />
);
const DefaultSpinnerComponent = withTheme(DefaultSpinnerComponentWithTheme);
const DefaultStyledSpinnerComponent = styled(DefaultSpinnerComponent)`
  display: inline-block;
  color: ${(p) => p.theme.colorAccent} !important;
  width: 40px;
  height: 40px;
`;
export default class LoadingIndicator extends React.Component {
  static propTypes = {
    SpinnerComponent: PropTypes.func,
    className: PropTypes.string,
    children: PropTypes.node,
    isLoading: PropTypes.bool,
  };

  static defaultProps = {
    SpinnerComponent: DefaultStyledSpinnerComponent,
    isLoading: true,
  };

  render = () => {
    const {
      SpinnerComponent,
      className,
      children,
    } = this.props;

    return this.props.isLoading
      ? (
        <div className={className}>
          <SpinnerComponent className="spinner" />
          {children && (
            <div className="message">
              {children}
            </div>
          )}
        </div>
      )
      : null;
  }
}

export const InlineLoader = styled(LoadingIndicator)`
  display: inline-block;
`;
export const PageLoader = styled(LoadingIndicator)`
  width: 100%;
  text-align: center;
  margin: 16px 0 30px;
  & > .spinner {
    display: block;
    margin: 12px auto;
  }
  & > .message {
    color: #9a9b9c;
    font-style: italic;
  }
`;

export const CenterLoader = styled(LoadingIndicator) `
  width: 100%;
  text-align: center;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  align-items: center;
  & > .message {
    margin-top: 20px;
    color: #9a9b9c;
    font-style: italic;
  }
`;
