import styled from 'styled-components';

export const PullLeft = styled.div`float: left;`;
export const PullRight = styled.div`float: right;`;
export const FloatClear = styled.div`clear: both;`;
