import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Isvg from 'react-inlinesvg';

const IconComponent = props => (
  <div className={props.className}>
    <Isvg src={props.icon} />
  </div>
);
IconComponent.propTypes = {
  className: PropTypes.string,
  icon: PropTypes.string,
};

export const Icon = styled(IconComponent).attrs({
  size: p => p.size || `100%`,
  color: p => p.color || `currentColor`,
  backgroundColor: p => p.backgroundColor || `transparent`,
}) `
  height: ${p => p.size};
  width: ${p => p.size};
  color: ${p => p.color};
  background: ${p => p.backgroundColor};
  overflow: hidden;
  svg {
    width: 100%;
    height: 100%;
  }
`;
export const FillIcon = Icon.extend.attrs({
  padding: p => p.padding || 0,
}) `
  height: 100%;
  width: 100%;
  padding: ${p => p.padding}px;
  position: absolute;
  top: 0;
  left: 0;
`;

export const CircleIcon = styled(IconComponent).attrs({
  size: p => p.size || 30,
  color: p => p.color || p.theme.colorPrimaryContrast,
  backgroundColor: p => p.backgroundColor || p.theme.colorPrimary,
}) `
  height: ${p => p.size}px;
  width: ${p => p.size}px;
  border-radius: ${p => p.size / 2}px;
  background: ${p => p.backgroundColor};
  color: ${p => p.color};
  overflow: hidden;
  svg {
    width: 100%;
    height: 100%;
  }
`;

export const CategoryIcon = styled(IconComponent).attrs({
  size: p => p.size || 8,
  color: p => p.color || `currentColor`,
  backgroundColor: p => p.backgroundColor || `transparent`,
}) `
  display: inline-block;
  height: ${p => p.size}px;
  width: ${p => p.size}px;
  background: ${p => p.backgroundColor};
  color: ${p => p.color};
  padding: 0 8px 0 0;
`;

export default Icon;
