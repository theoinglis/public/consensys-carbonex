import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Isvg from 'react-inlinesvg';

import FullLogoSvg from 'static/logos/full-logo.svg';

const LogoComponent = ({ logo, children, className, ...rest }) => (
  <div className={className}>
    <Isvg src={logo} {...rest}>
      {children}
    </Isvg>
  </div>
);
LogoComponent.propTypes = {
  logo: PropTypes.string.isRequired,
  className: PropTypes.string,
  children: PropTypes.node,
};

export const FullLogo = styled(props => (
  <LogoComponent
    logo={FullLogoSvg}
    {...props}
  >
    CARBONEX
  </LogoComponent>
))`
  display: inline-block;
  height: 30px;
  width: 120px;
`;
