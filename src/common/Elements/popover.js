import React from 'react';
import styled, { css } from 'styled-components';

import PopoverComponent from 'components/Popover';

const left = css`
  top: 50%;
  left: 100%;
`;
const right = css`
  top: 50%;
  right: 100%;
`;
const positionCss = {
  right,
  left,
};
const StyledTooltip = styled(PopoverComponent) `
  background: rgba(255,255,255,0.9);
  border-radius: 4px;
  border: 1px solid rgba(0,0,0,0.4);
  box-shadow: ${p => p.theme.boxShadowDefault};
  font-size: 12px;
  padding: 16px;
  min-width: 140px;
  color: black;
  &:before {
    transition: ${p => p.theme.transitionDefault};
    position: absolute;
    content: '';
    width: 0;
    border-top: 1px dashed ${p => p.theme.colorAccent};
    ${(p) => { return positionCss[p.position]; }}
    ${p => p.isVisible && css`
      width: 16px;
    `}
  }
`;
export const Tooltip = ({ position, ...rest }) => (
  <PopoverComponent
    isOnHover
    position={position || `right`}
    PopoverComponent={StyledTooltip}
    {...rest}
  />
);
