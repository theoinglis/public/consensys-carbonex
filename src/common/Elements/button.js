import v from 'components/StyleVariables';
import styled, { css } from 'styled-components';
import { Link } from 'react-router-dom';

export const BlankButton = styled.button`
  transition: all 0.4s ${v.easeOutCubic};
  cursor: pointer;
  background: transparent;
  font-size: inherit;
  border-radius: 0;
  border: none;
  outline: none;
  text-align: center;
  text-decoration: none;
  color: currentColor;
  margin: 5px;
  &:disabled {
    pointer-events: none;
    opacity: 0.6;
  }
`;
export const BlankAnchor = BlankButton.withComponent(`a`);
export const BlankLink = BlankButton.withComponent(Link);

const buttonThemes = {
  primary: css`
    background: ${p => p.theme.colorAccent};
    color: ${p => p.theme.colorAccentComplementary};
    font-weight: 500;
  `,
};
export const DefaultButton = BlankButton.extend.attrs({
  buttonTheme: p => p.buttonTheme || `primary`,
})`
  padding: 12px 18px;
  font-size: 14px;
  font-weight: 500;
  ${p => buttonThemes[p.buttonTheme]}
  &:hover {
    box-shadow: 0 2px 5px 0 rgba(120, 120, 120, .62);
  }
`;
export const DefaultLink = DefaultButton.withComponent(Link);
export const DefaultAnchor = DefaultButton.withComponent(`a`);


export const TextButton = BlankButton.extend`
  transition: ${p => p.theme.transitionDefault};
  border: 1px solid transparent;
  padding: 5px 18px;
  font-size: 18px;
  font-weight: 300;
  color: ${p => p.theme.colorAccent};
  ${p => p.onLeft && css`margin-right: 8px;`}
  ${p => p.onRight && css`margin-left: 8px;`}
  &:hover {
    text-decoration: underline;
  }
`;
export const TextLink = TextButton.withComponent(Link);
export const TextAnchor = TextButton.withComponent(`a`);

export const InlineAnchor = BlankAnchor.extend`
  font-weight: 500;
  text-decoration: underline;
  margin: 0;
  padding: 0;
`;

export const AddButton = BlankButton.extend`
  position: relative;
  display: block;
  border-radius: 5px;
  border: 2px solid ${p => p.theme.colorInput};
  height: ${p => p.theme.heightInputNumber * 1 + 2}px;
  padding-left: 34px;
  width: 100%;
  text-align: left;
  transition: ${p => p.theme.transitionDefault};
  font-size: 0.8em;
  &:hover {
    box-shadow: ${p => p.theme.boxShadowDefault};
  }

  &:before {
    display: inline-block;
    content: '+';
    position: absolute;
    text-align: center;
    top: 5px;
    left: 8px;
    height: 18px;
    width: 18px;
    font-size: 22px;
    line-height: 18px;
    border-radius: 9px;
    background: ${p => p.theme.colorPrimary};
    color: ${p => p.theme.colorPrimaryContrast}
  }
`;

export const IconButton = BlankButton.extend.attrs({
  color: p => p.color || `currentColor`,
  backgroundColor: p => p.backgroundColor || `white`,
  borderColor: p => p.borderColor || `white`,
  size: p => p.size || 30,
}) `
  position: relative;
  width: ${p => p.size}px;
  height: ${p => p.size}px;
  border-radius: ${p => p.size/2}px;
  background: transparent;
  border: 1px solid ${p => p.theme.colorAccent};
  color: ${p => p.theme.colorAccent};
  box-sizing: border-box;
  &:hover {
    background: ${p => p.theme.colorAccent};
    color: ${p => p.theme.colorAccentComplementary};
  }
  &:disabled {
    opacity: 0.7;
    pointer-events: none;
  }
`;
export const IconAnchor = IconButton.withComponent(`a`);
export const IconLink = IconButton.withComponent(Link);

export default DefaultButton;
