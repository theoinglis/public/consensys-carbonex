import 'react-select/dist/react-select.css';
import React from 'react';
import styled from 'styled-components';
import v from 'components/StyleVariables';
import Select, { Creatable } from 'react-select';

// Overwrite Inherited Styles
export const ClearedSelect = styled(Select) `
  .Select-control {
    height: 30px !important;
    border: 1px solid currentColor !important;
    box-shadow: none !important;
    cursor: pointer;
  }
  .Select-input {
    height: 30px !important;
  }
  .Select-value {
    height: 30px;
    line-height: 30px !important;
    padding: 0 8px 0 8px !important;
  }
  .Select-value-label {
    color: inherit !important;
  }
`;
export const ExpandableSelect = ClearedSelect.extend`
  .Select-control {
    height: auto !important;
    border: 1px solid currentColor !important;
    box-shadow: none !important;
    cursor: pointer;
  }
  .Select-input {
    height: auto !important;
    position: absolute;
  }
  &&& .Select-value {
    height: auto;
    line-height: 30px !important;
    padding: 0 8px 0 8px !important;
    position: relative;
    overflow: auto;
    white-space: initial;
    text-overflow: initial;
  }
  .Select-value-label {
    color: inherit !important;
  }
`;
export const TextSelect = ExpandableSelect.extend`
  .Select-control {
    background-color: transparent !important;
    color: ${p => p.theme.colorAccent} !important;
    border-radius: 0;
    border: none !important;
    font-weight: 500;
  }
  .Select-input {
    background-color: transparent !important;
  }
  .Select-menu-outer {
    background: ${p => p.theme.colorAccent};
    color: black;
    border-radius: 0;
    border: 0;
  }
  .Select-option {
    background: transparent;
    color: black;
    &:hover {
      text-decoration: underline;
    }
    &.is-focused {
      background: transparent;
    }
    &.is-selected {
      display: none;
    }
  }
  .Select-menu-outer {
    margin-top: 8px;
    transition: ${p => p.theme.transitionDefault};
  }
  &.is-open .Select-menu-outer {
    margin-top: 2px;
  }
`;
export const OutlineSelect = ClearedSelect.extend`
  .Select {
    &.is-open, &:hover, &:focus {
      .Select-control {
          transform: scale(1.001);
          box-shadow: 0 2px 5px 0 rgba(120, 120, 120, .62) !important;
      }
    }
   }
  .Select-control {
    cursor: pointer;
    border-radius: 4px;
    width: 100%;
    font-size: 14px;
    margin: 0 0 4px;
    box-sizing: border-box;
    color: inherit;
    transition: all 0.6s ${v.easeOutQuart};
    transform: scale(1);
    &:hover, &:focus {
      transform: scale(1.001);
      box-shadow: 0 2px 5px 0 rgba(120, 120, 120, .62) !important;
    }
  }
  .Select-menu-outer {
    border: 1px solid currentColor !important;
  }
  .Select-option {
    transition: all 0.6s ${v.easeOutQuart};
    margin: 4px 0;
    :hover, &.is-focused {
      transition: all 0.2s ${v.easeOutQuart};
      background: ${v.colorInputOptionHover};
    }
    &.is-selected {
      background: ${v.colorInputActive};
      color: white;
    }
  }
`;
export const OutlineCreatable = OutlineSelect.withComponent(Creatable);

const valueComponent = props => (
  <span className={props.className}>
    <span className={`content`}>
      {props.children}
    </span>
    <span className={`close`} onClick={() => props.onRemove(props.value)}>&#10005;</span>
  </span>
);
export const OutlineSelectValue = styled(valueComponent)`
  display:inline-block;
  border: 1px solid currentColor;
  border-radius: 3px;
  margin: 1px 4px 1px 1px;
  vertical-align: top;
  & > .content {
      display: inline-block;
      padding: 3px 6px;
  }
  & > .close {
    transition: all 0.4s ${v.easeOutQuart};
    display: inline-block;
    margin: 3px 8px;
    padding-left: 1px;
    cursor: pointer;
    height: 20px;
    width: 20px;
    border-radius: 10px;
    text-align: center;
    font-size: 12px;
    line-height: 20px;
    &:hover {
      background: ${v.colorPrimary};
      color: white;
    }
  }
`;

export default OutlineSelect;
