import styled, { css } from 'styled-components';

export const h1 = styled.h1`
  font-size: 32px;
  font-weight: 300;
  margin-bottom: 0.25em;
  line-height: 1.2em;
  ${p => p.theme.media.phablet` font-size: 34px; `}
  ${p => p.theme.media.tablet` font-size: 40px; `}
  ${p => p.theme.media.desktop` font-size: 46px; `}
`;
export const h2 = h1.withComponent(`h2`).extend`
  font-size: 28px;
  font-weight: 400;
  margin-bottom: 0.25em;
  color: ${p => p.theme.colorAccent};
`;
export const h3 = h1.withComponent(`h3`).extend`
  font-size: 20px;
  font-weight: 400;
  margin-bottom: 0.25em;
`;
export const p = styled.p`
  margin-bottom: 0.4em;
  font-weight: 300;
  ${p => p.theme.media.tablet` font-size: 18px; `}
`;
