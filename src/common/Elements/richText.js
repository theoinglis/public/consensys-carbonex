import 'react-quill/dist/quill.bubble.css';
import React from 'react';
import styled, { css } from 'styled-components';
import v from 'components/StyleVariables';
import Quill from 'react-quill';

const BaseRichText = styled(Quill) `
  box-sizing: border-box;
  margin: 0 0 4px;
  .ql-editor.ql-editor {
    line-height: 1.2em;
    padding: 0;
    font-family: ${v.fontSans};
    h1 {
      font-size: 1.3em;
      line-height: 1.5em;
      font-family: ${v.fontSerif};
    }
    h2 {
      font-size: 1.1em;
      line-height: 1.3em;
      font-family: ${v.fontSans};
    }
    h1, h2 {
      font-weight: 600;
      margin: 12px 0 6px;
    }
    ul, ol {
      margin: 6px 0;
      padding-left: 0.8em;
    }
    li {
      padding-bottom: 0.3em;
    }
    p {
      margin-bottom: 8px;
      line-height: 1.5em;
    }
  }
`;

export const OutlineRichText = BaseRichText.extend`
  border: 1px solid currentColor;
  border-radius: 4px;
  min-height: 140px;
  transition: all 0.6s ${v.easeOutQuart};
  transform: scale(1);
  .ql-editor.ql-editor {
    padding: 12px;
  }

  ${props => props.isFocused && css`
    transform: scale(1.001);
    box-shadow: 0 2px 5px 0 rgba(120, 120, 120, .62);
  `}
`;
export const ReadOnlyRichTextComp = BaseRichText.extend`
  border: none;
  border-radius: 0;
  pointer-events: none;
  .ql-editor.ql-editor {
    padding: 0;
  }
`;
export const ReadOnlyRichText = ({ children, ...rest }) => {
  const content = children.toJS ? children.toJS() : children;
  return (<ReadOnlyRichTextComp value={content || null} theme="bubble" {...rest} />);
};

export default OutlineRichText;
