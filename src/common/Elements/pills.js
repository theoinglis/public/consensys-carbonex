import styled from 'styled-components';

export const Pill = styled.div`
  display: inline-block;
  font-family: Raleway;
  font-size: 11px;
  font-weight: 400;
  border-radius: 4px;
  padding: 4px 8px;
  margin-right: 6px;
  margin-bottom: 4px;
  box-shadow: ${p => p.theme.boxShadowSoft};
`;
