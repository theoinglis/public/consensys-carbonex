import v from 'components/StyleVariables';
import styled, { css, keyframes } from 'styled-components';

const pulse = keyframes`
0% {
  transform: scale(1);
  opacity: 1;
}
20% {
  transform: scale(3);
  opacity: 0;
}
100% {
  transform: scale(3);
  opacity: 0;
}
`;

export const Dot = styled.span`
margin: 2px 4px;
display: inline-block;
width: 6px;
height: 6px;
border-radius: 3px;
background: ${props => props.color || v.colorPrimary};
transition: all 0.4s ease-out;
transform: scale(0);
${props => props.isVisible && css`
  transform: scale(1);
`}
&:before {
  ${props => !props.isVisible && css`
    display: none;
  `}
  position: absolute;
  z-index: -1;
  content: " ";
  display: inline-block;
  width: 6px;
  height: 6px;
  border-radius: 3px;
  background: ${props => props.color || v.colorPrimary};
  ${props => props.isActive && css`
    animation: ${pulse} 4s ease-out infinite;
  `}
}
`;
