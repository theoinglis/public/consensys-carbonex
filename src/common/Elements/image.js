import styled from 'styled-components';

export const CirclePhoto = styled.div.attrs({
  size: p => p.size || 120,
  borderRadius: p => p.borderRadius || 3,
  backgroundColor: p => p.backgroundColor || `white`,
})`
  height: ${p => p.size}px;
  width: ${p => p.size}px;
  border-radius: ${p => p.size / 2}px;
  border: ${p => p.borderRadius}px solid white;
  background-color: ${p => p.backgroundColor};
  background-size: cover;
  background-position: center;
  background-image: url(${p => p.url});
  box-shadow: ${p => p.theme.boxShadowSoft};
  box-sizing: border-box;
`;
