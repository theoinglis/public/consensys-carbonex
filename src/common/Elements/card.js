import styled, { css } from "styled-components";
import v from "components/StyleVariables";

export const Main = styled.div`
  transition: all 0.6s ${v.easeOutCubic};
  min-height: ${props => props.minHeight || `300px`};
  width: 100%;
  overflow: hidden;
  box-sizing: border-box;
  border: 1px solid rgba(220,220,220,0.4);
  border-radius: 4px;
  box-shadow: 0 2px 7px 0 rgba(159, 178, 200, .62);
  transform: scale(1);
  backface-visibility: hidden;
  -webkit-font-smoothing: antialiased;
  &:hover {
    box-shadow: 0 6px 7px 0 rgba(159, 178, 200, .62);
    ${props => !props.disableTransform && css`
      transform: scale(1.005);
    `}
  }
  ${props => props.isHidden && css`
    height: 0;
    box-shadow: none;
    border-color: transparent;
    pointer-events: none;
  `}
`;

export default Main;
