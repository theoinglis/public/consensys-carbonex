import 'react-infinite-calendar/styles.css';
import React from 'react';
import InfiniteCalendar from 'react-infinite-calendar';
import styled, { css } from 'styled-components';
import v from 'components/StyleVariables';

const ThemedCalendar = ({ calendarTheme, ...rest }) => ((
  <InfiniteCalendar
    theme={
      calendarTheme || {
        weekdayColor: v.colorPrimary,
      }
    }
    {...rest}
  />
));
export const Calendar = styled(ThemedCalendar) `
  transition: ${p => p.theme.transitionDefault};
  position: absolute !important;
  transform: translateY(20px);
  opacity: 0;
  pointer-events: none;
  box-shadow: none;
  ${p => p.shouldShow && css`
    transform: translateY(0);
    opacity: 1;
    pointer-events: auto;
    box-shadow: ${p.theme.boxShadowDefault};
  `}
`;
export default Calendar;
