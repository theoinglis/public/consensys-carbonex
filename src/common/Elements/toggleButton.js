import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';

const HiddenInput = styled.input`
  display: none;
`;
const ToggleButton = props => (
  <label className={props.className}>
    <div className={`container`}>
      <div className={`thumb`} />
    </div>
    <HiddenInput
      type="checkbox"
      name={props.name}
      checked={props.isChecked}
      onChange={() => props.onChange(!props.isChecked)}
      className={`input`}
    />
  </label>
);
ToggleButton.propTypes = {
  className: PropTypes.string,
  name: PropTypes.string,
  isChecked: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired,
};

export const OutlineToggleButton = styled(ToggleButton).attrs({
  length: 18,
})`
  & > .container {
    transition: ${p => p.theme.transitionDefault};
    position: relative;
    height: ${p => p.theme.heightInput};
    width: ${p => p.theme.heightInputNumber * 1 + p.length}px;
    border-radius: ${p => p.theme.heightInputNumber / 2}px;
    border: 1px solid currentColor;
    box-sizing: border-box;
    & > .thumb {
      transition: ${p => p.theme.transitionDefault};
      position: absolute;
      top: 1px;
      left: 1px;
      height: ${p => p.theme.heightInputNumber - 4}px;
      width: ${p => p.theme.heightInputNumber - 4}px;
      border-radius: ${p => (p.theme.heightInputNumber - 4) / 2}px;
      border: 1px solid currentColor;
      box-sizing: border-box;
      background: white;
      box-shadow: ${p => p.theme.boxShadowDefault};
    }
  } 
  ${p => p.isChecked && css`
    & > .container {
      background: ${p => p.theme.colorPrimary};
      & > .thumb {
        left: ${p => p.length + 2}px; 
      }
    }

  `}
`;

export default OutlineToggleButton;
