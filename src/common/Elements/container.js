import React from 'react';
import styled, { css } from 'styled-components';

export const PageContainer = styled.div`
  padding: ${p => p.theme.paddingSection};
`;

export const CenteredPageContainer = styled(PageContainer)`
  max-width: ${p => p.theme.maxPageWidth};
  margin: 0 auto;
  position: relative;
  ${p => p.removePadding && `padding: 0;`}
`;

export const PageSection = styled(({ removePadding, background, children, ...rest }) => (
  <div {...rest}>
    <CenteredPageContainer removePadding={removePadding}>
      {children}
    </CenteredPageContainer>
  </div>
)) `
  background: ${p => p.background ? p.background(p) : `transparent`};
`;

export const FullPageSection = PageSection.extend`
  min-height: calc(100vh - ${p => p.theme.heightHeader});
`;

