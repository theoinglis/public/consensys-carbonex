import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import * as e from 'common/Elements';

import LinkedInIcon from 'static/logos/social/LinkedIn.svg';
import EarthIcon from 'static/icons/earth.svg';

const Container = styled.div`
  display: inline-block;
  text-align: center;
  width: 240px;
  font-weight: 300;
  margin: 0 12px 60px;
  vertical-align: top;
`;
const Photo = styled.div`
  display: inline-block;
  background-image: url(${p => p.url});
  background-size: cover;
  background-repeat: no-repeat;
  width: ${p => p.size}px;
  height: ${p => p.size}px;
  border-radius: ${p => p.size / 2}px;
  margin-bottom: 40px;
`;
const Name = styled.div`
  font-size: 24px;
  color: ${p => p.theme.colorAccent};
  margin-bottom: 6px;
`;
const Position = styled.div`
  font-size: 16px;
  line-height: 20px;
  margin-bottom: 3px;
`;
const Qualifications = styled.div`
  font-size: 12px;
  min-height: 34px;
  line-height: 17px;
  opacity: 0.5;
  margin-bottom: 6px;
`;
const Bio = styled.div`
  line-height: 1.8em;
  min-height: 170px;
  margin-bottom: 16px;
`;
const Linkedin = styled.div`
`;
const LinkIcon = e.Icon.default.extend`
  display: inline-block;
`;

export default class Profile extends Component {
  static propTypes = {
    photoUrl: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    position: PropTypes.string.isRequired,
    bio: PropTypes.string.isRequired,
    linkedinUrl: PropTypes.string.isRequired,
  }

  render = () => {
    const {
      photoUrl,
      name,
      position,
      bio,
      qualifications,
      linkedinUrl,
      isLinkedin,
    } = this.props;
    return (
      <Container>
        <Photo
          url={photoUrl}
          size={160}
        />
        <Name>
          {name}
        </Name>
        <Position>
          {position}
        </Position>
        <Qualifications>
          {qualifications}
        </Qualifications>
        <Bio>
          {bio}
        </Bio>
        <Linkedin>

          <e.Button.BlankAnchor href={linkedinUrl} target={`_blank`}>
            {isLinkedin === false ? (
              <LinkIcon
                icon={EarthIcon}
                size={`26px`}
              />
            ) : (
              <LinkIcon
                icon={LinkedInIcon}
                size={`30px`}
              />
            )}
          </e.Button.BlankAnchor>
        </Linkedin>
      </Container>
    );
  }
}
