// eslint max-len: 'off'

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import * as THREE from 'three';
import * as TWEEN from 'tween';
import utilities from './utilities';
import materials from './materials';
import {
  createParticles,
  createText,
} from './geometry';
import { setTimeout } from 'timers';
import Chance from 'chance';

const random = new Chance();

const SECONDS = 1000;
const INVISIBLE_POS = { x: 999, y: 999, z: 999 };
export default class World extends Component {
  static propTypes = {
    onControlsEnabledChanged: PropTypes.func,
    position: PropTypes.number,
  }
  static defaultProps = {
    onControlsEnabledChanged: () => null,
  }

  constructor(props) {
    super(props);

    this.state = {
      position: 0,
      isControlsEnabled: false,
      areParticlesHidden: false,
      landParticleOpacity: 1,
      worldState: this.startWorldState,
    };
  }

  componentDidMount = async () => {
    window.addEventListener(`resize`, this.handleResize);

    this.createScene(this.renderElement);
    this.rerenderScene();

    this.keyLocations = this.getKeyLocations(this.config.keyLocations.count);
    this.world = new THREE.Group();
    this.satelliteGroup = new THREE.Group();
    this.satellites = await this.createSatelliteParticles();
    this.fixedSatellites = await this.createFixedSatelliteParticles();
    this.landParticles = await this.createLandParticles();
    this.minterParticles = await this.createMinterableParticles(this.keyLocations);
    this.minterParticles.particleSystem.material.opacity = 0;
    this.satelliteGroup.add(this.fixedSatellites.particleSystem);
    this.world.add(this.satelliteGroup);
    this.world.add(this.minterParticles.particleSystem);
    this.world.add(this.landParticles.particleSystem);
    this.world.position.x = this.config.world.startPos.x;
    this.world.position.y = this.config.world.startPos.y;
    this.world.position.z = this.config.world.startPos.z;
    this.scene.add(this.world);
    this.rerenderScene();

    this.satellites.neighbourMap = this.getClosestNeighboursMap(this.keyLocations, this.satellites.particles);
    this.landParticles.neighbourMap = this.getClosestNeighboursMap(this.keyLocations, this.landParticles.particles);
    this.minterParticles.neighbourMap = this.getClosestNeighboursMap(this.keyLocations, this.minterParticles.particles);
    // this.moveParticlesCloserToNeighbours(
    //   this.satellites,
    // );
    this.moveParticlesCloserToNeighbours(
      this.landParticles,
      TWEEN.Easing.Quadratic.In,
    );
    // this.animateParticlesToKeyLocation();

    this.floatParticles(this.fixedSatellites, this.config.particles.floatDistance, 12 * SECONDS);

    this.animateScene();
  }

  componentWillReceiveProps = (newProps) => {
    const wasPosition = this.props.position;
    const newPosition = newProps.position;
    if (wasPosition !== newPosition) {
      const story = this.story[newPosition];
      // TWEEN.removeAll();
      story();
    }
  }
  componentWillUnmount = () => {
    window.addEventListener(`resize`, this.handleResize);
  }
  handleResize = (e) => {
    const width = window.innerWidth;
    const height = window.innerHeight;
    this.camera.aspect = width / height;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize(width, height);
  }

  createSatelliteParticles = () => {
    return createParticles(
      this.config.particles.dynamicCount,
      materials.particles.experiment(),
      () => (utilities.random.between(this.config.particles.minRadius, this.config.particles.maxRadius, TWEEN.Easing.Cubic.In)),
    );
  }
  createFixedSatelliteParticles = () => {
    return createParticles(
      this.config.particles.fixedCount,
      materials.particles.experiment(),
      () => (
        utilities.random.between(this.config.particles.minRadius, this.config.particles.maxRadius, TWEEN.Easing.Cubic.In)
      ),
    );
  }
  createLandParticles = () => {
    return createParticles(
      this.config.particles.landCount,
      materials.particles.experiment(),
      () => this.config.world.size,
    );
  }
  createMinterableParticles = (keyLocations) => {
    const countPerLocation = 300;
    const totalCount = this.config.keyLocations.count * countPerLocation;
    let createdCount = 0;
    return createParticles(
      totalCount,
      materials.particles.experiment(),
      () => this.config.world.size,
      () => {
        const locationNumber = Math.floor(createdCount / countPerLocation);
        createdCount += 1;
        const position = keyLocations[locationNumber];
        return new THREE.Vector3(position.x, position.y, position.z);
      },
    );
  }

  createScene = (renderElement) => {
    const width = window.innerWidth;
    const height = window.innerHeight;
    // const width = renderElement.clientWidth;
    // const height = renderElement.clientHeight;
    this.renderer = new THREE.WebGLRenderer({ antialias: true });
    this.renderer.setSize(width, height);
    this.renderer.setPixelRatio(window.devicePixelRatio);
    renderElement.appendChild(this.renderer.domElement);
    this.camera = new THREE.PerspectiveCamera(45, width / height, 1, 1000);

    this.camera.position.z = 5;

    this.scene = new THREE.Scene();
    this.scene.background = new THREE.Color('black');
  }

  getKeyLocations = (locationCount) => {
    const keyLocations = [];
    for (let p = 0; p < locationCount; p += 1) {
      const position = utilities.random.pointOnSphere(this.config.world.size);
      keyLocations.push(position);
    }
    return keyLocations;
  }
  getClosestNeighbourId = (keyLocations, location) => {
    let minDistance = null;
    let closestLocation = null;
    for (const locationPos in keyLocations) {
      const locationToCheck = keyLocations[locationPos];
      const distance = location.distanceTo(locationToCheck);
      if (minDistance === null) {
        minDistance = distance;
        closestLocation = locationPos;
      } else if (distance < minDistance) {
        minDistance = distance;
        closestLocation = locationPos;
      }
    }
    return closestLocation;
  }
  getClosestNeighbour = (keyLocations, location) => {
    const closestNeighbourId = this.getClosestNeighbourId(keyLocations, location);
    return keyLocations[closestNeighbourId];
  }

  getClosestNeighboursMap = (keyLocations, particles) => {
    const neighbourMap = keyLocations.map(keyLocation => ({ keyLocation, neighbours: [] }));
    for (const particle of particles.vertices) {
      const closestLocationId = this.getClosestNeighbourId(keyLocations, particle);
      neighbourMap[closestLocationId].neighbours.push(particle);
    }
    return neighbourMap;
  }

  config = {
    world: {
      size: 2.5,
      rotation: 0.002,
      velocity: 0.05,
      deceleration: 0.988,
      startPos: { x: 3.5, y: 0, z: -1 },
    },
    particles: {
      size: 2,
      landCount: 4000,
      fixedCount: 2500,
      dynamicCount: 0,
      minRadius: 2.5,
      maxRadius: 3.5,
      floatDistance: 0.12,
    },
    keyLocations: {
      count: 20,
    },
  }
  tweens = {
    worldMovement: null,
    landFloatingTweens: [],
  }

  moveParticlesCloserToNeighbours = (particles, easing = TWEEN.Easing.Quadratic.In) => {
    const { neighbourMap, particleSystem } = particles;
    const destRadius = this.config.world.size;
    for (const { keyLocation, neighbours } of neighbourMap) {
      for (const particle of neighbours) {
        const originalRadius = utilities.calculate.getRadius(particle);
        const distanceToMove = easing(Math.random());
        const newRadius = utilities.calculate.positionAlongLine(originalRadius, destRadius, distanceToMove);
        const newPos = utilities.calculate.positionAlongArc(particle, keyLocation, newRadius, distanceToMove);
        particle.set(newPos.x, newPos.y, newPos.z);
      }
    }
    particleSystem.geometry.verticesNeedUpdate = true;
  }
  releaseCarbon = (particleGroup, animationDuration) => {
    const { particles, particleSystem } = particleGroup;
    for (const particle of particles.vertices) {
    // const particle = particles.vertices[0];
      const originalPos = new THREE.Vector3(particle.x, particle.y, particle.z);
      const originalRadius = this.config.world.size;
      const maxDistance = this.config.particles.maxRadius - this.config.particles.minRadius;
      const movement = TWEEN.Easing.Cubic.In(Math.random() * maxDistance);
      const destRadius = this.config.world.size + movement;
      const tween = new TWEEN.Tween()
        .to(1, animationDuration)
        .easing(TWEEN.Easing.Exponential.InOut)
        .onUpdate((progress) => {
          const newRadius = utilities.calculate.positionAlongLine(originalRadius, destRadius, progress);
          const newPos = utilities.calculate.putOnSphere(originalPos, newRadius);
          particle.set(newPos.x, newPos.y, newPos.z);
          particleSystem.geometry.verticesNeedUpdate = true;
        })
        .start();
    }
  }
  dropCarbon = (particleGroup, animationDuration) => {
    const { particles, particleSystem } = particleGroup;
    for (const particle of particles.vertices) {
    // const particle = particles.vertices[0];
      const originalPos = new THREE.Vector3(particle.x, particle.y, particle.z);
      const originalRadius = utilities.calculate.getRadius(originalPos);
      const destRadius = this.config.world.size;
      const tween = new TWEEN.Tween()
        .to(1, animationDuration)
        .easing(TWEEN.Easing.Bounce.Out)
        .onUpdate((progress) => {
          const newRadius = utilities.calculate.positionAlongLine(originalRadius, destRadius, progress);
          const newPos = utilities.calculate.putOnSphere(originalPos, newRadius);
          particle.set(newPos.x, newPos.y, newPos.z);
          particleSystem.geometry.verticesNeedUpdate = true;
        })
        .start();
    }
  }

  floatParticles = (particleGroup, movementSize, animationDuration, delay = 0) => {
    const { particles, particleSystem } = particleGroup;
    const tweens = [];
    for (const particle of particles.vertices) {
    // const particle = particles.vertices[0];
      const originalPos = new THREE.Vector3(particle.x, particle.y, particle.z);
      const originalRadius = utilities.calculate.getRadius(originalPos);
      const movement = Math.random() * movementSize;
      const randomDuration = animationDuration + (animationDuration * 0.2 * Math.random());
      const tween = new TWEEN.Tween()
        .to(1, randomDuration)
        .easing(TWEEN.Easing.Quadratic.Out)
        .onUpdate((progress) => {
          const overallProgress = 2 * Math.PI * progress;
          const adjustment = (Math.sin(overallProgress) * movement);
          const newRadius = adjustment + originalRadius;

          const newPos = utilities.calculate.putOnSphere(originalPos, newRadius);
          particle.set(newPos.x, newPos.y, newPos.z);
          particleSystem.geometry.verticesNeedUpdate = true;
        });
      tweens.push(tween);
      tween
        .chain(tween)
        .delay(delay)
        .start();
    }
    return tweens;
  }
  captureCarbon = (particles, animationDuration) => {
    const { neighbourMap, particleSystem } = particles;
    const easing = TWEEN.Easing.Quadratic.Out;
    for (const { keyLocation, neighbours } of neighbourMap) {
      for (const particle of neighbours) {
        const originalPos = new THREE.Vector3(particle.x, particle.y, particle.z);
        const originalRadius = utilities.calculate.getRadius(originalPos);
        const destPos = keyLocation;
        const destRadius = this.config.world.size;
        const onUpdate = (progress) => {
          const newRadius = utilities.calculate.positionAlongLine(originalRadius, destRadius, progress);
          const newPos = utilities.calculate.positionAlongArc(originalPos, destPos, newRadius, progress);
          particle.set(newPos.x, newPos.y, newPos.z);
          particleSystem.geometry.verticesNeedUpdate = true;
        };
        const onStop = () => {
          particle.set(INVISIBLE_POS.x, INVISIBLE_POS.y, INVISIBLE_POS.z);
          particleSystem.geometry.verticesNeedUpdate = true;
          setTimeout(() => {
            particle.set(originalPos.x, originalPos.y, originalPos.z);
            particleSystem.geometry.verticesNeedUpdate = true;
          }, animationDuration);
        };
        const tween = new TWEEN.Tween()
          .to(1, (0.5 + Math.random()) * animationDuration)
          .easing(easing)
          .onUpdate(onUpdate)
          .onComplete(() => {
            if (this.state.worldState.isCaptureEnabled) tween.start();
            else onStop();
          });
        new TWEEN.Tween()
          .to(1, (0.5 + Math.random()) * animationDuration)
          .easing(TWEEN.Easing.Quadratic.InOut)
          .onUpdate(onUpdate)
          .onComplete(() => {
            if (this.state.worldState.isCaptureEnabled) tween.start();
            else onStop();
          })
          .start();
      }
    }
  }
  minterCarbon = (particles, animationDuration) => {
    const { neighbourMap, particleSystem } = particles;
    const removeLoc = (locs, locNo) => {
      const clonedLocs = [].concat(locs);
      clonedLocs.splice(locNo, 1);
      return clonedLocs;
    };
    const getClosestLocations = (locationsToCheck, location, locationCount) => {
      const closestLocations = [];
      let remainingLocationsToCheck = [].concat(locationsToCheck);
      for (let i = 0; i < locationCount; i += 1) {
        const closestLocationId = this.getClosestNeighbourId(remainingLocationsToCheck, location);
        closestLocations.push(remainingLocationsToCheck[closestLocationId]);
        remainingLocationsToCheck = removeLoc(remainingLocationsToCheck, closestLocationId);
      }
      return closestLocations;
    };
    const keyLocations = neighbourMap.map(n => n.keyLocation);
    for (const currentLocationNo in neighbourMap) {
      const { keyLocation, neighbours } = neighbourMap[currentLocationNo];
      const neighboursExcludingCurrentLocation = removeLoc(keyLocations, currentLocationNo);
      const neighbourSubset = getClosestLocations(neighboursExcludingCurrentLocation, keyLocation, 6);
      const randomNeighbourSubset = random.pickset(neighbourSubset, 4);
      const randomDelayAdjustment = (Math.random() * 1 - 0.5);
      for (const particle of neighbours) {
        const originalPos = keyLocation;
        const worldRadius = this.config.world.size;
        const destNeighbour = random.pickone(randomNeighbourSubset);
        const destPos = destNeighbour;
        const randomLength = TWEEN.Easing.Sinusoidal.In(Math.random());
        const delay = animationDuration * Math.random();
        const tween = new TWEEN.Tween()
          .to(1, delay+animationDuration)
          .easing(TWEEN.Easing.Exponential.Out)
          .onUpdate((progress) => {
            const newPos = utilities.calculate.positionAlongArc(originalPos, destPos, worldRadius, progress);
            particle.set(newPos.x, newPos.y, newPos.z);
            particleSystem.geometry.verticesNeedUpdate = true;
          })
          .onComplete(() => {
            if (this.state.worldState.isMinterEnabled) tween.start();
          });
        setTimeout(() => {
          // Use setTimeout because I don't want to
          // permanently add a delay to the tween
          if (this.state.worldState.isMinterEnabled) tween.start();
        }, delay);
      }
    }
  }
  minterCarbonBetweenAllNeighbours = (particles, animationDuration) => {
    const { neighbourMap, particleSystem } = particles;
    const getRandomOtherLocation = (currentLoc, locationCount) => {
      const randomLocationNumber = Math.floor(Math.random() * (locationCount - 1));
      if (randomLocationNumber >= currentLoc) return randomLocationNumber + 1;
      else return randomLocationNumber;
    };
    for (const currentLocationNo in neighbourMap) {
      const { keyLocation, neighbours } = neighbourMap[currentLocationNo];
      for (const particle of neighbours) {
        const originalPos = keyLocation;
        const worldRadius = this.config.world.size;
        const destinationLocationNumber = getRandomOtherLocation(currentLocationNo, neighbourMap.length);
        const destPos = neighbourMap[destinationLocationNumber].keyLocation;
        const tween = new TWEEN.Tween()
          .to(1, animationDuration)
          .easing(TWEEN.Easing.Quadratic.Out)
          .onUpdate((progress) => {
            const newPos = utilities.calculate.positionAlongArc(originalPos, destPos, worldRadius, progress);
            particle.set(newPos.x, newPos.y, newPos.z);
            particleSystem.geometry.verticesNeedUpdate = true;
          });
        const delay = animationDuration * Math.random();
        tween
          .delay(delay)
          .chain(tween)
          .start();
      }
    }
  }
  showTransactionFees = async () => {
    for (const keyLocation of this.keyLocations) {
      const dollar = await createText(`$`);
      this.world.add(dollar);
      const originalRadius = this.config.world.size;
      const destRadius = originalRadius + 0.5;
      dollar.position.x = keyLocation.x;
      dollar.position.y = keyLocation.y;
      dollar.position.z = keyLocation.z;
      dollar.material.opacity = 0.3;
      new TWEEN.Tween()
        .to(1, 12 * SECONDS)
        .onUpdate((progress) => {
          const newOpacity = (progress * 0.7) + 0.3;
          const newRadius = utilities.calculate.positionAlongLine(originalRadius, destRadius, progress);
          dollar.material.opacity = newOpacity;
          const newPosition = utilities.calculate.putOnSphere(dollar.position, newRadius);
          dollar.position.x = newPosition.x;
          dollar.position.y = newPosition.y;
          dollar.position.z = newPosition.z;
        })
        .start();
    }
  }

  moveWorldToPosition = (toPosition, duration, easing = TWEEN.Easing.Quartic.InOut) => {
    if (this.tweens.worldMovement) {
      this.tweens.worldMovement.stop();
      this.tweens.worldMovement = null;
    }
    const wp = this.world.position;
    const wasPosition = new THREE.Vector3(wp.x, wp.y, wp.z);
    const tween = new TWEEN.Tween()
      .to(1, duration)
      .easing(easing)
      .onUpdate((progress) => {
        const newPoint = utilities.calculate.positionAlong3dLine(wasPosition, toPosition, progress);
        this.world.position.x = newPoint.x;
        this.world.position.y = newPoint.y;
        this.world.position.z = newPoint.z;
      })
      .start();
    this.tweens.worldMovement = tween;
    return tween;
  }
  makeParticlesVisible = (particles, duration, isVisible = true, delay = 0, easing = TWEEN.Easing.Quartic.Out) => {
    const currentVisibility = particles.particleSystem.material.opacity;
    const destinationVisibility = isVisible ? 1 : 0;
    return new TWEEN.Tween()
      .to(1, duration)
      .easing(easing)
      .delay(delay)
      .onUpdate((progress) => {
        const newOpacity = utilities.calculate.positionAlongLine(currentVisibility, destinationVisibility, progress);
        particles.particleSystem.material.opacity = newOpacity;
      })
      .start();
  }

  rerenderScene = () => {
    this.renderer.render(this.scene, this.camera);
  }
  animateScene = () => {
    requestAnimationFrame(this.animateScene);
    this.rerenderScene();
    this.world.rotation.y -= this.config.world.rotation;
    this.fixedSatellites.particleSystem.rotation.y += this.config.world.rotation * 0.5;
    TWEEN.update();
  }
  enableControls = (isControlsEnabled) => {
    if (this.state.isCaptureEnabled !== isControlsEnabled) {
      this.props.onControlsEnabledChanged(isControlsEnabled);
    }
    this.setState({
      isControlsEnabled,
    });
  }

  worldPosition = {
    largeRight: { x: 3.5, y: 0, z: -1 },
    right: { x: 2.5, y: 0, z: -6 },
    bottom: { x: 0, y: -3.5, z: -1 },
    left: { x: -2.5, y: 0, z: -6 },
    center: { x: 0, y: 0, z: -2 },
  }
  storyBoard = {
    overview: () => {
      this.setWorldState({
        worldPosition: `largeRight`,
        isReleased: false,
      });
    },
    startOverview: () => {
      this.setWorldState({
        worldPosition: `right`,
        isLandFloatEnabled: true,
      });
    },
    captureCarbon: () => {
      this.setWorldState({
        worldPosition: `right`,
        isCaptureEnabled: true,
      });
    },
    disableCapture: () => {
      this.setWorldState({
        worldPosition: `bottom`,
        isCaptured: true,
      });
    },
    centerWorld: () => {
      this.setWorldState({
        worldPosition: `bottom`,
        isCaptured: true,
      });
    },
    minterCarbon: () => {
      this.setWorldState({
        worldPosition: `left`,
        isMinterEnabled: true,
        isCaptured: true,
      });
    },
    minterCarbonForValues: () => {
      console.log('minter carbon for ')
      this.setWorldState({
        worldPosition: `left`,
        isMinterEnabled: true,
        isCaptured: true,
      });
    },
    centered: () => {
      console.log('centered')
      this.setWorldState({
        worldPosition: `center`,
        isCaptured: true,
      });
    },
  }

  defaultWorldState = {
    worldPosition: `largeRight`,
    isLandFloatEnabled: false,
    isCaptureEnabled: false,
    isMinterEnabled: false,
    isCaptured: false,
    isReleased: true,
  }
  startWorldState = {
    worldPosition: `largeRight`,
    isLandFloatEnabled: false,
    isCaptureEnabled: false,
    isMinterEnabled: false,
    isCaptured: false,
    isReleased: false,
  }
  setWorldState = (newChangedState) => {
    const newState = Object.assign({}, this.defaultWorldState, newChangedState);
    const oldState = this.state.worldState;
    if (newState.worldPosition !== oldState.worldPosition) {
      this.moveWorldToPosition(this.worldPosition[newState.worldPosition], 3 * SECONDS);
    }
    if (newState.isReleased !== oldState.isReleased) {
      if (newState.isReleased) {
        this.releaseCarbon(this.landParticles, 3 * SECONDS);
      } else {
        this.dropCarbon(this.landParticles, 1.5 * SECONDS); 
      }
    }
    if (false && newState.isLandFloatEnabled !== oldState.isLandFloatEnabled) {
      if (newState.isLandFloatEnabled) {
        this.tweens.landFloatingTweens = this.floatParticles(this.landParticles, 0.08, 12 * SECONDS, 3 * SECONDS);
      } else {
        this.tweens.landFloatingTweens.forEach(tween => tween.stop());
        this.tweens.landFloatingTweens = [];
      }
    }
    if (newState.isCaptureEnabled !== oldState.isCaptureEnabled) {
      const captureDuration = 3 * SECONDS;
      if (newState.isCaptureEnabled) {
        this.captureCarbon(this.landParticles, captureDuration);
      }
    }
    if (newState.isCaptured !== oldState.isCaptured) {
      const captureDuration = 3 * SECONDS;
      if (!newState.isCaptured) {
        this.tweens.landParticleVisibility = this.makeParticlesVisible(this.landParticles, 2 * SECONDS, true);
      } else {
        this.tweens.landParticleVisibility = this.makeParticlesVisible(this.landParticles, 1 * SECONDS, false, 2*SECONDS);
        //this.tweens.landParticleVisibility = this.makeParticlesVisible(this.landParticles, 0, false, captureDuration);
      }
    }
    if (newState.isMinterEnabled !== oldState.isMinterEnabled) {
      const minterDuration = 4 * SECONDS;
      if (newState.isMinterEnabled) {
        this.makeParticlesVisible(this.minterParticles, 0 * SECONDS, true);
        this.minterCarbon(this.minterParticles, minterDuration);
      } else {
        this.makeParticlesVisible(this.minterParticles, 2 * SECONDS, false, 1 * SECONDS);
      }
    }
    this.setState({
      worldState: newState,
    });
  }

  resetLandParticleVisibility = (opacity) => {
    if (this.state.landParticleOpacity !== opacity) {
      if (this.tweens.landParticleVisibility) {
        this.tweens.landParticleVisibility.stop();
      }
      this.setState({
        landParticleOpacity: opacity,
      });
      return true;
    }
    return false;
  }

  story = [
    this.storyBoard.overview,
    // this.storyBoard.test,
    this.storyBoard.startOverview,
    this.storyBoard.startOverview,
    this.storyBoard.captureCarbon,
    this.storyBoard.centerWorld,
    this.storyBoard.minterCarbonForValues,
    this.storyBoard.minterCarbonForValues,
    this.storyBoard.centered,
  ]

  render = () => {
    return (
      <div ref={(c) => { this.renderElement = c; }} style={{height: '100vh'}} />
    );
  }
}
