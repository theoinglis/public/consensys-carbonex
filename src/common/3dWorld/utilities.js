import * as THREE from 'three';

export const utilities = {
  random: {
    pointOnSphere: (radius) => {
      const x = Math.random() - 0.5;
      const y = Math.random() - 0.5;
      const z = Math.random() - 0.5;
      if (x === 0 && y === 0 && z === 0) {
        return utilities.calculate.getRandomPointOnSphere(radius);
      }

      return utilities.calculate.putOnSphere({ x, y, z }, radius);
    },
    between: (minValue, maxValue, easing = a => a) => {
      const range = maxValue - minValue;
      const randomAdd = Math.random() * range;
      const addValue = easing(randomAdd);
      return addValue + minValue;
    },
  },
  calculate: {
    getRadius: (point) => {
      return Math.sqrt((point.x ** 2) + (point.y ** 2) + (point.z ** 2));
    },
    putOnSphere: (point, radius) => {
      const normalisingValue = 1 / utilities.calculate.getRadius(point);
      return new THREE.Vector3(
        normalisingValue * radius * point.x,
        normalisingValue * radius * point.y,
        normalisingValue * radius * point.z,
      );
    },
    distanceBetweenPoints: (point1, point2) => {
      return Math.sqrt(
        ((point1.x - point2.x) ** 2) +
        ((point1.y - point2.y) ** 2) +
        ((point1.z - point2.z) ** 2),
      );
    },
    positionAlongArc: (pointFrom, pointTo, radius, percentage) => {
      return utilities.calculate.putOnSphere(
        utilities.calculate.positionAlong3dLine(pointFrom, pointTo, percentage),
        radius);
    },
    positionAlongLine: (pointFrom, pointTo, percentage) => {
      return ((pointTo - pointFrom) * percentage) + pointFrom;
    },
    positionAlong3dLine: (pointFrom, pointTo, percentage) => {
      return {
        x: utilities.calculate.positionAlongLine(pointFrom.x, pointTo.x, percentage),
        y: utilities.calculate.positionAlongLine(pointFrom.y, pointTo.y, percentage),
        z: utilities.calculate.positionAlongLine(pointFrom.z, pointTo.z, percentage),
      };
    },
  },
};

export default utilities;
