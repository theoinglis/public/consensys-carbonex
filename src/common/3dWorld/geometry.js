import * as THREE from 'three';
import utilities from './utilities';

const openSansRequired = require(`url-loader!./fonts/Open Sans_Bold.json`);
const generateSprite = () => {
  var canvas = document.createElement( 'canvas' );
  canvas.width = 16;
  canvas.height = 16;
  var context = canvas.getContext( '2d' );
  var gradient = context.createRadialGradient( canvas.width / 2, canvas.height / 2, 0, canvas.width / 2, canvas.height / 2, canvas.width / 2 );
  gradient.addColorStop( 0, 'rgba(255,255,255,1)' );
  gradient.addColorStop( 0.2, 'rgba(0,255,255,1)' );
  gradient.addColorStop( 0.4, 'rgba(0,0,64,1)' );
  gradient.addColorStop( 1, 'rgba(0,0,0,1)' );
  context.fillStyle = gradient;
  context.fillRect( 0, 0, canvas.width, canvas.height );
  return canvas;
}
export const createExperimentalParticles = (
  particleCount,
  materialPromise,
  getRadius,
  getPosition = utilities.random.pointOnSphere,
) => {
  return new Promise((resolve) => {
    const material = new THREE.SpriteMaterial({
      map: new THREE.CanvasTexture(generateSprite()),
      blending: THREE.AdditiveBlending,
    });
    materialPromise
      .then(() => {
        const group = new THREE.Group();
        for (let p = 0; p < particleCount; p++) {
          const radius = getRadius();
          const point = getPosition(radius);
          // const particle = new THREE.Vector3(point.x, point.y, point.z);
          const particle = new THREE.Sprite(material);
          particle.position.set(point.x, point.y, point.z);
          // add it to the geometry
          group.add(particle);
        }

        resolve(group);
      });
  });
};
export const createParticles = (
  particleCount,
  materialPromise,
  getRadius,
  getPosition = utilities.random.pointOnSphere,
) => {
  return new Promise((resolve) => {
    materialPromise
      .then((material) => {
        const particles = new THREE.Geometry();
        for (let p = 0; p < particleCount; p++) {
          const radius = getRadius();
          const point = getPosition(radius);
          const particle = new THREE.Vector3(point.x, point.y, point.z);

          // add it to the geometry
          particles.vertices.push(particle);
        }

        // create the particle system
        const particleSystem = new THREE.Points(
          particles,
          material);
        particleSystem.sortParticles = true;
        resolve({ particleSystem, particles });
      });
  });
};
export const createBlackHole = (size, widthSegment, heightSegment) => {
  return new Promise((resolve, reject) => {
    const blackHole = new THREE.Mesh(
      new THREE.SphereGeometry(size, widthSegment, heightSegment),
      new THREE.MeshBasicMaterial({ color: 0x000000 }),
    );
    resolve(blackHole);
  });
};

export const createText = (text) => {
  return new Promise((resolve, reject) => {
    const loader = new THREE.FontLoader();
    loader.load(openSansRequired, (font) => {
      const textGeometry = new THREE.TextGeometry(text, {
        font,
        size: 0.2,
        height: 0.02,
        curveSegments: 12,
      });
      resolve(new THREE.Mesh(
        textGeometry,
        new THREE.MeshPhongMaterial({
          shading: THREE.SmoothShading,
          blending: THREE.AdditiveBlending,
          transparent: true,
          size: 1,
          color: 0xffffff,
          ambient: 0xffffff,
          specular: 0x000000,
          shininess: 1,
          vertexColors: false,
        }),
      ));
    });
  });
};
