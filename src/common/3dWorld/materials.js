import * as THREE from 'three';
import particle from './particles/particle@4x.png';

export const materials = {
  particles: {
    lightBlue: new Promise((resolve) => {
      resolve(new THREE.MeshPhongMaterial({
        shading: THREE.SmoothShading,
        blending: THREE.AdditiveBlending,
        transparent: true,
        size: 1,
        color: 0xffffff,
        ambient: 0xffffff,
        specular: 0x000000,
        shininess: 1,
        vertexColors: false,
      }));
    }),
    experiment: (colour = 0x29BCF5) => new Promise((resolve) => {
      new THREE.TextureLoader().load(particle, (particleMap) => {
        resolve(new THREE.PointsMaterial({
          color: colour,
          map: particleMap,
          transparent: true,
          blending: THREE.AdditiveBlending,
          fog: false,
          size: 0.14,
          depthTest: false,
        }));
      });
    }),
    png: (particleColor = 0xFFFF00) => (new Promise((resolve) => {
      new THREE.TextureLoader().load(particle, (particleMap) => {
        resolve(new THREE.PointsMaterial({
          color: particleColor,
          size: 0.06,
          map: particleMap,
          transparent: true,
          blending: THREE.AdditiveBlending,
          fog: false,
          depthTest: false,
        }));
      });
    })),
    points: new Promise((resolve) => {
      resolve(new THREE.PointsMaterial({
        color: 0x0000FF,
        size: 0.4,
        transparent: true,
        blending: THREE.AdditiveBlending,
        fog: false,
        depthTest: false,
      }));
    }),
  },
};
export default materials;
