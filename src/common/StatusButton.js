import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';

import DefaultButtonComponent from 'common/Elements/buttons';
import DefaultLoadingComponent from 'common/Elements/loading';
import { Icon } from 'common/Elements/icons';
import { Tooltip } from 'common/Elements/popover';

import CrossIcon from 'static/icons/cancel.svg';
import TickIcon from 'static/icons/checked.svg';

const StatusButtonWrapper = styled.div`
  display: inline-block;
  position: relative;
  vertical-align: bottom;
`;
const ShowWrapper = styled.div`
  position: relative;
  z-index: 0;
  transition: transform 0.4s ${p => p.theme.easeOutQuart};
  display: inline-block;
  transform: scale(0);
  pointer-events: none;
  ${p => p.isShowing && css`
    transition: transform 0.4s ${p => p.theme.easeOutQuart} 0.1s;
    transform: scale(1);
    z-index: 1;
    pointer-events: auto;
  `}
`;
const ShowWrapperAsOverlay = styled(ShowWrapper) `
  position: absolute;
  left: 0;
  right: 0;
  text-align: center;
`;
const StatusIcon = styled.div`
  background: ${p => p.color};
  border-radius: 13px;
  height: 26px;
  width: 26px;
  display: inline-block;
  cursor: pointer;
  padding: 5px;
  box-sizing: border-box;
  box-shadow: ${p => p.theme.boxShadowSoft};
`;

const ErrorTooltip = ({ isError, errorMessage, ...rest }) => ((
  <Tooltip
    isDisabled={!isError}
    content={errorMessage}
    {...rest}
  />
));
const StyledLoadingComponent = styled(DefaultLoadingComponent) `
`;

export default class StatusButton extends Component {
  static propTypes = {
    ButtonComponent: PropTypes.func,
    LoadingComponent: PropTypes.func,
    ErrorComponent: PropTypes.func,
    className: PropTypes.string,
    status: PropTypes.shape({
      lastReceivedTime: PropTypes.object,
      state: PropTypes.string.isRequired,
      isFetching: PropTypes.bool,
      isError: PropTypes.bool,
      errorMessage: PropTypes.string,
      error: PropTypes.object,
    }).isRequired,
    children: PropTypes.node,
    position: PropTypes.oneOf([`left`, `right`]),
    minLoadingTime: PropTypes.number,
    showResultTime: PropTypes.number,
  }
  static defaultProps = {
    ButtonComponent: DefaultButtonComponent,
    LoadingComponent: DefaultLoadingComponent,
    ErrorComponent: ErrorTooltip,
    position: `left`,
    minLoadingTime: 2000,
    showResultTime: 4000,
  }

  constructor(props) {
    super(props);
    this.state = {
      showState: `ready`,
      isMinLoadingTimePassed: true,
    };
  }

  componentWillReceiveProps(nextProps) {
    const {
      minLoadingTime,
      showResultTime,
    } = this.props;
    const oldState = this.props.status.state;
    const newState = nextProps.status.state;
    const didStateChange = newState !== oldState;
    if (didStateChange) {
      switch (newState) {
        case `fetching`:
          this.setState({
            showState: `fetching`,
            isMinLoadingTimePassed: false,
          });
          setTimeout(() => {
            this.setState({
              showState: this.props.status.state,
              isMinLoadingTimePassed: true,
            });
          }, minLoadingTime);
          return;

        case `success`:
        case `failure`:
          this.setState({
            showState: newState,
          });
          setTimeout(() => {
            this.setState({
              showState: `ready`,
            });
          }, showResultTime);
          return;

        default:
          this.setState({
            showState: newState,
          });
      }
    }
  }
  setReady = () => {
    this.setState({
      showState: `ready`,
    });
  }

  getState = () => {
    return this.state.isMinLoadingTimePassed
      ? this.state.showState
      : `fetching`;
  }

  render = () => {
    const {
      ButtonComponent,
      LoadingComponent,
      ErrorComponent,
      className,
      children,
      status,
      position,
      minLoadingTime,
      ...rest
    } = this.props;
    const {
      isMinLoadingTimePassed,
    } = this.state;
    const state = this.getState();

    return (
      <StatusButtonWrapper className={className}>
        <ShowWrapper isShowing={state === `ready`}>
          <ButtonComponent {...rest}>
            { children }
          </ButtonComponent>
        </ShowWrapper>
        <ShowWrapperAsOverlay isShowing={state === `fetching`}>
          <LoadingComponent
            isLoading={!isMinLoadingTimePassed || status.isFetching}
            position={position}
          />
        </ShowWrapperAsOverlay>
        <ShowWrapperAsOverlay
          isShowing={state === `failure`}
          onClick={this.setReady}
        >
          <ErrorComponent
            className={className}
            isError={status.isError}
            errorMessage={status.errorMessage}
            error={status.error}
            position={position}
          >
            <StatusIcon color={`rgb(225,45,53)`}>
              <Icon icon={CrossIcon} size={`16px`} />
            </StatusIcon>
          </ErrorComponent>
        </ShowWrapperAsOverlay>
        <ShowWrapperAsOverlay
          isShowing={state === `success`}
          onClick={this.setReady}
        >
          <ErrorComponent
            className={className}
            isError={!isMinLoadingTimePassed || status.isError}
            errorMessage={status.errorMessage}
            error={status.error}
            position={position}
          >
            <StatusIcon color={`rgb(105,226,197)`}>
              <Icon icon={TickIcon} size={`16px`} />
            </StatusIcon>
          </ErrorComponent>
        </ShowWrapperAsOverlay>
      </StatusButtonWrapper>
    );
  }
}
