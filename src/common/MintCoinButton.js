import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Query, Mutation } from "react-apollo";
import gql from "graphql-tag";
import { Web3Consumer } from 'components/Web3/component';
import styled from 'styled-components';

import * as e from 'common/Elements';

const CREATE_CARBON_CREDIT = gql`
  mutation CreateCarbonCredit($carbonCredit: CarbonCreditInput!) {
    createCarbonCredit(carbonCredit: $carbonCredit) {
      id,
      address,
      variant,
      source,
    }
  }
`;

export default class MintCoinButton extends Component {
  static propTypes = {
    onNewCarbonCredit: PropTypes.func,
  }
  static defaultProps = {
    onNewCarbonCredit: () => null,
  }

  onWeb3Retrieved = async (web3) => {
    this.setState({
      account: await getPrimaryAccount(web3),
      minter: await getMinter(web3),
    });
    this.listenForUpdates();
    this.refreshCoinsList();
  }

  mintNewCoin = async (createCarbonCredit, refetch, ipfsHash, variant, source) => {
    const {
      onNewCarbonCredit,
    } = this.props;
    const {
      minter,
      account,
    } = this.state;
    const mint = await minter.mintCarbonCredit(ipfsHash, variant, source, { from: account });
    const {
      coinAddress,
    } = mint.logs[0].args;
    const newCarbonCredit = createCarbonCredit({
      variables: {
        carbonCredit: {
          address: coinAddress,
          variant,
          source,
        },
      },
    });
    onNewCarbonCredit(newCarbonCredit);
    return mint;
  }

  render = () => {
    return (
      <Web3Consumer onInitialise={this.onWeb3Retrieved}>
        <Mutation
          mutation={CREATE_CARBON_CREDIT}
          // onCompleted={() => { refetch(); }}
        >
        {(createCarbonCredit, { loading, error }) => {
          return (
            <e.Button.DefaultButton onClick={() => this.mintNewCoin(createCarbonCredit, refetch, `fdsskjnfdsk`, `VER`, `UK`)}>
              Mint a coin
            </e.Button.DefaultButton>
          );
        }}
        </Mutation>
      </Web3Consumer>
    );
  }
}
