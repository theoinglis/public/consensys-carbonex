import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';
import Swipe from 'react-hammerjs';

import Carousel from 'components/Carousel';
import ScrollItem from 'components/ScrollItem';
import * as e from 'common/Elements';

import arrowSvg from 'static/icons/arrow.svg';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  min-height: 100vh;
  min-height: calc(100vh - ${p => p.theme.heightHeader});
  justify-content: space-between;
`;
export const Menu = styled.div`
  position: relative;
  z-index: 1;
  text-align: center;
  padding: 60px 0 20px;
  flex: 0;
`;
export const CollapsedMenu = styled.div`
  margin: 0 auto;
  max-width: 360px;
  display: flex;
  flex-direction: row;
  ${p => p.theme.media.tablet`
    display: none;
  `}
`;
export const ExpandedMenu = styled.div`
  display: none;
  ${p => p.theme.media.tablet`
    display: block;
  `}
`;
export const MenuOption = styled(e.Button.BlankButton) `
  position: relative;
  color: ${p => p.theme.colorAccent};
  font-size: 18px;
  padding: 12px 0;
  margin: 0 20px;
  &:after {
    transition: ${p => p.theme.transitionDefault}; 
    content: ' ';
    position: absolute;
    bottom: 0;
    height: 1px;
    width: 0;
    background: currentColor;
    ${props => (
    props.isBack
      ? css`
        left: 0;
        right: initial;
      ` : css`
        left: initial;
        right: 0;
      `)}
    ${props => (
    props.isActive
      && css`
        transition: ${p => p.theme.transitionDefaultDelayed}; 
        width: 100%;
        ${p => (
      p.isBack
        ? css`
            left: initial;
            right: 0;
          ` : css`
            left: 0;
            right: initial;
          `)}
      `)}
  }
`;
export const ItemContainer = styled.div`
  position: relative;
  flex: 0;
`;
export const Item = styled.div`
  padding: 16px;
  margin: 0 auto;
`;
export const Breadcrumbs = styled.div`
  width: 100%;
  text-align: center;
  margin: 20px 0 60px;
  position: relative;
  z-index: 1;
`;
export const Breadcrumb = styled.div.attrs({
  size: p => p.size || 12,
})`
  transition: ${p => p.theme.transitionDefault};
  display: inline-block;
  margin: 0 ${p => p.size}px;
  border-radius: ${p => p.size / 2}px;
  width: ${p => p.size}px;
  height: ${p => p.size}px;
  background: ${p => p.theme.colorAccent};
  cursor: pointer;
  opacity: ${p => (p.isActive ? 1 : 0.4)};
`;
export const ProgressButton = e.Button.BlankButton.extend`
  flex: 0 0 40px;
  cursor: pointer;
`;
export const MenuSelect = e.Select.TextSelect.extend`
  flex: 1;
  display: inline-block;
  width: 100%;
  margin: 8px 0;
  cursor: pointer;
`;

export default class CarouselComponent extends Component {
  static propTypes = {
    data: PropTypes.array.isRequired,
    title: PropTypes.node,
  }

  constructor(props) {
    super(props);
    this.state = {
      wasPosition: 0,
      position: 0,
    };
  }


  onSelectChange = (selectedOption) => {
    this.setPosition(selectedOption.value);
  }
  setPosition = (toPosition) => {
    if (!this.canSetPosition(toPosition)) return;

    this.setState({
      wasPosition: this.state.position,
      position: toPosition,
    });
  }
  canSetPosition = (toPosition) => {
    const {
      data,
    } = this.props;
    if (toPosition < 0) return false;
    if (toPosition >= data.length) return false;

    return true;
  }

  onSwipe = (a) => {
    if (a.direction === 2) {
      this.setPosition(this.state.position + 1);
    } else if (a.direction === 4) {
      this.setPosition(this.state.position - 1);
    }
  }

  render = () => {
    const {
      data,
      title,
    } = this.props;
    const {
      position,
      wasPosition,
    } = this.state;
    const isFirstEntry = position < 1;
    const isLastEntry = position >= data.length - 1;

    return (
      <Swipe onSwipe={this.onSwipe}>
        <div>
          <Container>
            {title || (
              <Menu>
                <CollapsedMenu>
                  <ProgressButton
                    onClick={() => this.setPosition(position - 1)}
                    disabled={isFirstEntry}
                    style={{ transform: `rotate(180deg)` }}
                  >
                    <e.Icon.default size={`32px`} icon={arrowSvg} />
                  </ProgressButton>
                  <MenuSelect
                    name={`title`}
                    value={position}
                    searchable={false}
                    clearable={false}
                    arrowRenderer={null}
                    onChange={this.onSelectChange}
                    options={
                      data.map(dataItem => ({
                        value: dataItem.id,
                        label: dataItem.title,
                      }))
                    }
                  />
                  <ProgressButton
                    onClick={() => this.setPosition(position + 1)}
                    disabled={isLastEntry}
                  >
                    <e.Icon.default size={`32px`} icon={arrowSvg} />
                  </ProgressButton>
                </CollapsedMenu>
                <ExpandedMenu>
                  {
                    data
                      .map((dataItem, index) => (
                        <ScrollItem key={dataItem.id} isInline scrollDelay={index * 0.1}>
                          <MenuOption
                            onClick={() => this.setPosition(index)}
                            isActive={position === index}
                            isBack={wasPosition > position}
                          >
                            {dataItem.title}
                          </MenuOption>
                        </ScrollItem>
                      ))
                  }
                </ExpandedMenu>
              </Menu>
            )}
            <ItemContainer>
              <Carousel
                position={position}
                onChange={this.setPosition}
                ControlsComponent={() => null}
                items={data.map(dataItem => () => (
                  <Item key={dataItem.id}>
                    {dataItem.item}
                  </Item>
                ))}
              />
            </ItemContainer>
            <Breadcrumbs>
              {
                data.map((dataItem, index) => (
                  <Breadcrumb
                    key={dataItem.id}
                    onClick={() => this.setPosition(index)}
                    isActive={position === index}
                  />
                ))
              }
            </Breadcrumbs>
          </Container>
        </div>
      </Swipe>
    );
  }
}
