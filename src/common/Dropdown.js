import React, { Component } from 'react';
import styled, { css } from 'styled-components';
import ClickOutsideWrapper from 'react-click-outside';

const Container = styled(ClickOutsideWrapper)`
  position: relative;
`;
const Button = styled.div`
  cursor: pointer;
`;
const DropdownComponent = styled.div`
  position: absolute;
  top: 100%;
  left: 50%;
  transform: translateX(-50%);
  transition: ${p => p.theme.transitionDefault};
  pointer-events: none;
  opacity: 0;
  margin-top: 20px;
  ${props => props.isOpen && css`
    pointer-events: auto;
    opacity: 1;
    margin-top: 0;
  `}
`;
export default class Dropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
    };
  }

  render = () => {
    const {
      children,
      dropdown,
    } = this.props;
    const {
      isOpen,
    } = this.state;

    return (
      <Container onClickOutside={() => this.setState({ isOpen: false })}>
        <Button onClick={() => this.setState({ isOpen: !isOpen })}>
          {children}
        </Button>
        <DropdownComponent isOpen={isOpen} onClick={this.onDropdownClick}>
          {dropdown}
        </DropdownComponent>
      </Container>
    );
  }
}
