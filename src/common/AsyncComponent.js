import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import * as e from 'common/Elements';

const Container = styled.div`
  display: inline-block;
`;

export const asyncWrapper = (config) => (WrappedComponent) => {
  const wrapper = (props) => {
    const isLoading = config.check.reduce((acc, val) => {
      if (acc) return true;
      else if (props[val].loading) return true;
      return false;
    }, false);
    const errors = config.check.map((val) => props[val].error).filter((err) => !!err);
    return (
      <AsyncComponent isLoading={isLoading} isError={errors.length > 0} error={errors}>
        {() => (
          <WrappedComponent {...props} />
        )}
      </AsyncComponent>
    );
  };

  return wrapper;
};

export const AsyncFunction = (children) => (data) => (
  <AsyncComponent isLoading={data.loading} isError={!!data.error} error={data.error} data={data}>
    {(d) => children(d)}
  </AsyncComponent>
);

export default class AsyncComponent extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
    isLoading: PropTypes.bool.isRequired,
    isError: PropTypes.bool,

    error: PropTypes.object,
    data: PropTypes.object,
    loadingMessage: PropTypes.string,
    errorMessage: PropTypes.string,
    renderLoading: PropTypes.func,
    renderError: PropTypes.func,

    alwaysRenderChild: PropTypes.bool,
  }

  static defaultProps = {
    alwaysRenderChild: false,
    loadingMessage: `This compononent is loading ...`,
    errorMessage: `There was an error with this component`,
  }

  renderLoading = () => {
    const {
      renderLoading,
      loadingMessage,
    } = this.props;
    if (renderLoading) return renderLoading(loadingMessage);
    return (
      <e.Loading.CenterLoader isLoading>
        { loadingMessage }
      </e.Loading.CenterLoader>
    );
  }

  renderError = () => {
    const {
      renderError,
      error,
      errorMessage,
    } = this.props;
    debugger;
    if (renderError) return renderError(errorMessage, error);
    return (
      <div>
        {errorMessage}
      </div>
    );
  }

  render = () => {
    const {
      children,
      isLoading,
      isError,
      data,
      alwaysRenderChild,
    } = this.props;
    if (isLoading) return this.renderLoading();
    if (isError) return this.renderError();
    return children(data);
  }
}
