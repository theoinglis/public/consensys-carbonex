import React from 'react';
import PropTypes from 'prop-types';
import prismicCtxPromise from 'services/prismic';

export default class Component extends React.Component {
  static propTypes = {
    component: PropTypes.func,
  };

  constructor(props) {
    super(props);

    prismicCtxPromise.then(prismicCtx => this.setState({
      prismicCtx,
      isLoading: false,
    }))
      .catch((error) => {
        this.setState({
          isLoading: false,
          error,
        });
      });

    this.state = {
      isLoading: true,
      prismicCtx: null,
    };
  }

  render() {
    const { isLoading, error, prismicCtx } = this.state;
    const PrismicComponent = this.props.component;

    return error
      ? <div>Error: {error}</div>
      : isLoading
        ? <div>Loading...</div>
        : <PrismicComponent prismicCtx={prismicCtx} {...this.props} />;
  }
}

export const connectToPrismic = component => props => (
  <Component component={component} {...props} />
);
