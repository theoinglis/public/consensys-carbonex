
const defaults = {
  isLoggedIn: false,
  account: {
    __typename: `Account`,
    isAdmin: false,
    isLoggedIn: false,
    selectedAccount: null,
    accounts: [],
    balance: null,
  },
  errors: {
    __typename: `Errors`,
    latest: null,
    all: [],
  },
  categories: {
    __typename: `Categories`,
    other: 'thing',
    test: {
      __typename: 'Value',
      value: 'thing'
    },
    variants: [
      { value: `REC`, label: `REC`, __typename: `SelectOption` },
      { value: `VER`, label: `VER`, __typename: `SelectOption` },
      { value: `CER`, label: `CER`, __typename: `SelectOption` },
      { value: `EUA`, label: `EUA`, __typename: `SelectOption` },
      { value: `REDD`, label: `REDD+`, __typename: `SelectOption` },
    ],
    regions: [
      { value: `AFR`, label: `AFR`, __typename: `SelectOption` },
      { value: `AUS`, label: `AUS`, __typename: `SelectOption` },
      { value: `EU`, label: `EU`, __typename: `SelectOption` },
      { value: `LATAM`, label: `LATAM`, __typename: `SelectOption` },
      { value: `NORAM`, label: `NORAM`, __typename: `SelectOption` },
      { value: `N.ASIA`, label: `N.ASIA`, __typename: `SelectOption` },
      { value: `S.ASIA`, label: `S.ASIA`, __typename: `SelectOption` },
    ],
    methods: [
      { value: `AGRI`, label: `Agriculture`, __typename: `SelectOption` },
      { value: `BIO`, label: `Bio`, __typename: `SelectOption` },
      { value: `EEGEN`, label: `EEGen`, __typename: `SelectOption` },
      { value: `EEIND`, label: `EEInd`, __typename: `SelectOption` },
      { value: `FOREST`, label: `Forest`, __typename: `SelectOption` },
      { value: `FUELSWITCH`, label: `FuelSwitch`, __typename: `SelectOption` },
      { value: `HYDRO`, label: `Hydro`, __typename: `SelectOption` },
      { value: `GEOTHERMAL`, label: `Geo Thermal`, __typename: `SelectOption` },
      { value: `LANDGAS`, label: `Land Gas`, __typename: `SelectOption` },
      { value: `N2O`, label: `N2O`, __typename: `SelectOption` },
      { value: `SOLAR`, label: `Solar`, __typename: `SelectOption` },
      { value: `WIND`, label: `Wind`, __typename: `SelectOption` },
    ]
  }
};
export default defaults;
