import gql from "graphql-tag";

export const detail = gql`
  fragment DetailedCarbonCredit on CarbonCreditType {
    id,
    address,
    volume
    variant,
    region,
    method,
    state
  }
`;
