import { logErrorResolver } from 'services/error';

const resolvers = {
  Mutation: {
    logError: logErrorResolver,
    setAccountDetails: (_, { account }, { cache }) => {
      cache.writeData({
        data: {
          account: {
            ...account,
            __typename: `Account`,
          },
        },
      });
      return null;
    },
  },
};
export default resolvers;
