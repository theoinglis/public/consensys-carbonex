/* global __PUBLIC_DATA__ */
const configFromServer = typeof __PUBLIC_DATA__ === `undefined` ? `` : __PUBLIC_DATA__.config;

const config = Object.assign(configFromServer, {
  // Static config
  api: {},
});

export default config;
