import ApolloClient from "apollo-boost";
import defaults from "gql/defaults";
import resolvers from "gql/resolvers";

export const client = new ApolloClient({
  uri: `http://localhost:5000/graphql`,
  clientState: {
    defaults,
    resolvers,
  },
});
export default client;
