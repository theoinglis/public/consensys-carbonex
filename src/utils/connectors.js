import { Connect, SimpleSigner } from 'uport-connect';

export const uport = new Connect(`Carbonex`, {
  clientId: `2opQTFHXoSdjJECGFthZrLJ5Soj4brEbXxa`,
  network: `rinkeby`,
  signer: SimpleSigner(`28cdf0d56ada22ff6b9f1a42b30f2cd02a0f35785032879e49f68088841421ea`),
});
