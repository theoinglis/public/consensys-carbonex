
import contract from 'truffle-contract';

export const getContract = (web3, Contract, address) => {
  return new Promise((resolve, reject) => {
    const contractInstance = contract(Contract);
    contractInstance.setProvider(web3.currentProvider);
    const contractPromise = address ? contractInstance.at(address) : contractInstance.deployed();
    contractPromise
      .then(resolve)
      .catch(reject);
  });
};

export const getAccounts = (web3) => {
  return new Promise((resolve, reject) => {
    web3.eth.getAccounts((error, accounts) => {
      if (error) return reject(error);
      else {
        resolve(accounts);
      }
    });
  });
};

export const getBalance = (web3, account) => {
  return new Promise((resolve, reject) => {
    web3.eth.getBalance(account, (error, balance) => {
      if (error) return reject(error);
      else {
        resolve(balance);
      }
    });
  });
};
export const getConvertedBalance = (web3, account, unit = `ether`) => {
  return getBalance(web3, account)
    .then((balance) => web3.toDecimal(web3.fromWei(balance, unit)));
};

export const getPrimaryAccount = async (web3) => {
  const accounts = await getAccounts(web3);
  if (accounts && accounts.length > 0) return accounts[0];
  else return null;
};
