
import React from 'react';
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';

let logErrorMutation;
const LOG_ERROR = gql`
  mutation LogError($errorMessage: String!) {
    logError(errorMessage: $errorMessage) @client
  }
`
export const ErrorProvider = (props) => {
  return (
    <Mutation mutation={LOG_ERROR}>
      {(_logErrorMutation) => {
        logErrorMutation = _logErrorMutation;
        return props.children;
      }}
    </Mutation>
  )
}

export const logError = (errorMessage) => {
  logErrorMutation({
    variables: {
      errorMessage,
    },
  });
};
export const logErrorResolver = (_, variables, { cache }) => {
  const query = gql`
    query GetErrors {
      errors @client {
        all
        latest
      }
    }
  `;
  const previous = cache.readQuery({ query });
  const newError = { errorMessage: variables.errorMessage, __typename: 'Error' };

  cache.writeQuery({
    query,
    data: {
      errors: {
        latest: newError,
        all: previous.errors.all.concat([newError]),
        __typename: `Errors`,
      },
    },
  });
  return null;
};

export default logError;
