
import contract from 'truffle-contract';
import { getContract } from 'utils/web3';
import CxBrokerContract from 'contracts/CxBroker.json';

export default class CbnSales {
  constructor(_web3) {
    this.web3 = _web3;
    this.getContract();
  }

  getContract = () => {
    if (this.contractPromise) return this.contractPromise;
    else {
      this.contractPromise = new Promise((resolve, reject) => {
        const contractInstance = contract(CxBrokerContract);
        contractInstance.setProvider(this.web3.currentProvider);
        contractInstance.deployed()
          .then(resolve)
          .catch(reject);
      });
      return this.contractPromise;
    }
  }

  getSalesAddresses = async () => {
    const broker = await this.getContract();
    return broker.getSales();
  }
}
