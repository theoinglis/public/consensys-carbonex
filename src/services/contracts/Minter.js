
import { getContract } from 'utils/web3';
import CxMinterContract from 'contracts/CxMinter.json';

export default class CbnSales {
  constructor(_web3) {
    this.web3 = _web3;
    this.getContract();
  }

  getContract = () => {
    if (this.contractPromise) return this.contractPromise;
    else {
      this.contractPromise = getContract(this.web3, CxMinterContract);
      return this.contractPromise;
    }
  }
  getCoinAddresses = async () => {
    const minter = await this.getContract();
    return minter.getCoins();
  }
  getMyCoinAddresses = async () => {
    const minter = await this.getContract();
    return minter.getMyCoins();
  }
}
