
import contract from 'truffle-contract';

import CategoriesContract from 'contracts/Categories.json';

export default class Categories {
  constructor(_web3) {
    this.web3 = _web3;
    this.contractPromise = this.getContract();
  }

  getContract = () => {
    return new Promise((resolve, reject) => {
      const categories = contract(CategoriesContract);
      categories.setProvider(this.web3.currentProvider);
      categories.deployed()
        .then(resolve)
        .catch(reject);
    });
  }

  getOptions = async (categoryName) => {
    const categories = await this.contractPromise;

    const regionList = await categories.getCategoryOptions(categoryName);
    return regionList.map((l) => this.web3.toUtf8(l));
  }
}
