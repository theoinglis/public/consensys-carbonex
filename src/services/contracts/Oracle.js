
import { getContract } from 'utils/web3';
import OracleProcessor from 'contracts/OracleProcessor.json';

export default class CbnSales {
  constructor(_web3) {
    this.web3 = _web3;
    this.getContract();
  }

  getContract = () => {
    if (this.contractPromise) return this.contractPromise;
    else {
      this.contractPromise = getContract(this.web3, OracleProcessor);
      return this.contractPromise;
    }
  }

  listen = async () => {
    // Listen in browser ... not the most robust solution as it will only be able to respond
    // if someone is on the website and using the users processing,
    // but for demo purposes it works just fine
    console.log('listening')
    const oracle = await this.getContract();
    console.log('waiting ..')
    oracle.GetData()
      .watch((err, info) => {
        const {
          sender,
          originator,
          data,
        } = info.args;
        oracle.SetData(sender, originator, 'some retrieved answer');
      });
  }
}
