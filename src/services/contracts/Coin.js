
import contract from 'truffle-contract';
import { getContract } from 'utils/web3';
import CbnCoinContract from 'contracts/CbnCoin.json';

export default class Coin {
  constructor(_web3) {
    this.web3 = _web3;
  }

  getState = (stateBigNo) => {
    const stateNo = this.web3.toDecimal(stateBigNo);
    switch (stateNo) {
      case 0: return `Requested`;
      case 1: return `Minted`;
      case 2: return `Expired`;
      default: return `UNKNOWN`;
    }
  }

  getIpfsData = (coin) => {
    return new Promise((resolve, reject) => {
      coin.RetrievedIpfsData()
        .watch((err, result) => {
          console.log(`got ipfs data`, err, result);
          if (err) reject(err);
          resolve(result);
        })
      coin.getIpfsData()
        .then((err, data) => {
          console.log('got data back', data);
        });
    })
  }

  getCoinData = async (coinAddress, doUpdate) => {
    doUpdate({
      loading: true,
      error: false,
      data: {
        address: coinAddress,
      },
    });
    try {
      const coin = await getContract(this.web3, CbnCoinContract, coinAddress);
      const [
        hasOwnership,
        volume,
        variant,
        region,
        method,
        ownershipAmount,
        state,
      ] = await coin.getData();
      doUpdate({
        loading: false,
        error: false,
        data: {
          address: coinAddress,
          hasOwnership,
          volume: this.web3.toDecimal(volume),
          variant,
          region,
          method,
          ownershipAmount: this.web3.toDecimal(ownershipAmount),
          state: this.getState(state),
          coin,
        },
      });
    } catch (e) {
      doUpdate({
        loading: false,
        error: e,
        data: {
          address: coinAddress,
        },
      });
    }
  }

  buyFromDeed = async (
    deedAddress,
    numberOfCarbonCredits,
    buyerAddress,
  ) => {
    const deed = await getContract(this.web3, CbnCoinContract, deedAddress);
    const priceInWei = await deed.pricePerCoin();
    deed.buy(numberOfCarbonCredits, {
      from: buyerAddress,
      value: priceInWei.mul(numberOfCarbonCredits),
    });
  }
}
