
import contract from 'truffle-contract';
import { getContract } from 'utils/web3';
import CbnSaleContract from 'contracts/CbnSale.json';

export default class CbnSales {
  constructor(_web3) {
    this.web3 = _web3;
  }

  getState = (stateBigNo) => {
    const stateNo = this.web3.toDecimal(stateBigNo);
    switch (stateNo) {
      case 0: return `Created`;
      case 1: return `Available`;
      case 2: return `Complete`;
      case 3: return `Cancelled`;
      default: return `UNKNOWN`;
    }
  }

  getContract = (saleAddress) => {
    return getContract(this.web3, CbnSaleContract, saleAddress);
  }

  withdraw = async (saleAddress, from) => {
    const sale = await this.getContract(saleAddress);
    return sale.withdrawPayments({
      from,
    });
  }

  getSaleData = async (saleAddress, doUpdate) => {
    doUpdate({
      loading: true,
      error: false,
      data: {
        address: saleAddress,
      },
    });
    try {
      const sale = await this.getContract(saleAddress);
      const [
        isSeller,
        coinAddress,
        sellerAddress,
        pricePerCoinInWei,
        remainingAmount,
        totalAmount,
        balance,
        state,
      ] = await sale.getData();
      doUpdate({
        loading: false,
        error: false,
        data: {
          address: saleAddress,
          isSeller,
          sellerAddress,
          coinAddress,
          pricePerCoin: this.web3.toDecimal(this.web3.fromWei(pricePerCoinInWei, `ether`)),
          remainingAmount: this.web3.toDecimal(remainingAmount),
          totalAmount: this.web3.toDecimal(totalAmount),
          balance: this.web3.toDecimal(this.web3.fromWei(balance, `ether`)),
          state: this.getState(state),
        },
      });
    } catch (e) {
      doUpdate({
        loading: false,
        error: e,
        data: {
          address: saleAddress,
        },
      });
    }
  }

  buyFromDeed = async (
    deedAddress,
    numberOfCarbonCredits,
    buyerAddress,
  ) => {
    const deed = await this.getContract(deedAddress);
    const priceInWei = await deed.pricePerCoin();
    deed.buy(numberOfCarbonCredits, {
      from: buyerAddress,
      value: priceInWei.mul(numberOfCarbonCredits),
    });
  }
}
