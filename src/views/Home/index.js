import React, { Component } from 'react';

import * as e from 'common/Elements';
import CarbonCreditsTable from 'views/common/Tables/CarbonCredits';


export default class Home extends Component {

  renderNoCarbonCreditsMinted = () => {
    return (
      <div>
        <p>There were no carbon credits minted</p>
      </div>
    );
  }

  render = () => {
    return (
      <e.Container.CenteredPageContainer>
         <CarbonCreditsTable
           renderNone={this.renderNoCarbonCreditsMinted}
          filterRow={(data) => {
            return data.state !== `Minted`
          }
          }
           getAddresses={(minter) => minter.getCoinAddresses()}
         />
      </e.Container.CenteredPageContainer>
    );
  }
}
