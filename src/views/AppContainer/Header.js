import React, { Component } from 'react';
import { Query, Mutation } from "react-apollo";
import gql from "graphql-tag";
import styled from 'styled-components';

// UI Components
import * as e from 'common/Elements';
import * as c from './components';

const GET_ACCOUNT_DETAILS = gql`
  {
    account @client {
      isLoggedIn
      isAdmin
      selectedAccount
      accounts
      balance
    }
  }
`;
export default class ComponentName extends Component {
  renderLoggedInNav = (data) => {
    return (
      <span>
        <e.Button.DefaultButton style={{ opacity: 0, pointerEvents: `none` }}>
          Logout
        </e.Button.DefaultButton>
        <e.Button.TextLink to={`/`}>
          List
        </e.Button.TextLink>
        {data.account.isAdmin ? (
          <e.Button.TextLink to={`/admin`}>
            Admin
          </e.Button.TextLink>
        ) : (
          <span>
            <e.Button.TextLink to={`/purchase`}>
              Buy
            </e.Button.TextLink>
            <e.Button.TextLink to={`/mint`}>
              Mint
            </e.Button.TextLink>
            <e.Button.TextLink to={`/account`}>
              Account
            </e.Button.TextLink>
          </span>
        )}
        <span>Balance: {data.account.balance} ETH</span>
      </span>
    );
  }

  renderLoggedOutNav = (data) => {
    return (
      <span>
      <e.Button.DefaultButton>
        Login
      </e.Button.DefaultButton>
      </span>
    );
  }

  render = () => {
    return (
      <c.HeaderContainer>
        <c.LogoContainer>
          <e.Button.BlankLink to={`/`}>
            <e.Logo.FullLogo />
          </e.Button.BlankLink>
        </c.LogoContainer>
        <c.NavItems>
          <Query query={GET_ACCOUNT_DETAILS}>
            {({ loading, error, data }) => {
              console.log(data);
              if (data.account.isLoggedIn) return this.renderLoggedInNav(data);
              return this.renderLoggedOutNav(data);
            }}
          </Query>
        </c.NavItems>
      </c.HeaderContainer>
    );
  }
}
