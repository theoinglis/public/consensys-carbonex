import React, { Component } from 'react';
import { Query } from "react-apollo";
import gql from "graphql-tag";
import styled from 'styled-components';

import AsyncComponent from 'common/AsyncComponent';
import * as c from './components';
import Header from './Header';

export default class AppContainer extends Component {
  render = () => {
    const {
      children,
    } = this.props;
    
    return (
      <c.AppContainer>
        <Header />
        <c.ContentContainer>
          {children}
        </c.ContentContainer>
      </c.AppContainer>
    );
  }
}
