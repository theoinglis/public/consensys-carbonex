import styled from 'styled-components';

export const AppContainer = styled.div`
  background: ${(p) => p.theme.colorPrimary};
  display: flex;
  flex-direction: column;
  height: 100vh;
  width: 100vw;
`;
export const HeaderContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding: 8px;
  background: black;
  color: white;
`;
export const ContentContainer = styled.div`
  background: ${(p) => p.theme.colorPrimary};
  color: white;
  flex: 1;
  padding: 8px;
`;

export const NavItems = styled.div`
`;
export const LogoContainer = styled.div`
  padding-top: 15px;
`;
