import React, { Component } from 'react';
import {
  compose, graphql, withApollo,
} from "react-apollo";
import gql from "graphql-tag";
import injectWeb3 from 'components/Web3/hoc';
import { asyncWrapper } from 'common/AsyncComponent';

import * as e from 'common/Elements';
import TransactionModal from 'views/common/TransactionModal';

import CxMinterService from 'services/contracts/Minter';
import CarbonCreditsTable from 'views/common/Tables/CarbonCredits';

export class AdminView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      transactionStatus: null,
      transactionMessage: null,
    };
  }

  doApprove = async (account, coin) => {
    const {
      web3,
    } = this.props;
    const {
      minterService,
    } = web3.data.services;
    try {
      this.setState({transactionMessage: `Please authorise the minting of the coin`})
      const minter = await minterService.getContract();
      await minter.mintCarbonCredit(coin.address, { from: account });
      this.setState({ transactionMessage: `The coin should be minted shortly`, transactionStatus: `SUCCESS` });
    } catch (e) {
      this.setState({transactionMessage: e.message, transactionStatus: `ERROR`});
    }
  }

  renderNoneToMint = () => {
    return (
      <p>
        There are no new carbon credits to mint.
      </p>
    );
  }

  render = () => {
    const {
      transactionStatus,
      transactionMessage,
    } = this.state;
    const {
      account,
    } = this.props;

    return (
      <e.Container.CenteredPageContainer>
        <h1>Carbon Credit Requests</h1>
        <CarbonCreditsTable
          renderNone={this.renderNoneToMint}
          filterRow={(data) => {
            return data.state !== `Requested`;
          }}
          actionComponent={
            (data) => (
              <e.Button.default
                onClick={() => this.doApprove(account.account.selectedAccount, data)}
              >
                Approve
              </e.Button.default>
            )
          }
        />
        <TransactionModal status={transactionStatus} message={transactionMessage} onClose={() => this.setState({ transactionStatus: null, transactionMessage: null })} />
      </e.Container.CenteredPageContainer>
    );
  }
}
export default compose(
  withApollo,
  graphql(gql`
  {
    account @client {
      selectedAccount
    }
  }
`, { name: `account` }),
  injectWeb3({
    onInitialise: async (web3) => {
      const minterService = new CxMinterService(web3);
      await minterService.getContract();
      return {
        services: {
          minterService,
        },
      };
    },
  }),
  asyncWrapper({
    check: [`web3`, `account`],
  }),
)(AdminView);
