import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Query } from "react-apollo";
import gql from "graphql-tag";
import Web3Provider from 'components/Web3/component';
import { getContract } from 'utils/web3';

import * as e from 'common/Elements';
import * as Form from 'components/Form';
import { AsyncFunction } from 'common/AsyncComponent';
import TransactionModal from 'views/common/TransactionModal';

import Categories from 'services/contracts/Categories';
import CxMinterContract from 'contracts/CxMinter.json';


const QUERY = gql`
  {
    account @client {
      selectedAccount
    }
  }
`;

export default class MintCoin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isValid: false,
      methods: [],
      transactionStatus: null,
      transactionMessage: null,
    };
  }

  onWeb3Retrieved = async (web3) => {
    const categories = new Categories(web3);
    this.setState({
      minter: await getContract(web3, CxMinterContract),
      variants: (await categories.getOptions(`variant`)).map((o) => ({ value: o, label: o })),
      regions: (await categories.getOptions(`region`)).map((o) => ({ value: o, label: o })),
      methods: (await categories.getOptions(`method`)).map((o) => ({ value: o, label: o })),
    });
    return true;
  }

  onSubmit = (account) => {
    const {
      isValid,
    } = this.state;
    this.form.submit();
    if (isValid) {
      const model = this.form.getModel();
      this.mintNewCoin(account, model);
    }
  }

  mintNewCoin = async (account, data) => {
    const {
      minter,
    } = this.state;
    try {
      this.setState({ transactionMessage: `Please authorise the transaction to request your carbon credits to be minted` });
      const mint = await minter.requestCarbonCreditMinting(data.volume, data.variant, data.region, data.method, data.ipfsHash, { from: account });
      this.setState({
        transactionStatus: `SUCCESS`,
        transactionMessage: `Great. Once the transaction has been successfully processed and we have received your certificate someone from our team will look to authorise the request`,
      });
      const {
        coinAddress,
      } = mint.logs[0].args;
      this.form.reset();
    } catch (e) {
      this.setState({transactionMessage: e.message, transactionStatus: `ERROR`});
    }
    return mint;
  }

  render = () => {
    const {
      isValid,
      transactionStatus,
      transactionMessage,
    } = this.state;

    return (
      <e.Container.CenteredPageContainer>
        <h1>Mint your Carbon Credits</h1>
        <p>
          In order to sell your carbon credits we need to mint them on our platform.
        </p>
        <p>
          Enter the information about them and send in your certificate of ownership.
          We can then confirm their legitimacy and make them available to sell.
        </p>
        <Query
          query={QUERY}
        >
          {AsyncFunction(({ data: { account: { selectedAccount } } }) => (
            <Web3Provider onInitialise={this.onWeb3Retrieved}>
              {AsyncFunction(() => (
                <Form.Container
                  formRef={(c) => { this.form = c; }}
                  onValid={() => { this.setState({ isValid: true }); }}
                  onInvalid={() => { this.setState({ isValid: false }); }}
                >
                  <Form.Input.default
                    title={`Volume`}
                    name={`volume`}
                    value={``}
                    type={`number`}
                    validationErrors={{
                      isDefaultRequiredValue: `The volume of the coin is required`,
                    }}
                    required
                  />
                  <Form.Select.LocalDataSingleSelect
                    title={`Variant`}
                    name={`variant`}
                    value={``}
                    options={this.state.variants}
                    validationErrors={{
                      isDefaultRequiredValue: `The variant of the coin is required`,
                    }}
                    required
                  />
                  <Form.Select.LocalDataSingleSelect
                    title={`Region`}
                    name={`region`}
                    value={``}
                    options={this.state.regions}
                    validationErrors={{
                      isDefaultRequiredValue: `The region of the coin is required`,
                    }}
                    required
                  />
                  <Form.Select.LocalDataSingleSelect
                    title={`Methods`}
                    name={`method`}
                    value={``}
                    options={this.state.methods}
                    validationErrors={{
                      isDefaultRequiredValue: `The method to produce the coin is required`,
                    }}
                    required
                  />
                  <Form.Input.default
                    title={`IPFS File Hash (Not implemented so enter any dummy data)`}
                    name={`ipfsHash`}
                    // type={`file`}
                    value={``}
                    validationErrors={{
                      isDefaultRequiredValue: `The ipfs hash of the coin will be required`,
                    }}
                    required
                  />
                  <e.Button.DefaultButton
                    disabled={!isValid}
                    onClick={() => this.onSubmit(selectedAccount)}
                  >
                    Mint a coin
                  </e.Button.DefaultButton>
                </Form.Container>
              ))}
            </Web3Provider>
          ))}
        </Query>
        <TransactionModal status={transactionStatus} message={transactionMessage} onClose={() => this.setState({ transactionStatus: null, transactionMessage: null })} />
      </e.Container.CenteredPageContainer>
    );
  }
}
