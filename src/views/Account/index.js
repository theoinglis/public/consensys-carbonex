import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import {
  compose, graphql, withApollo,
} from "react-apollo";
import gql from "graphql-tag";

import * as e from 'common/Elements';

import CarbonCreditsTable from 'views/common/Tables/CarbonCredits';
import SalesTable from 'views/common/Tables/SalesTable';
import TransactionModal from 'views/common/TransactionModal';
import SellModal from './SellModal';


export class AccountView extends Component {
  static propTypes = {
    account: PropTypes.object.isRequired,
  }

  constructor(props) {
    super(props);
    this.state = {
      deedForSelling: null,
      transactionStatus: null,
      transactionMessage: null,
    };
  }

  withdraw = async (saleAddress, salesService) => {
    try {
      this.setState({ transactionMessage: `Please authorise the withdrawal of your ether. (Ganache has an issue (https://github.com/trufflesuite/ganache-core/issues/26) estimating the required gas so please increase (nearly double) the gas limit in a development environment)` });
      await salesService.withdraw(saleAddress);
      this.setState({ transactionMessage: `You should receive your ether shortly`, transactionStatus: `SUCCESS` });
    } catch (e) {
      this.setState({ transactionMessage: e.message, transactionStatus: `ERROR` });
    }
  }


  renderNoWallet = () => {
    return (
      <div>
        <p>
          You haven't requested any carbon credits to be minted yet.
        </p>
        <p>
          You can onboard your carbon credits using the <a href={`/mint`}>Mint Form</a>
        </p>
      </div>
    );
  }

  render = () => {
    const {
      deedForSelling,
      transactionStatus,
      transactionMessage,
    } = this.state;
    const {
    } = this.props;
    return (
      <e.Container.CenteredPageContainer>
        <h1>Account Dashboard</h1>
        <CarbonCreditsTable
          renderNone={this.renderNoWallet}
          getAddresses={(minter) => minter.getMyCoinAddresses()}
          actionComponent={
            (data, coinService) => {
              if (data.ownershipAmount > 0) {
                return (
                  <span>
                  <e.Button.default onClick={() => this.setState({ deedForSelling: data })}>
                    Sell
                  </e.Button.default>
                  <e.Button.default onClick={() => coinService.getIpfsData(data.coin)}>
                    get data
                  </e.Button.default>
                  </span>
                );
              } else {
                return null;
              }
            }
          }
        />
        <h2>Sales Table</h2>
        <SalesTable
          getAddresses={(broker) => broker.getMySales()}
          filterRow={(data) => {
            return !data.isSeller;
          }}
          actionComponent={(data, salesService) => {
            if (data.balance > 0) {
              return (
                <td>
                  <e.Button.default onClick={() => this.withdraw(data.address, salesService)}>
                    Withdraw
                  </e.Button.default>
                </td>
              );
            } else return null;
          }}
        />
        <SellModal
          deed={deedForSelling}
          isOpen={!!deedForSelling}
          onClose={() => this.setState({ deedForSelling: null })}
        />
        <TransactionModal status={transactionStatus} message={transactionMessage} onClose={() => this.setState({ transactionStatus: null, transactionMessage: null })} />
      </e.Container.CenteredPageContainer>
    );
  }
}
export default compose(
  withApollo,
  graphql(gql`
  {
    account @client {
      selectedAccount
    }
  }
`, { name: `account` }),
//   graphql(gql`
//   {
//     carbonCredits(state: "REQUESTED") {
//       ...DetailedCarbonCredit
//     }
//   }
//   ${detail}
// `, { name: `carbonCreditsToMint` }),
//   graphql(gql`
//   mutation MintCarbonCredit($walletAddress: String!, $coinAddress: String!) {
//     mintCarbonCredit(walletAddress: $walletAddress, coinAddress: $coinAddress) {
//       ...DetailedCarbonCredit
//     }
//   }
//   ${detail}
// `, { name: `mintCarbonCredit` }),
)(AccountView);
