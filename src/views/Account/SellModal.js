import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import {
  compose, graphql, withApollo,
} from 'react-apollo';
import gql from 'graphql-tag';
import cc from 'cryptocompare';

import * as e from 'common/Elements';
import * as Form from 'components/Form';
import Modal from 'components/Modal';
import TransactionModal from 'views/common/TransactionModal';

import injectWeb3 from 'components/Web3/hoc';
import { getContract } from 'utils/web3';
import CxBrokerContract from 'contracts/CxBroker.json';
import CbnCoinContract from 'contracts/CbnCoin.json';

export class SellModal extends Component {
  static propTypes = {
    isOpen: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    deed: PropTypes.object,
  }

  constructor(props) {
    super(props);
    this.state = {
      isValid: false,
      usdCoversion: null,
      transactionStatus: null,
      transactionMessage: null,
    };
  }

  componentWillMount = () => {
    cc.price(`ETH`, `USD`)
      .then(({ USD }) => {
        this.setState({
          usdCoversion: USD,
        });
      });
  }

  putCarbonCreditsUpForSale = async ({ pricePerCC, sellAmount }) => {
    const {
      account,
      deed,
      web3,
      onClose,
    } = this.props;
    try {
      const weiPrice = web3.web3.toWei(pricePerCC);
      const coinAddress = deed.address;
      const broker = web3.data.contracts.cxBroker;
      const coin = await getContract(web3.web3, CbnCoinContract, coinAddress);
      this.setState({ transactionMessage: `We want to ensure you are always in contract of your credits so please confirm our exchange can hold your credits for you so that they can be sold.` });
      await coin.approve(broker.address, sellAmount);
      this.setState({ transactionMessage: `Great, now please confirm you want us to put these carbon credits on the exchange for people to purchase.` });
      await broker.putCarbonCreditsUpForSale(coinAddress, sellAmount, weiPrice, { from: account.account.selectedAccount });
      this.setState({ transactionMessage: `Your credits should be on the market shortly.`, transactionStatus: `SUCCESS` });
      onClose();
    } catch (e) {
      this.setState({ transactionMessage: e.message, transactionStatus: `ERROR` });
    }
  }

  onSubmit = () => {
    const {
      isValid,
    } = this.state;

    this.form.submit();
    if (isValid) {
      const model = this.form.getModel();
      this.putCarbonCreditsUpForSale(model);
    }
  }

  onFormChanged = (newProps) => {
    const {
      usdCoversion,
    } = this.state;
    if (newProps && newProps.pricePerCC) {
      const ethPrice = parseFloat(newProps.pricePerCC);
      this.setState({
        usdPrice: usdCoversion * ethPrice,
      });
    }
  }

  render = () => {
    const {
      isValid,
      usdPrice,
      transactionStatus,
      transactionMessage,
    } = this.state;
    const {
      isOpen,
      deed,
      onClose,
      web3,
    } = this.props;

    return (
      <Modal isOpen={isOpen} onClose={onClose}>
        <div>
          <p>Do you want to put these carbon credits up for sale?</p>
          <p>
            <Form.Container
              formRef={(c) => { this.form = c; }}
              onValid={() => { this.setState({ isValid: true }); }}
              onInvalid={() => { this.setState({ isValid: false }); }}
              onChange={this.onFormChanged}
            >
              <Form.Input.default
                title={`Sell Amount`}
                name={`sellAmount`}
                placeholder={`How many do you want to sell?`}
                value={``}
                type={`number`}
                validations={{
                  isMin: 0,
                  isMax: deed && deed.volume,
                }}
                validationErrors={{
                  isMin: `You need to supply a positive number`,
                  isMax: `You can only sell as much as you own (${deed && deed.volume})`,
                  isDefaultRequiredValue: `The volume of the coin is required`,
                }}
                required
              />
              <Form.Input.default
                title={`Price per Carbon Credit (ETH)`}
                name={`pricePerCC`}
                placeholder={`How much would you like to sell them for?`}
                value={``}
                type={`number`}
                validations={{
                  isMin: 0,
                }}
                validationErrors={{
                  isMin: `You need to supply a positive number`,
                  isDefaultRequiredValue: `The volume of the coin is required`,
                }}
                required
              />
              {usdPrice ? (
                <div>(Roughly {usdPrice} USD)</div>
              ) : null}
              {web3.initialised ? (
                <e.Button.DefaultButton
                  disabled={!isValid}
                  onClick={() => this.onSubmit()}
                >
                  Put up for sale
                </e.Button.DefaultButton>
              ) : null}
            </Form.Container>
          </p>
        </div>
        <TransactionModal status={transactionStatus} message={transactionMessage} onClose={() => this.setState({ transactionStatus: null, transactionMessage: null })} />
      </Modal>
    );
  }
}
export default compose(
  withApollo,
  graphql(gql`
  {
    account @client {
      selectedAccount
    }
  }
`, { name: `account` }),
  injectWeb3({
    onInitialise: async (web3) => {
      const cxBroker = await getContract(web3, CxBrokerContract);
      console.log(`got broker`, cxBroker);
      return {
        contracts: {
          cxBroker,
        },
      };
    },
  }),
)(SellModal);
