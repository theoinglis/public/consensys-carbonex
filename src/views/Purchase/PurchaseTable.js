import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import {
  compose, graphql, withApollo,
} from 'react-apollo';
import gql from 'graphql-tag';
import injectWeb3 from 'components/Web3/hoc';
import { asyncWrapper } from 'common/AsyncComponent';

import * as e from 'common/Elements';
import BuyModal from './BuyModal';

import SalesService from 'services/contracts/Sales';
import CxBrokerService from 'services/contracts/Broker';

export class PurchaseTable extends Component {
  static propTypes = {
    filterRow: PropTypes.func,
  }
  static defaultProps = {
    filterRow: () => false,
  }

  constructor(props) {
    super(props);
    this.state = {
      sales: null,
      salesData: {},
      deedToBuyFrom: null,
    };
  }

  componentDidMount = () => {
    this.refreshSales();
  }

  refreshSales = async () => {
    const {
      web3,
    } = this.props;
    const {
      brokerService,
      salesService,
    } = web3.data.services;
    const sales = await brokerService.getSalesAddresses();
    if (!sales) {
      this.setState({
        sales: null,
      });
    } else {
      this.setState({
        sales,
        salesData: {},
      });
      sales.forEach((saleAddress) => {
        salesService.getSaleData(saleAddress, (data) => {
          const newSalesData = this.state.salesData;
          newSalesData[saleAddress] = data;
          this.setState({
            salesData: newSalesData,
          });
        });
      });
    }
  }

  renderNoSales = () => {
    return (
      <div>
        There aren't any carbon credits to purchase at the moment
      </div>
    );
  }

  render = () => {
    const {
      filterRow,
    } = this.props;
    const {
      sales,
      salesData,
      account,
    } = this.state;

    return (
      <div>
        {sales ? (
          <table>
            <thead>
              <th>Price Per Coin (ETH)</th>
              <th>Remaining</th>
              <th>Total</th>
              <th></th>
            </thead>
            <tbody>
              {sales.map((s) => {
                const data = salesData[s];
                if (!data || data.loading) return <tr><td span={3}>loading</td></tr>
                if (data.error) return <tr><td span={3}>error</td></tr>
                if (filterRow(data.data)) return null;
                else {
                  return (
                    <tr>
                      <td>{data.data.pricePerCoin}</td>
                      <td>{data.data.remainingAmount}</td>
                      <td>{data.data.totalAmount}</td>
                      <td>
                        <e.Button.default onClick={() => this.setState({ deedToBuyFrom: data.data })}>
                          Buy
                        </e.Button.default>
                      </td>
                    </tr>
                  );
                }
              })}
            </tbody>
          </table>
        ) : this.renderNoSales()}
        <BuyModal
          isOpen={!!this.state.deedToBuyFrom}
          onClose={() => this.setState({ deedToBuyFrom: null })}
          deed={this.state.deedToBuyFrom}
        />
      </div>
    );
  }
}
export default compose(
  withApollo,
  injectWeb3({
    onInitialise: async (web3) => {
      const brokerService = new CxBrokerService(web3);
      const salesService = new SalesService(web3);
      return {
        services: {
          brokerService,
          salesService,
        },
      };
    },
  }),
  asyncWrapper({
    check: [`web3`],
  }),
)(PurchaseTable);
