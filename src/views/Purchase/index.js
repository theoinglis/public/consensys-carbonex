import React, { Component } from 'react';
import {
  compose, withApollo,
} from 'react-apollo';

import * as e from 'common/Elements';
import BuyModal from './BuyModal';

import SalesTable from 'views/common/Tables/SalesTable';

export class PurchaseView extends Component {

  constructor(props) {
    super(props);
    this.state = {
      deedToBuyFrom: null,
    };
  }

  render = () => {
    const {
      deedToBuyFrom,
    } = this.state;
    return (
      <e.Container.CenteredPageContainer>
        <h1>Purchase</h1>
        <SalesTable
          filterRow={(data) => {
            return data.isSeller;
          }}
          actionComponent={(data, service) => (
            <td>
              <e.Button.default onClick={() => this.setState({ deedToBuyFrom: data })}>
                Buy
              </e.Button.default>
            </td>
          )}
        />
        <BuyModal
          isOpen={!!deedToBuyFrom}
          onClose={() => this.setState({ deedToBuyFrom: null })}
          deed={deedToBuyFrom}
        />
      </e.Container.CenteredPageContainer>
    );
  }
}
export default compose(
  withApollo,
)(PurchaseView);
