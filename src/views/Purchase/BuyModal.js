import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import {
  compose, graphql, withApollo,
} from 'react-apollo';
import gql from 'graphql-tag';
const cc = require('cryptocompare');

import * as e from 'common/Elements';
import * as Form from 'components/Form';
import Modal from 'components/Modal';
import TransactionModal from 'views/common/TransactionModal';

import injectWeb3 from 'components/Web3/hoc';
import CbnSaleService from 'services/contracts/Sales';

export class BuyModal extends Component {
  static propTypes = {
    isOpen: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    deed: PropTypes.object,
  }

  constructor(props) {
    super(props);
    this.state = {
      isValid: false,
      usdCoversion: null,
      transactionStatus: null,
      transactionMessage: null,
    };
  }

  componentWillMount = () => {
    cc.price(`ETH`, [`USD`,`GBP`])
      .then(({ USD }) => {
        this.setState({
          usdCoversion: USD,
        });
      });
  }

  buyCarbonCredits = async ({ buyAmount }) => {
    const {
      account,
      deed,
      web3,
      onClose,
    } = this.props;
    const {
      address,
    } = deed;
    try {
      this.setState({ transactionMessage: `Please confirm you want to purchase these carbon credits` });
      const cbnSales = web3.data.services.cbnSales;
      await cbnSales.buyFromDeed(address, buyAmount, account.account.selectedAccount);
      this.setState({ transactionMessage: `You should receive your carbon credits soon`, transactionStatus: `SUCCESS` });
      onClose();
    } catch (e) {
      this.setState({ transactionMessage: e.message, transactionStatus: `ERROR` });
    }
  }

  onSubmit = () => {
    const {
      isValid,
    } = this.state;

    this.form.submit();
    if (isValid) {
      const model = this.form.getModel();
      this.buyCarbonCredits(model);
    }
  }

  onFormChanged = (newProps) => {
    const {
      deed,
    } = this.props;
    const {
      usdCoversion,
    } = this.state;
    if (newProps && newProps.buyAmount && deed) {
      const ethCost = deed.pricePerCoin * newProps.buyAmount;
      this.setState({
        // ethCost,
        usdCost: usdCoversion * ethCost,
      });
    }
  }

  render = () => {
    const {
      isValid,
      usdCost,
      transactionStatus,
      transactionMessage,
    } = this.state;
    const {
      isOpen,
      deed,
      onClose,
      web3,
    } = this.props;

    return (
      <Modal isOpen={isOpen} onClose={onClose}>
        <div>
          <p>How many carbon credits do you want to buy?</p>
          <p>
            <Form.Container
              formRef={(c) => { this.form = c; }}
              onValid={() => { this.setState({ isValid: true }); }}
              onInvalid={() => { this.setState({ isValid: false }); }}
              onChange={this.onFormChanged}
            >
              <Form.Input.default
                title={`Buy Amount`}
                name={`buyAmount`}
                placeholder={`How many do you want to buy?`}
                value={``}
                type={`number`}
                validations={{
                  isMin: 0,
                  isMax: deed && deed.remainingAmount,
                }}
                validationErrors={{
                  isMin: `You need to supply a positive number`,
                  isMax: `You can only buy as much as many as are available (${deed && deed.remainingAmount})`,
                  isDefaultRequiredValue: `The number you want to buy is required`,
                }}
                required
              />
              {usdCost ? (
                <div>(Roughly {usdCost} USD)</div>
              ) : null}
              {web3.initialised ? (
                <e.Button.DefaultButton
                  disabled={!isValid}
                  onClick={() => this.onSubmit()}
                >
                  Buy
                </e.Button.DefaultButton>
              ) : null}
            </Form.Container>
          </p>
        </div>
        <TransactionModal status={transactionStatus} message={transactionMessage} onClose={() => this.setState({ transactionStatus: null, transactionMessage: null })} />
      </Modal>
    );
  }
}
export default compose(
  withApollo,
  graphql(gql`
  {
    account @client {
      selectedAccount
    }
  }
`, { name: `account` }),
  injectWeb3({
    onInitialise: async (web3) => {
      const cbnSales = new CbnSaleService(web3);
      return {
        services: {
          cbnSales,
        },
      };
    },
  }),
)(BuyModal);
