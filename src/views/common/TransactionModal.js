import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import * as e from 'common/Elements';
import Modal from 'components/Modal';

export default class TransactionModal extends Component {
  render = () => {
    const {
      message,
      status,
      onClose,
    } = this.props;
    return (
      <Modal isOpen={!!message} canClose={status !== null} onClose={onClose}>
        {status === `SUCCESS` ? (
          <div>
            {message}
          </div>
        ) : status === `ERROR` ? (
          <div>
            {message}
          </div>
        ) : (
          <e.Loading.CenterLoader>
            {message}
          </e.Loading.CenterLoader>
        )}
      </Modal>
    );
  }
}
