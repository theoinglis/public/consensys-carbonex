import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import {
  compose, graphql, withApollo,
} from 'react-apollo';
import gql from 'graphql-tag';
import injectWeb3 from 'components/Web3/hoc';
import { asyncWrapper } from 'common/AsyncComponent';

import * as e from 'common/Elements';

import CbnCoinService from 'services/contracts/Coin';
import CxMinterService from 'services/contracts/Minter';

export class CbnCoinTable extends Component {
  static propTypes = {
    renderNone: PropTypes.func,
    filterRow: PropTypes.func,
    getAddresses: PropTypes.func,
    actionComponent: PropTypes.func,
  }
  static defaultProps = {
    filterRow: () => false,
    getAddresses: minter => minter.getCoinAddresses(),
    actionComponent: () => null,
  }

  constructor(props) {
    super(props);
    this.state = {
      coins: null,
      coinsData: {},
    };
  }

  componentWillMount = () => {
    this.refreshCbnCoin();

    const {
      web3,
    } = this.props;
    const {
      minterService,
    } = web3.data.services;

    minterService.getContract()
      .then((minter) => {
        minter.MintingRequest()
          .watch((err, result) => {
            this.refreshCbnCoin();
          });
        minter.MintedCoin()
          .watch((err, result) => {
            debugger;
            this.refreshCbnCoin();
          })
        minter.OwnsCoins()
          .watch((err, result) => {
            this.refreshCbnCoin();
          })
      });
  }

  refreshCbnCoin = async () => {
    const {
      web3,
      getAddresses,
    } = this.props;
    const {
      minterService,
      coinsService,
    } = web3.data.services;
    const coins = await getAddresses(minterService);
    if (!coins || coins.length === 0) {
      this.setState({
        coins: null,
      });
    } else {
      this.setState({
        coins,
        coinsData: {},
      });
      coins.forEach((saleAddress) => {
        coinsService.getCoinData(saleAddress, (data) => {
          const newCbnCoinData = this.state.coinsData;
          newCbnCoinData[saleAddress] = data;
          this.setState({
            coinsData: newCbnCoinData,
          });
        });
      });
    }
  }

  renderNoCbnCoin = () => {
    const {
      renderNone,
    } = this.props;
    if (renderNone) return renderNone();
    return (
      <div>
        No coins were retrieved
      </div>
    );
  }

  render = () => {
    const {
      filterRow,
      web3,
    } = this.props;
    const {
      coinsService,
    } = web3.data.services;
    const {
      coins,
      coinsData,
    } = this.state;

    return (
      <div>
        {coins ? (
          <table>
            <thead>
              <th>Volume</th>
              <th>Variant</th>
              <th>Region</th>
              <th>Method</th>
              <th>Ownership Amount</th>
              <th>State</th>
              <th />
            </thead>
            <tbody>
              {coins.map((s) => {
                const data = coinsData[s];
                if (!data || data.loading) return <tr><td colSpan={99}>loading</td></tr>
                else if (data.error) {
                  return <tr><td colSpan={99}>error loading coin with address {s}</td></tr>
                }
                else if (filterRow(data.data)) return null;
                else {
                  return (
                    <tr>
                      <td>{data.data.volume}</td>
                      <td>{data.data.variant}</td>
                      <td>{data.data.region}</td>
                      <td>{data.data.method}</td>
                      <td>{data.data.ownershipAmount}</td>
                      <td>{data.data.state}</td>
                      {
                        this.props.actionComponent(data.data, coinsService)
                      }
                    </tr>
                  );
                }
              })}
            </tbody>
          </table>
        ) : this.renderNoCbnCoin()}
      </div>
    );
  }
}
export default compose(
  withApollo,
  injectWeb3({
    onInitialise: async (web3) => {
      const minterService = new CxMinterService(web3);
      await minterService.getContract();
      const coinsService = new CbnCoinService(web3);
      return {
        services: {
          minterService,
          coinsService,
        },
      };
    },
  }),
  asyncWrapper({
    check: [`web3`],
  }),
)(CbnCoinTable);
