import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  compose, graphql, withApollo,
} from 'react-apollo';
import gql from 'graphql-tag';
import injectWeb3 from 'components/Web3/hoc';
import { asyncWrapper } from 'common/AsyncComponent';

import * as e from 'common/Elements';

import CoinsService from 'services/contracts/Coin';
import SalesService from 'services/contracts/Sales';
import CxBrokerService from 'services/contracts/Broker';

export class SalesTable extends Component {
  static propTypes = {
    renderNone: PropTypes.func,
    filterRow: PropTypes.func,
    getAddresses: PropTypes.func,
    actionComponent: PropTypes.func,
  }
  static defaultProps = {
    filterRow: () => false,
    getAddresses: broker => broker.getSales(),
    actionComponent: () => null,
  }

  constructor(props) {
    super(props);
    this.state = {
      sales: null,
      salesData: {},
      coinsData: {},
    };
  }

  componentWillMount = () => {
    this.refreshSales();

    const {
      web3,
    } = this.props;
    const {
      brokerService,
    } = web3.data.services;

    brokerService.getContract()
      .then((broker) => {
        broker.OnSaleCreated()
          .watch((err, result) => {
            this.refreshSales();
          });
      });
  }

  refreshSales = async () => {
    const {
      web3,
      getAddresses,
    } = this.props;
    const {
      brokerService,
      salesService,
      coinsService,
    } = web3.data.services;
    const salesContract = await brokerService.getContract();
    const sales = await getAddresses(salesContract);
    if (!sales || sales.length === 0) {
      this.setState({
        sales: null,
      });
    } else {
      this.setState({
        sales,
        salesData: {},
      });
      sales.forEach((saleAddress) => {
        salesService.getSaleData(saleAddress, (data) => {
          const newSalesData = this.state.salesData;
          newSalesData[saleAddress] = data;
          if (data.data && data.data.coinAddress) {
            coinsService.getCoinData(data.data.coinAddress, (coinData) => {
              const newCoinsData = this.state.coinsData;
              newCoinsData[data.data.coinAddress] = coinData;
              this.setState({
                coinsData: newCoinsData,
              });
            });
          }
          this.setState({
            salesData: newSalesData,
          });
        });
      });
    }
  }

  renderNoSales = () => {
    return (
      <div>
        You have not put any coins on the market
      </div>
    );
  }

  render = () => {
    const {
      sales,
      salesData,
      coinsData,
      transactionStatus,
      transactionMessage,
    } = this.state;
    const {
      web3,
      filterRow,
    } = this.props;
    const {
      salesService,
    } = web3.data.services;

    return (
      <div>
        {sales ? (
          <table>
            <thead>
              <th>Price Per Coin (ETH)</th>
              <th>Remaining</th>
              <th>Total</th>
              <th>Balance (ETH)</th>
              <th>Region</th>
              <th>Variant</th>
              <th>Method</th>
            </thead>
            <tbody>
              {sales.map((s) => {
                const data = salesData[s];
                if (!data || data.loading) return <tr><td colSpan={999}>loading</td></tr>;
                else if (filterRow(data.data)) return null;
                else {
                  const coinData = coinsData[data.data.coinAddress];
                  return (
                    <tr>
                      <td>{data.data.pricePerCoin}</td>
                      <td>{data.data.remainingAmount}</td>
                      <td>{data.data.totalAmount}</td>
                      <td>{data.data.balance}</td>
                      <td>{coinData.data.region}</td>
                      <td>{coinData.data.variant}</td>
                      <td>{coinData.data.method}</td>
                      {
                        this.props.actionComponent(data.data, salesService)
                      }
                    </tr>
                  );
                }
              })}
            </tbody>
          </table>
        ) : this.renderNoSales()}
      </div>
    );
  }
}
export default compose(
  withApollo,
  graphql(gql`
  {
    account @client {
      selectedAccount
    }
  }
`, { name: `account` }),
  injectWeb3({
    onInitialise: async (web3) => {
      const brokerService = new CxBrokerService(web3);
      await brokerService.getContract();
      const salesService = new SalesService(web3);
      const coinsService = new CoinsService(web3);
      return {
        services: {
          brokerService,
          salesService,
          coinsService,
        },
      };
    },
  }),
  asyncWrapper({
    check: [`web3`],
  }),
)(SalesTable);
