import React from 'react';
import PropTypes from 'prop-types';

const LogoutButton = ({ onLogoutUserClick }) => {
  return (
    <li className="pure-menu-item">
      <button
        className="pure-menu-link"
        type={`button`}
        onClick={(event) => onLogoutUserClick(event)}
      >
Logout
      </button>
    </li>
  );
};

LogoutButton.propTypes = {
  onLogoutUserClick: PropTypes.func.isRequired,
};

export default LogoutButton;
