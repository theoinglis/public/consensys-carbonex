import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Profile extends Component {
  static propTypes = {
    authData: PropTypes.object.isRequired,
  }

  render() {
    const {
      authData,
    } = this.props;
    return (
      <main className="container">
        <div className="pure-g">
          <div className="pure-u-1-1">
            <h1>
Profile
            </h1>
            <p>
Change these details in UPort to see them reflected here.
            </p>
            <p>
              <strong>
Name
              </strong>
<br />
              {authData.name}
            </p>
          </div>
        </div>
      </main>
    );
  }
}

export default Profile;
