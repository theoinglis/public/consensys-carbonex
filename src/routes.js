import React from 'react';
import { Route, Switch } from 'react-router-dom';
import {
  compose, graphql, withApollo,
} from "react-apollo";
import gql from "graphql-tag";


import AppContainer from './views/AppContainer';
import Home from './views/Home';
import AccountView from './views/Account';
import MintCoinView from './views/MintCoin';
import PurchaseView from './views/Purchase';
import AdminView from './views/Admin';


export function Routes(props) {
  const {
    isAdmin,
  } = props.account.account;
  return (
    <Switch>
      <AppContainer>
        {
          isAdmin ? (
            <Switch>
              <Route path={`/admin`} component={AdminView} />
              <Route path={`/`} component={Home} />
            </Switch>
          ) : (
            <Switch>
              <Route path={`/account`} component={AccountView} />
              <Route path={`/mint`} component={MintCoinView} />
              <Route path={`/purchase`} component={PurchaseView} />
              <Route path={`/`} component={Home} />
            </Switch>
          )
        }
      </AppContainer>
    </Switch>
  );
}
export default compose(
  withApollo,
  graphql(gql`
  {
    account @client {
      isAdmin
    }
  }
`, { name: `account` }),
)(Routes);
