import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Dashboard extends Component {
  static propTypes = {
    authData: PropTypes.object.isRequired,
  }

  render() {
    const {
      authData,
    } = this.props;
    return (
      <main className="container">
        <div className="pure-g">
          <div className="pure-u-1-1">
            <h1>
Dashboard
            </h1>
            <p>
<strong>
Congratulations
{authData.name}
!
</strong>
{` `}
              {`If you're seeing this page, you've logged in with UPort successfully.`}
            </p>
          </div>
        </div>
      </main>
    );
  }
}

export default Dashboard;
