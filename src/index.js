import "./index.css";
import React from 'react';
import ReactDOM from 'react-dom';
import {
  BrowserRouter as Router, Route,
} from 'react-router-dom';
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import { getConvertedBalance, getAccounts, getContract } from 'utils/web3';
import { Provider } from 'react-redux';
import { ApolloProvider } from 'react-apollo';
import { ThemeProvider } from 'styled-components';
import client from 'utils/client';
import { UserIsAuthenticated } from 'utils/wrappers';
import Web3Provider from 'components/Web3/component';
import { ErrorProvider } from 'services/error';
import CxMinterContract from 'contracts/CxMinter.json';
import styled from 'styled-components';
import * as e from 'common/Elements';
import theme from './components/StyledComponentTheme';
import Routes from './routes';
// Redux Store
import store from './store';

const PageContainer = styled.div`
  background: ${(p) => p.theme.colorPrimary};
  color: white;
  display: flex;
  flex-direction: column;
  height: 100vh;
  width: 100vw;
`;
const CenteredContentContainer = styled.div`
  flex: 1;
  display: flex;
  align-items: center;
  justify-content: center;
`;
const CenteredContent = styled.div`
  padding: 0 20px;
  max-width: 400px;
  margin-bottom: 40px;
`;

const SET_ACCOUNT_DETAILS = gql`
  mutation SetAccountDetails($account: Account!) {
    setAccountDetails(account: $account) @client
  }
`;

const setAccountDetails = async (web3, mutate) => {
  const accounts = await getAccounts(web3);
  const selectedAccount = accounts && accounts[0];
  if (!selectedAccount) throw Error(`We couldn't select an account to use for transactions. Make sure you are logged in to your wallet provider.`);
  web3.eth.defaultAccount = selectedAccount;
  const balance = await getConvertedBalance(web3, selectedAccount);
  let isAdmin = false;
  try {
    const contract = await getContract(web3, CxMinterContract);
    isAdmin = await contract.amAdmin();
  } catch (err) {
    throw Error(`It doesn't look like the blockchain contracts have been deployed to this network. Please ensure you are connected to the correct network.`, err);
  }
  const accountInfo = {
    isLoggedIn: true,
    balance: balance.toFixed(2),
    isAdmin,
    selectedAccount,
    accounts,
  };
  mutate({
    variables: {
      account: accountInfo,
    },
  });
  return accountInfo;
};

ReactDOM.render((
  <ApolloProvider client={client}>
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <PageContainer>
          <Router>
            <ErrorProvider>
              <Mutation mutation={SET_ACCOUNT_DETAILS}>
                {(mutate) => (
                <Web3Provider onInitialise={async (web3) => {
                  return await setAccountDetails(web3, mutate);
                }}
                >
                    {({
                      initialised, loading, error, errorInfo,
                    }) => {
                      if (error) {
                        return (
                          <CenteredContentContainer>
                            <CenteredContent>
                              <h1>Oh no! We had a problem interacting with the blockchain</h1>
                              <p>
                                {errorInfo.message ? errorInfo.message : (
                                  `Please ensure you are connected to the right network and try again.`
                                )}
                              </p>
                            </CenteredContent>
                          </CenteredContentContainer>
                        );
                      } else if (!initialised || loading) {
                        return (
                          <CenteredContentContainer>
                            <CenteredContent>
                              <e.Loading.CenterLoader isLoading>
                                Initialising web3
                              </e.Loading.CenterLoader>
                            </CenteredContent>
                          </CenteredContentContainer>
                        );
                      } else {
                        return (
                          <Routes />
                        );
                      }
                    }

                  }
                </Web3Provider>
                )}
              </Mutation>
            </ErrorProvider>
          </Router>
        </PageContainer>
      </ThemeProvider>
    </Provider>
  </ApolloProvider>
),
document.getElementById(`root`));
