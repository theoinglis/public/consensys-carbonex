# Features

## Running the app
Please see [Intro](../README.md) to see how to run the app locally.

## Metamask
I have used metamask to sign transactions for the user safely.

## Security Measures
These are comprehensively outlined in the [Avoiding Common Attacks](./avoiding_common_attacks.md) document, but a quick overview is the use of tried and tested contracts, particularly those available through the [Zeppelin](https://openzeppelin.org/) that have been audited and verified by experts. This includes the contract Pausible which I used in CbnSale, that adopts the circuit breaker/emergency stop design to allow a user to stop the sale of their coins if they have a problem.

## Account information
Relevant user information has been extracted (e.g. user balance), but also to provide contextual information such as a unique interface for admin users, and owners of coins/sales.

## Reloading on Events
Events are used on all state changes so that any client can listen to them. These have been used through the site to reload information when appropriate.

## Registry/Upgradeable Pattern
I have used ENS to create a registry that holds addresses to other contracts. This allows the registry to update the address and the relevant contracts to reference the new contract, thus making it possible to upgrade functionality without redeploying everything.

## ENS
I have used ENS as my registry contract. I currently own `carbonex.eth` which I wll be using in production, but in the meantime in development environments I have deployed my own versions (as I don't need the addresses publicly available). The resolver is then passed to any contract referencing another singleton contract. This will make it easy and flexible to update some business logic in the future.

## IPFS
I will be using IPFS further for holding data I don't want to store on the blockchain to reduce costs (e.g. long descriptions and images), but for now I have used IPFS to host my static website. It can be slow to load at: https://ipfs.io/ipfs/QmeRsZFouWtg1SDHDjvfJWjiwUajTrXByqkZtLdz7yjPip/ so please try the Infura gateway instead at: https://ipfs.infura.io/ipfs/QmeRsZFouWtg1SDHDjvfJWjiwUajTrXByqkZtLdz7yjPip/ .
It it slow to load their too please look at the html in the browser inspector to see the html has loaded, and https://ipfs.io/ipfs/QmeRsZFouWtg1SDHDjvfJWjiwUajTrXByqkZtLdz7yjPip/static/css/main.55a2e683.css to see that the css is deployed. It seems it mostly has an issue loading the js. 

If those gateway links don't work please try another gateway from the following list: https://ipfs.github.io/public-gateway-checker/ or alternatively, you can start an `ipfs daemon` and view it at [http://localhost:8080/ipfs/QmeRsZFouWtg1SDHDjvfJWjiwUajTrXByqkZtLdz7yjPip/](http://localhost:8080/ipfs/QmeRsZFouWtg1SDHDjvfJWjiwUajTrXByqkZtLdz7yjPip/) and that should load faster.


## Oracle
I have implemented my own Oracle. It only works locally (won't work on any other network either) as it requires the running of the [Oracle Server](../backend/oracle.js). It has no use at the moment but is there for future uses. It is used by the CbnCoin contract. It simply forwards a request to the OracleProcessor, so that the oracle server who is waiting for events can proccess the request of chain, and send the results back to the processor who can then subsequently forward it on to the original contract.

## Testnet
The contracts have been deployed to both Rinkeby and Ropsten. The relevant addresses can be found in the [deployed_addresses.txt](./deployed_addresses.txt) file. Alternatively, please change your Metamask network to interact with them at the [IPFS url](https://ipfs.io/ipfs/QmeRsZFouWtg1SDHDjvfJWjiwUajTrXByqkZtLdz7yjPip/). You can even use the mnemonic "bullet heavy stock steak salute annual solution involve document inner home latin" to interact as an admin.
