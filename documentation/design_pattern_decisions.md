# Design Pattern Decisions
[https://courses.consensys.net/courses/course-v1:ConsenSysAcademy+2018DP+1/courseware/6ad0b7c56c8947a2b101cb916896fb9f/edd7e596883247cbb04e5145b3508467/?activate_block_id=block-v1%3AConsenSysAcademy%2B2018DP%2B1%2Btype%40sequential%2Bblock%40edd7e596883247cbb04e5145b3508467](Consensys Design Pattern Page)

## Naming convention
Started contract names with Cbn to easily identify trusted contracts

## Fail early and fail loud
I have extensively used require/modifiers to ensure that contracts don't get into a bad state.

## Restricting Access
I have also used Ownable and Admin inheritance with modifiers to restrict function to only those users who need to perform the function and would be less likely to be malicious with their intentions (as it is more likely they would be the victim). I have further been cautious with use of visibility modifiers, e.g. public/external/internal/private to restrict access as much as possible

## Inheritance/Library/EthPM
I have inherited from robust and code tested by experts (e.g. Zeppeling throught EthPM). This reduces duplication and means more of my code has been verified as safe by experts.

## Commenting/Interfaces
I have thoroughly used and commented interfaces. Interfaces make it easy to update business functionality later and prevent recursive depenedencies. There is thorough commenting on the interfaces too following Doxygen commenting.

## Registry
I have a registry that is passed into contracts and allows them to reference other contracts. This allows the contracts to be upgraded at a later date and for the registry contract to update the address allowing all contracts to be updated.

## Storage
I have used several contract to separate storage in a scalable way. By ensuring contracts have a separation of concerns there will be less duplication, e.g. there is contract (CxMinter) to store the list of coins, and another contract (CbnCoin) to hold it's information, that way a new coin won't significantly add to the storage of CxMinter and result in creating coins to be much more expensive.

## Withdrawal Pattern
I have used the Zeppelin PullPayment contract to inherit from in CbnSale (the only contract where payment is being made). This uses the withdrawal pattern which protects against multiple security concerns.

## Circuit Breakers
CbnSale also uses the Zeppelin Pausible contract that creates a circuit breaker for the user. This will allow them to pause any user from purchasing carbon credits in the contract if they need or are concerned by a bug.

## State Machine
I have used state machines throughout (see CbnCoin and CbnSale) to prevent contracts getting in to states they shouldn't or perform functions until they are ready.