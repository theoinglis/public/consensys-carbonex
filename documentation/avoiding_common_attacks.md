# Security

## Using Tested Contracts/Libraries
I have extensively used EthPM and tested contracts/libraries that have been verified by experts in the field, such as EIP20 token and Zeppelin. Instead of creating new implementations this provides the contracts with the best chance of best practices and avoiding security vulnerabilities

## Extensive use of modifiers/require statements
These are used to ensure valid data before changes are attempted and have been used extensively through all contracts.

## SafeMath
To avoid Integer Overflow/Underflow issues I have used the Zeppelin SafeMath library that will throw an error if any possible side effects might be caused.

## External calls last
I have put external contract calls (especially external contracts) last. This removes the vulnerability to several attacks (Reentrancy, Cross-function Race Conditions, Transaction-Ordering Dependence (TOD) / Front Running).

## Timestamp dependence
None of my contracts have a dependency on a timestamp avoiding this problem. 

## Withdrawal Pattern
I have employed the withdrawal pattern (by inheriting and using the Zeppelin PullPayment contract) in my CbnSale contract (the only contract accepting payment). This avoids (DoS with unexpected revert and DoS with Block Gas Limit).

## Allow for extra Ether
In order to accommodate a user Forcibly Sending Ether to a Contract I have ensured checks are used appropriately and that it checks if it is greater or equal to the balance so that an attacker can't use this to put a contract in bad state.

## Owner/Admin functionality
I have used owner/admin functionality as much as possible to make sure that only Carbonex or the users with good intentions (those who won't benefit from manipulation) can interact with contracts in the right way.