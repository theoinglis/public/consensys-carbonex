# Architecture

## Mint
This folder handles the creation and use of the carbon credits (coins).

**CbnCoin:** This is a represenation of a Carbon Credit. It has the essential information (and no more, to ensure not too much is stored on the blockchain). It allows the ownership and transfer of coins, and admin functions to only accept the creation of the coins on an admin confirming they are genuine.\
**CxMinter:** The Carbonex minter handles the list of coins created, and takes ownership of them, in order to ensure their integrity. The list of coins is kept separate from the data in order to prevent the contract storage expanding too quickly and becoming expensive.

## Sale
This folder focuses on the sale of carbon credits.

**CbnSale:** This holds the information for a user selling their coin. It holds information about the coin being sold, the owner of the coin, the amount being purchased and for what price. Buyers can interact with this contract to purchase and transfer carbon credits.\
**CbnBroker:** This holds the list of carbon credits up for sale and again keeps the storage per block kept to a minimum by keeping the sale information separate from the list of sales.

## Categories
The category contract ensures data integrity and is for validation purposes. It's a universal tool to ensure that only admin created categories and relevant options are able to be created.

**For Example:** There are several methods to create cabon credits (e.g. solar, wind, ...) that purchases might care about. We need to allow carbon credits to be created with this information but we want to ensure that the data is valid. This is currently being done by the Categories contract and is external to the other contracts. This allows updates to happen and for the list of categories not to be repeated.

I expect this to be moved out to a trusted oracle request in the future to reduce the on chain computation and minimise costs.

## Storage
Storage has been separated as much as possible to increase scalability. This structure requires creating more contracts, which can be costly, however, it comes at the benefit of not passing on costs to every user as the exchange grows. If it wasn't done this way every transaction would require duplication of all the data, which would eventually become very costly. So instead we pass the cost on to the user who wants to sell or create coins (who has the most reason to invest in the action).

We also split the storage into several places. This means we create 3 main storage contracts (CxStorage, CxMinter, CxBroker), which has a small overhead in creation, but again reduces the costs of future transactions which we therefore expect to be beneficial. However, all the storage still makes the most of the separation of concerns and can be seen as a central contract to get and update their respective information.

## Registry (ENS)
I have made use of a registry contract to allow interaction between singleton contracts. Specifically I am interacting with an ENS registry I have deployed or the ENS registry in production. For the relevant contracts I pass in this registry and it allows me to change out some business logic as I require.

**For Example:** CbnCoin has refence to the Categories contract. It does this by querying the registry ("categories.carbonex.eth") and using that contract to validate the information. This makes it very flexible in future to update the validation without redeploying the contract. I can simply point it to a new contract in ENS to do the validation.

## Oracle
A simple Oracle has been created. It includes:

**OracleRequester:** By inheriting this contract and implementing the abstract function to handle the oracle callback the contract can get information not on the blockchain.\
**OracleProcessor:** This is a separate contract that responds to requests. A server (see [Oracle Server](../backend/oracle.js)) listens for events, processes them and sends them back to the processor, who can then forward them back to the original contract.

## Support/EthPM
I have used Support libraries and contracts wherever possible, of which have been created by me, installed from EthPM, or duplicated from reliable sources. This ensure minimum duplication and makes the most of tried and tested contracts for maximum stability and security.

## Interfaces and Libraries
**Libraries:** Some of the functionality was extracted into libraries (e.g. CxBrokerLibrary). This is to reduce contract size while establishing consistent and robust contract logic that can be reused on upgrading contracts.\
**Interfaces:** Interfaces have been used (and commented). This allows functionality to be upgraded and exchanged in the future and allow for contracts to reference each other without causing any circular references.

