module.exports = {
  parserOptions: {
    ecmaVersion: 6,
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true,
    },
  },
  env: {
    'browser': true,
    'node': true,
    'es6': true,
    'mocha': true,
    'jest/globals': true,
  },
  parser: 'babel-eslint',
  plugins: [
    'import',
    'react',
    'jsx-a11y',
    'jest',
    'import-order-autofix',
  ],
  extends: 'airbnb',
  rules: {
    'no-console': 'error',
    'arrow-parens': ["error", "always"],
    'arrow-body-style': 'off',
    'no-await-in-loop': 'off',
    'no-restricted-syntax': 'off',
    'no-else-return': 'off',
    'no-param-reassign': [2, { 
      'props': false
    }],
    'guard-for-in': 'off',
    'class-methods-use-this': 'off',
    'dot-notation': 'off',
    'no-underscore-dangle': 'off',
    'quotes': ['warn', 'backtick'],
    'quote-props': ['error', 'consistent-as-needed'],
    'react/react-in-jsx-scope': 'off',
    'import/no-extraneous-dependencies': 'off',
    'import/no-named-as-default': 'off',
    'no-nested-ternary': 'off',
    'import/first': [2, { 'absolute-first' : 'off' }],
    'import/prefer-default-export': 'off',
    'react/prefer-stateless-function': 'off',
    'react/jsx-indent-props': [2, 2],
    'react/require-default-props': 'off',
    'react/forbid-prop-types': 'off',
    'react/jsx-indent': 'off',
    'react/jsx-curly-brace-presence': 'off',
    'jsx-a11y/label-has-for': 'off',
    'jsx-a11y/anchor-is-valid': 'off',
    'react/jsx-one-expression-per-line': 'off',
    'react/sort-comp': [1, {
      order: [
        'static-methods',
        'lifecycle',
        'everything-else',
        'rendering',
      ],
      groups: {
        rendering: [
          '/^render.+$/',
          'render'
        ]
      }
    }],
    'react/jsx-filename-extension': [
      1,
      {
        extensions: [
          '.js',
        ],
      },
    ],
    'consistent-return': 'warn',
    'jest/no-disabled-tests': 'warn',
    'jest/no-focused-tests': 'error',
    'jest/no-identical-title': 'error',
    'jest/valid-expect': 'error',
    'jsx-a11y/no-noninteractive-element-interactions': 'off',
    'jsx-a11y/no-static-element-interactions': 'off',
  },
  overrides: [
    {
      files: [
        '**/__test__/**/*.js',
        '**/*.spec.js',
      ],
      rules: {
        'no-param-reassign': 'off',
      }
    },
    {
      files: [
        'test/**/*.js',
        'migrations/**/*.js',
      ],
      globals: {
        "artifacts": true,
        "contract": true,
        "simpleStorageInstance": true,
        "assert": true,
      },
    },
    {
      files: [
        'support/migrations/*.js',
      ],
      rules: {
        'no-param-reassign': 'off',
      }
    },
    {
      files: [
        'bin/*.js',
      ],
      rules: {
        'no-console': 'off',
      }
    },
  ],
  globals: {
    require: 0,
    shallow: 0,
    render: 0,
    mount: 0,
  },
  settings: {
    'import/resolver': {
      node: {
        paths: './backend',
      },
      webpack: {
        config: './config/webpack.config.dev.js',
      },
    },
  },
};
